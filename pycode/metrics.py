import numpy as np
import scipy.optimize

def max_eigenvalue(X, XS):
    return np.linalg.norm(XS.T.dot(XS), 2)

def min_eigenvalue(X, XS):
    return np.min(np.linalg.eigvals(XS.T.dot(XS)).real)

def spectral_conditioning(X, XS):
    eigs = np.linalg.eigvals(XS.T.dot(XS))
    return np.max(eigs) / np.min(eigs)

def num_unique(X, XS):
    return len(np.unique(XS.round(4), axis=0))






# Leverage for linear regression

def relative_leverage_in_dir(points, u, beta):
    """Error in estimating u if regression vector is beta"""
    return np.abs(u.dot(beta)) * np.sum(np.abs(points.dot(beta))) / np.sum(points.dot(beta)**2)

def relative_leverage(points, u):
    """Worst-case relative leverage for u"""
    return -scipy.optimize.minimize(lambda v: -relative_leverage_in_dir(points, u, v), u*1).fun

def worst_leverage(X, XS):
    unique = np.unique(X, axis=0)
    return np.max([relative_leverage(XS, u) for u in unique])

def relative_leverages_missing_one(points):
    unique_rows = np.unique(points, axis=0, return_index=True)
    return [relative_leverage(np.concatenate([points[:i], points[i+1:]]), points[i])
                 for i in unique_rows]

