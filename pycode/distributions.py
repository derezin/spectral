import numpy as np
import scipy.linalg

def isotropify(X):
    S = scipy.linalg.sqrtm(X.T.dot(X))
    return X.dot(np.linalg.inv(S))

class Generator(object):
    def generate(self, n, d):
        raise NotImplementedError()

    def __call__(self, *args, **kws):
        return isotropify(self.generate(*args, **kws))

class SimplexGenerator(Generator):
    def generate(self, n, d):
        """Return a d+1 x d matrix representing a simplex."""
        assert n == d + 1
        lifted = np.identity(d+1)
        lifted -= np.mean(lifted)
        projected = np.linalg.svd(lifted)[0][:,:d]
        projected /= np.linalg.norm(projected[0])
        return projected

class RandomGenerator(Generator):
    def generate(self, n, d):
        return np.random.randn(n, d) / n**.5

class RedundantGenerator(Generator):
    def __init__(self, other_generator: Generator, redundancy: int):
        self.other_generator = other_generator
        self.redundancy = redundancy

    def generate(self, n, d):
        smaller_n = (n + self.redundancy - 1) // self.redundancy
        smaller_set = self.other_generator.generate(smaller_n, d)
        answer = np.vstack([smaller_set]*self.redundancy)[:n]
        return answer


# (d + k) clusters: RedundantGenerator(RandomGenerator(), 10)

# Cayley graphs
