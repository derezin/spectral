import numpy as np
import scipy.stats

class Sampler(object):
    def sample(self, X, m):
        raise NotImplementedError()

    def __call__(self, *args, **kws):
        return self.sample(*args, **kws)

class UniformSampler(Sampler):
    def __init__(self, replace=True):
        self.replace = replace

    def sample(self, X, m):
        n, d = X.shape
        indices = np.random.choice(np.arange(n), m, replace=self.replace)
        return X[indices, :] * (n/m)**.5

class LeverageScoreSampler(Sampler):
    def sample(self, X, m):
        U = np.linalg.svd(X, full_matrices=False)[0]
        leverage_scores = np.linalg.norm(U, axis=1)**2
        sampler = scipy.stats.rv_discrete(values=(np.arange(X.shape[0]), leverage_scores / X.shape[1]))
        indices = sampler.rvs(size=m)
        return (X.shape[1]/m)**.5* X[indices] / (leverage_scores[indices]**.5).reshape(-1, 1)

def proportional_sample(p, m):
    sampler = scipy.stats.rv_discrete(values=(np.arange(len(p)), p / np.sum(p)))
    return sampler.rvs(size=m)

class VolumeSampler(Sampler):
    """n^2d, but faster if n = O(d)."""
    def sample(self, X, m):
        n, d = X.shape
        assert m >= d

        Z = np.linalg.inv(X.T.dot(X))
        p = 1 - np.sum(X.dot(Z) * X, axis=1)
        S = np.ones(n, dtype=bool)
        for t in range(n, m, -1):
            assert np.sum(p) > 0
            sample = proportional_sample(p[S], 1)[0]
            i = np.where(S)[0][sample]
            v = (X[i:i+1,:].dot(Z) / p[i]**.5).T
            S[i] = 0
            p = np.clip(p - (X.dot(v).flatten())**2, 0, None)
            Z += v.dot(v.T)
        indices = np.where(S)[0]

        U = np.linalg.svd(X, full_matrices=False)[0]
        leverage_scores = np.linalg.norm(U, axis=1)**2
        return (d/m)**.5* X[indices] / (leverage_scores[indices]**.5).reshape(-1, 1)


# Samplers to create:

# Gaussian embedding sampler

# Volume sampling + uniform/leverage

# Volume + adaptive

# power of d choices

# BSS
