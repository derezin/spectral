import numpy as np
import pylab


import distributions
import samplers
import metrics


def test(distribution, sampler, metrics):
    d = 30
    n = 5 * (30 + 5)
    ms = np.geomspace(30, 200, 10, dtype=int)

    value_arr = []
    for m in ms:
        print(f"Sampling {m} out of {n}x{d}...")
        X = distribution(n, d)
        XS = sampler(X, m)
        values = [metric(X, XS) for metric in metrics]
        print(f"{values}")
        value_arr.append(values)

    return np.array(ms), np.array(value_arr)

dist = distributions.RedundantGenerator(distributions.RandomGenerator(), 5)
sampler = samplers.UniformSampler()
ms, values = test(dist, sampler, [metrics.worst_leverage,
                                  metrics.num_unique])

pylab.plot(ms, values[:,0], color='b')
pylab.ylabel("Linear regression approx. ratio")
pylab.ylim([0, 50])
pylab.twinx()
pylab.plot(ms, 35-values[:,1], color='r')
pylab.ylabel("# missing clusters")
pylab.legend()
pylab.savefig('example.pdf')
