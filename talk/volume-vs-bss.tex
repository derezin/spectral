\documentclass{beamer}

\mode<presentation>
{
    %\usetheme{Warsaw}
    \usecolortheme{crane}
    % or ...

    \setbeamercovered{invisible}
    % or whatever (possibly just delete it)
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]{}

\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usepackage{mathtools}
\usepackage{relsize}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{colortbl}
%\usepackage{multicol}
\usepackage{cancel}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{xfrac}
\usepackage{forloop}% http://ctan.org/pkg/forloop
\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}
}

\input{../shortdefs}
\renewcommand{\Xinv}{(\X^\top\X)^{-1}}
\renewcommand{\Xinvr}{(\lambda\I+\X_{-1}^\top\X_{-1})^{-1}}
\newcommand{\svr}[1]{{\textcolor{darkSilver}{#1}}}
%\newcommand{\white}[1]{{\textcolor{white}{#1}}}
\definecolor{brightyellow}{cmyk}{0,0,0.7,0.0}
\definecolor{lightyellow}{cmyk}{0,0,0.3,0.0}
\definecolor{lighteryellow}{cmyk}{0,0,0.1,0.0}
\definecolor{lightestyellow}{cmyk}{0,0,0.05,0.0}

%\renewcommand{\Red}[1]{\hspace{1cm}\vspace{5mm}}

\title[]{Bridging volume sampling and BSS}

\author[]{Micha{\l } Derezi\'{n}ski}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}


\begin{frame}
  \frametitle{Volume sampling}
  Let $\X\in\R^{n\times d}$. Let $\x_i^\top$ denote the $i$th row of $\X$,\\
  Also define $\U=\X(\X^\top\X)^{-\frac12}$ (where $\U^\top\U=\I$).\\[4mm]
  \textit{Volume sampling}:\\
  distribution over sets of size $d$, $S\subseteq\{1..n\}$, where
  \begin{align*}
   \Vol(\X)\!:\qquad \Pr(S) = \frac{\det(\X_S)^2}{\det(\X^\top\X)} = \det(\U_S)^2.
  \end{align*}
  \textbf{Note:} The marginal probability of index $i$ in volume sampling is:
  \begin{align*}
    \Pr(i\in S) = \x_i^\top(\X^\top\X)^{-1}\x_i = \|\u_i\|^2.
  \end{align*}
\end{frame}


  \begin{frame}
    \frametitle{ Bottom-up sampling algorithm}
    Preprocessing: we compute matrix $\U=\X(\X^\top\X)^{-\frac12}$.\\[2mm]
    
    $\verb~Bottom-up~(\U)$:\vspace{0mm}
    
  \begin{algorithmic}[1]
    \STATE \textbf{input:} $\U\in\R^{n\times d}$, such that $\U^\top\U=\I$
    % \STATE $\forall _{i=1}^n\ \ubt_i \leftarrow\u_i$
    \STATE $\U^{(0)} \leftarrow \U$
    \STATE \textbf{for }$t=0\,..\,d\!-\!1$
    \STATE  \quad Sample $\sigma_{t+1} \sim
    \big(\|\u_1^{(t)}\|^2,\dots,\|\u_n^{(t)}\|^2\big)$ 
    \STATE \quad$\U^{(t+1)}\leftarrow \U^{(t)}\Big(\I -
    \frac{\u^{(t)}_{\sigma_{t+1}}\u^{(t)\top}_{\sigma_{t+1}}}{\|\u^{(t)}_{\sigma_{t+1}}\|^2}\Big)$
    \STATE \textbf{end for}
    \RETURN $S = \{\sigma_1,\dots,\sigma_d\}$
  \end{algorithmic}
  Note that $\U^{(t) \top}\U^{(t)}$ is a $(d-t)$-dimensional projection
  so
  \begin{align*}
    \sum_{i=1}^n \big\|\u_i^{(t)}\big\|^2
    &= \tr\big(\U^{(t)\top}\U^{(t)}\big) = d-t,\\
\Pr(S)
&=d!\cdot\prod_{t=0}^{d-1}\frac{\|\u_{\sigma_t}^{(t)}\|^2}{d-t}
=\det(\U_S)^2     
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Volume sampling as the limit of a ``BSS-like'' algorithm}
  \begin{algorithmic}[1]
    \STATE \textbf{input:} $\X\in\R^{n\times d}$, constant
    $\lambda>0$, integer $k>0$
    \STATE $\A \leftarrow \X^\top\X$
    \STATE \textbf{for }$t=1..k$
    \STATE \quad Sample $\sigma_t\sim
    \big\{\x_i^\top\A^{-1}\x_i\big\}_{i=1}^n$
    \STATE \quad $\A\leftarrow \A + \frac1{\lambda}\,\x_{\sigma_t}\x_{\sigma_t}^\top$
    \STATE \textbf{end for}
    \RETURN $\sigma_1,\dots,\sigma_k$
  \end{algorithmic}
  \vspace{1mm}
  
  Define this distribution as $\sigma\sim\Vol^k(\X,\lambda)$\\[5mm]
  \textbf{Theorem.} \ This distribution converges to volume sampling:
  \begin{align*}
    \Vol^d(\X,\lambda)\underset{\lambda\rightarrow 0}{\longrightarrow}\Vol(\X).
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Proof of Theorem}
  We use Sherman-Morrison:
  \begin{align*}
    \Big(\A+\frac1{\lambda}\x_i\x_i^\top\Big)^{-1} =
    &\
    \A^{-1}-\frac{\A^{-1}\x_i\x_i^\top\A^{-1}}{\lambda+\x_i^\top\A^{-1}\x_i}\\
    \hspace{-1mm}\underset{\lambda\rightarrow
      0}{\longrightarrow}&\
                           \A^{-1}-\frac{\A^{-1}\x_i\x_i^\top\A^{-1}}{\x_i^\top\A^{-1}\x_i}\\ 
    =&\ \A^{-\frac12}\bigg(\I -
       \frac{\A^{-\frac12}\x_i\x_i^\top\A^{-\frac12}}{\x_i^\top\A^{-1}\x_i}\bigg)\A^{-\frac12}\\
=&\ \A^{-\frac12}\bigg(\I - \frac{\A^{-\frac12}\x_i\x_i^\top\A^{-\frac12}}{\x_i^\top\A^{-1}\x_i}\bigg)^2\A^{-\frac12},
  \end{align*}
  so if $\U^{(t)} = \X\A^{-\frac12}$ and
  $\U^{(t+1)}=\X(\A+\frac1{\lambda}\x_i\x_i^\top)^{-\frac12}$, then
  \begin{align*}
    \U^{(t+1)}\underset{\lambda\rightarrow
      0}{\longrightarrow}\X\A^{-\frac12}\bigg(\I -
    \frac{\A^{-\frac12}\x_i\x_i^\top\A^{-\frac12}}
    {\x_i^\top\A^{-1}\x_i}\bigg)=\U^{(t)}\bigg(\I-
    \frac{\u^{(t)}_i\u^{(t)\top}_i}{\big\|\u^{(t)}_i\big\|^2}\bigg),
  \end{align*}
  so we exactly recover the bottom-up algorithm.
\end{frame}

\begin{frame}
  \frametitle{A natural extension of volume sampling}
  \textbf{Definition.} \ For $k>d$, it is natural to define:
  \[\Vol^k(\X)\defeq \lim_{\lambda\rightarrow 0}\Vol^k(\X,\lambda).\]
  \textbf{Theorem.} \ For $k\geq d$, if $\sigma\sim \Vol^k(\X)$, then:
  \begin{align*}
    \sigma_{[d]}\
    &\sim\  \Vol(\X),\\
    \Pr\big(\sigma_{t+1}\!=\!i\ |\ \sigma_{[t]}\big) \
&\propto\ \x_{i}^\top\big(\X_{\sigma_{[t]}}^\top\X_{\sigma_{[t]}}\big)^{-1}\x_{i},
\quad\text{for }t\geq d.
  \end{align*}
  \textbf{Proof.} \ For $p_i=\x_i^\top\A^{-1}\x_i$ we have
  \begin{align*}
    \frac{p_i}{\lambda}
    &= \frac1{\lambda}\x_i^\top
      \Big(\frac1{\lambda}\X_{\sigma_{[t]}}^\top\X_{\sigma_{[t]}} +
      \X^\top\X\Big)^{-1}\x_i \\
    &=\x_i^\top\big(\X_{\sigma_{[t]}}^\top\X_{\sigma_{[t]}} +
      \lambda\X^\top\X\big)^{-1}\x_i
      \underset{\lambda\rightarrow
      0}{\longrightarrow}\x_i^\top\big(\X_{\sigma_{[t]}}^\top\X_{\sigma_{[t]}}\big)^{-1}\x_i,
  \end{align*}
  because $\X_{\sigma_{t}}$ is full-rank.
\end{frame}

\begin{frame}
  \frametitle{A rescaled version of the ``BSS-like'' algorithm}
  Without loss of generality assume that $\X^\top\X=\I$
  \begin{algorithmic}[1]
    \STATE \textbf{input:} $\X\in\R^{n\times d}$, constant
    $\lambda>0$, integer $k>0$
    \STATE $\A \leftarrow \I$
    \STATE \textbf{for }$t=1..k$
    \STATE \quad Sample $\sigma_t\sim \{p_1,\dots,p_n\}$,\quad where
    $p_i=\frac{\x_i^\top\A^{-1}\x_i}{\tr(\A^{-1})}$
    \STATE \quad $\A\leftarrow \A + \frac1{\lambda \,p_{\sigma_i}}\,\x_{\sigma_t}\x_{\sigma_t}^\top$
    \STATE \textbf{end for}
    \RETURN $\sigma_1,\dots,\sigma_k$
  \end{algorithmic}
  \vspace{1mm}
  
  Define this distribution as $\sigma\sim\Vol^k(\X,\lambda)$\\[5mm]
  \textbf{Theorem.} \ This distribution converges to volume sampling:
  \begin{align*}
    \Vol^d(\X,\lambda)\underset{\lambda\rightarrow 0}{\longrightarrow}\Vol(\X).
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Proof of Theorem}
  We use Sherman-Morrison:
  \begin{align*}
    \Big(\A+\frac1{\lambda\, p_i}\x_i\x_i^\top\Big)^{-1} =
    &\
    \A^{-1}-\frac{\A^{-1}\x_i\x_i^\top\A^{-1}}{\lambda\, p_i+\x_i^\top\A^{-1}\x_i}\\
    \hspace{-1mm}\underset{\lambda\rightarrow
      0}{\longrightarrow}&\
                           \A^{-1}-\frac{\A^{-1}\x_i\x_i^\top\A^{-1}}{\x_i^\top\A^{-1}\x_i}\\ 
    =&\ \A^{-\frac12}\bigg(\I -
       \frac{\A^{-\frac12}\x_i\x_i^\top\A^{-\frac12}}{\x_i^\top\A^{-1}\x_i}\bigg)\A^{-\frac12}\\
=&\ \A^{-\frac12}\bigg(\I - \frac{\A^{-\frac12}\x_i\x_i^\top\A^{-\frac12}}{\x_i^\top\A^{-1}\x_i}\bigg)^2\A^{-\frac12},
  \end{align*}
  so if $\U^{(t)} = \X\A^{-\frac12}$ and
  $\U^{(t+1)}=\X(\A+\frac1{\lambda\,p_i}\x_i\x_i^\top)^{-\frac12}$, then
  \begin{align*}
    \U^{(t+1)}\underset{\lambda\rightarrow
      0}{\longrightarrow}\X\A^{-\frac12}\bigg(\I -
    \frac{\A^{-\frac12}\x_i\x_i^\top\A^{-\frac12}}
    {\x_i^\top\A^{-1}\x_i}\bigg)=\U^{(t)}\bigg(\I-
    \frac{\u^{(t)}_i\u^{(t)\top}_i}{\big\|\u^{(t)}_i\big\|^2}\bigg),
  \end{align*}
  so we exactly recover the bottom-up algorithm.
\end{frame}

\begin{frame}
  \frametitle{A rescaled extension of volume sampling}
  \textbf{Definition.} \ For $k>d$, it is natural to define:
  \[\Vol^k(\X)\defeq \lim_{\lambda\rightarrow 0}\Vol^k(\X,\lambda).\]
  \textbf{Theorem.} \ For $k\geq d$, if $\sigma\sim \Vol^k(\X)$, then:
  \begin{align*}
    \sigma_{[d]}\
    &\sim\  \Vol(\X),\\
    \Pr\big(\sigma_{t+1}\!=\!i\ |\ \sigma_{[t]}\big) \
&\propto\ \x_{i}^\top\Big(\sum_{i=1}^t\frac1{p_{\sigma_i}}\x_{\sigma_i}\x_{\sigma_i}^\top\Big)^{-1}\x_{i},
\quad\text{for }t\geq d.
  \end{align*}
  \textbf{Proof.} \ For $p_i=\x_i^\top\A^{-1}\x_i$ we have
  \begin{align*}
    \frac{p_i}{\lambda}
    &= \frac1{\lambda}\x_i^\top
      \Big(\frac1{\lambda}\sum_{i=1}^t\frac1{p_{\sigma_i}}\x_{\sigma_i}\x_{\sigma_i}^\top
      +\I\Big)^{-1}\x_i \\ 
    &=\x_i^\top\Big(\sum_{i=1}^t\frac1{p_{\sigma_i}}\x_{\sigma_i}\x_{\sigma_i}^\top +
      \lambda\I\Big)^{-1}\x_i
      \underset{\lambda\rightarrow
      0}{\longrightarrow}\x_i^\top\Big(\sum_{i=1}^t\frac1{p_{\sigma_i}}\x_{\sigma_i}\x_{\sigma_i}^\top\Big)^{-1}\x_i.
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Expected bound on the inverse}
  \begin{align*}
\hspace{-5mm}    \E_i\bigg[\Big(\A+\frac1{\lambda p_i}\x_i\x_i^\top\Big)^{-1}\bigg]
    =&\ \E\bigg[\A^{-1}-\frac{\A^{-1}\x_i\x_i^\top\A^{-1}}{\lambda\,
    p_i+\x_i^\top\A^{-1}\x_i}\bigg]
 \\
    =&\ \A^{-1} - \sum_{i=1}^n\frac{\x_i^\top\A^{-1}\x_i}{\tr(\A^{-1})}\cdot
      \frac{\A^{-1}\x_i\x_i^\top\A^{-1}}{\x_i^\top\A^{-1}\x_i\big(1+\lambda/\tr(\A^{-1})\big)}
    \\
    =&\ \A^{-1}-\sum_{i=1}^n\frac{\A^{-1}\x_i\x_i^\top\A^{-1}}{\tr(\A^{-1})
      + \lambda}
    \\
    =&\ \A^{-1} - \A^{-2}/\big(\tr(\A^{-1})+\lambda\big)
    \\
    =&\ \A^{-1}\Big(\I\ -\ \A^{-1}/\big(\tr(\A^{-1})+\lambda\big)\Big)
    \\
    \hspace{-1mm}\underset{\lambda\rightarrow
    0}{\longrightarrow}&\
\A^{-1}\Big(\I\ -\ \A^{-1}/\tr(\A^{-1})\Big)
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Estimator of the identity}
  Note that $\E_i[\frac1{\lambda p_i}\x_i\x_i^\top] =
  \frac1\lambda\,\I$. So for $\B_t = \frac\lambda {t+\lambda} \A_t$,
  \begin{align*}
    \E\big[\B_t\big] =
    \frac\lambda {t+\lambda}\bigg(\I+\sum_{i=1}^t\E_{\sigma_i}\Big[\frac1{\lambda p_{t,\sigma_i}}\x_{\sigma_i}\x_{\sigma_i}^\top\Big]\bigg)
  =\frac\lambda{t+\lambda}\Big(\I + \frac t\lambda \I\Big)  = \I
  \end{align*}
  \begin{align*}
    \E\big[\B_{t+1}^{-1}\,|\,\B_t\big]
    &=\frac{t+1+\lambda}{\lambda}\A_t^{-1}\Big(\I\ -\
      \A_t^{-1}/\big(\tr(\A_t^{-1})+\lambda\big)\Big)
\\ &=\frac{t+1+\lambda}{t+\lambda}\B_t^{-1}\Big(\I\ -\ \frac\lambda {t+\lambda}
     \B_t^{-1}/\big(\frac\lambda {t+\lambda}\tr(\B_t^{-1})+\lambda\big)\Big)
\\ &=\Big(1+\frac1{t+\lambda}\Big)\B_t^{-1}\Big(\I\ -\ \B_t^{-1}/\big(\tr(\B_t^{-1})+t+\lambda\big)\Big)
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Distributional interpretation}
  We repesent our dataset as a $d$-variate distribution $\x\sim D$.\\
  Denote $\Sigmab_D=\E_D[\x\x^\top]$. Now, the bottom-up algorithm is: 
    \begin{algorithmic}[1]
    \STATE \textbf{input:} $D$, constant $\lambda>0$, integer $k>0$
    \STATE $\A_0 \leftarrow \Sigmab_D$
    \STATE \textbf{for }$i=1..k$
    \STATE \quad Sample $\x_i\ \sim\ \x^\top\A_{i-1}^{-1}\x\cdot D(\x)$
    \STATE \quad $\A_i\leftarrow \A_{i-1} + \frac1{\lambda}\,\x_i\x_i^\top$
    \STATE \textbf{end for}
    \RETURN $\x_1,\dots,\x_k$ 
  \end{algorithmic}
Let $D_i$ be the distribution of $\x_i$ conditioned on
$\x_1,\dots,\x_{k-1}$.\\[2mm]
\textbf{Question:} What happens in the limit $D_1,D_2,\dots$ ?\\[2mm]
Let $\xbt\sim \Dt$ be defined as
$\xbt=\frac{\xbh}{\sqrt{\xbh^\top\Sigmab_D^{-1}\xbh}}$, where $\xbh\sim
\x^\top\Sigmab_D^{-1}\x\cdot D(\x)$
\begin{align*}
  \text{Then}\quad \lim_{k\rightarrow\infty}\Dt_k = \Dt\quad{and}\quad
  \lim_{k\rightarrow\infty}\frac\lambda k \A_k = \Sigmab_D=\Sigmab_{\Dt}
\end{align*}
\end{frame}

\end{document}