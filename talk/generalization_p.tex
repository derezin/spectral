%!TEX root = regression.tex
\section{Generalization to $\ell_p$-loss}
In this section, we consider the $\ell_p$ loss function $L(\beta):=\sum_{i=1}^n |a_i^{\top} \beta - y_i|^p$ for a given $p \in (1,2]$. The main result is to show that $\wt{L}(\beta)$, when generated properly according to the Lewis weights of $A$, satisfies the centered uniform approximation in Definition~\ref{def:uniform_approx} such that $\wt{\beta}=\arg\min \wt{L}(\beta)$ provides an $(1+\eps)$-approximation by Lemma~\ref{lem:approx_ratio}. 

\begin{theorem}\label{thm:property_ellp}
Given $p \in (1,2]$ and a matrix $A \in \mathbb{R}^{n \times d}$, let $(w'_1,\ldots,w'_n)$ be a $\gamma$-approximation of the Lewis weights $(w_1,\ldots,w_n)$ of $A$, i.e., $w_i \approx_{\gamma} w'_i$ for all $i \in [n]$. For $m=O(\frac{\gamma \cdot d^2 \cdot \log d/\eps}{\eps^2})$ with a sufficiently large constant, we sample $s_i \sim \frac{d}{m \cdot w'_i} \cdot \poiss(\frac{m \cdot w'_i}{d})$ for each $i \in [n]$.

Then with probability 0.8, $\wt{L}(\beta):=\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|^p$ satisfies centered uniform approximation. Namely,
\[ \wt{L}(\beta) - \wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot L(\beta) \text{ for any $\beta \in \mathbb{R}^d$}. \]
\end{theorem}

First of all, we bound the support size of $s$. Note that $\Pr[s_i=0]=e^{-\frac{m \cdot w_i}{d}}$ and $\Pr[s_i>0]= 1- e^{-\frac{m \cdot w_i}{d}} \le \frac{m \cdot w_i}{d}$. So $\E[|\supp(s)|] \le \sum_i \frac{m \cdot w_i}{d} =m$. By Markov's inequality, we know with probability 0.99, $|\supp(s)|=O(m)$.

In the rest of this section, we show the guarantee in Theorem~\ref{thm:property_ellp}, i.e., $\wt{L}(\beta) - \wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot L(\beta)$ with a unknown label vector $y$. The proof is via a reduction to the case of uniform Lewis weights, which is in the same spirit of the proof of Theorem~\ref{thm:Lewis_weight_importance}. Specifically, we will create an equivalent problem with matrix $A' \in \mathbb{R}^{N \times d}$ and $y' \in \mathbb{R}^{N}$ such that $\|A' \beta - y'\|_p=\|A \beta - y\|_p$ and the Lewis weights of $A'$ are almost uniform. Moreover, we show an equivalent way to generate $(s_1,\ldots,s_n)$ and $\wt{L}(\beta)$ in $A'$. Finally we use the guarantee of $A'$ to conclude the guarantee.

%Given $m$ and $A$, For each $i\in[n]$, we randomly generate $s_i=\frac{n}{m}$ with probability $\frac{m}{n}$ and $0$ with probability $1-\frac{m}{n}$. Similar to Section~\ref{sec:uniform_concergence}, we define $L(\beta):=\sum_{i=1}^n |a_i^{\top} \beta - y_i|^p$ and $\beta^*:=\arg\min_{\beta} L(\beta)$. After generating $s_1,\ldots,s_n$, we define $\wt{L}(\beta):=\sum_i s_i \cdot |a_i^{\top} \beta - y_i|^p$.

We state the following lemma for matrices with almost uniform Lewis weights and defer its proof to Section~\ref{sec:property_uniform_Lewis}. It may be convenient to assume $\gamma=O(1)$, $\alpha = O(1)$, $w'_i \approx d/n$ such that the probability $\frac{m \cdot w'_i}{d}<1$ in this statement.

\begin{lemma}\label{lem:convergence_p}
Given any matrix $A$, let $(w'_1,\ldots,w'_n)$ be a $\gamma$-approximation of the Lewis weights $(w_1,\ldots,w_n)$ of $A$, i.e., $w'_i \approx_{\gamma} w_i$ for all $i \in [n]$. Further move, suppose the Lewis weights are almost uniform: $w_i  \approx_{\alpha} d/n$ for a given parameter $\alpha$. 

Let $m=O(\frac{\gamma \cdot d^2 \cdot \alpha^{O(1)} \cdot \log d/\eps}{\eps^2})$. For each $i\in[n]$, we randomly generate $s_i=\frac{d}{m \cdot w'_i}$ with probability $\frac{m \cdot w'_i}{d}$ and $0$ otherwise. Then with probability 0.9, for $\wt{L}(\beta):=\sum_i s_i \cdot |a_i^{\top} \beta - y_i|^p$, we have
\[ \wt{L}(\beta) - \wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot L(\beta) \text{ for any $\beta \in \mathbb{R}^d$}. \]
\end{lemma}

Now we are ready to finish the proof of Theorem~\ref{thm:property_ellp}.

\begin{proofof}{Theorem~\ref{thm:property_ellp}}
Because we could always adjust $w'_i$ and $m$ by a constant factor, let $\eps \rightarrow 0$ be a tiny constant such that $N_1=w'_1/\eps,N_2=w'_2/\eps,\ldots,N_n=w'_n/\eps$ are integers. We define $A' \in \mathbb{R}^{N \times d}$ with $N=\sum_i N_i$ rows as
\[
\begin{bmatrix} 
a_1/N_1^{1/p} \\
\vdots \\
a_1/N_1^{1/p} \\
a_2/N_2^{1/p} \\
\vdots \\
a_n/N_n^{1/p}
\end{bmatrix}
\]
where the first $N_1$ rows are of the form $a_1/N_1^{1/p}$, the next $N_2$ rows are of the form $a_2/N_2^{1/p}$, and so on.
Similarly, we define $y' \in \mathbb{R}^{N}$ as \[
\left( \underbrace{y_1/N_1^{1/p},\ldots,y_1/N_1^{1/p}}_{N_1},\underbrace{y_2/N_2^{1/p},\ldots,y_2/N_2^{1/p}}_{N_2},\ldots,\underbrace{y_n/N_n^{1/p},\ldots,y_n/N_n^{1/p}}_{N_n} \right). 
\]
By the definition, $L(\beta)=\|A \beta - y\|_p^p=\|A' \beta - y'\|_p^p$.

Then we consider $\wt{L}(\beta)$. First of all, the Lewis weights of $A'$ are 
\[
\left( \underbrace{w_1/N_1,\ldots,w_1/N_1}_{N_1},\underbrace{w_2/N_2,\ldots,w_2/N_2}_{N_2},\ldots,\underbrace{w_n/N_n,\ldots,w_n/N_n}_{N_n} \right). 
\]
Second, $(w'_1,\ldots,w'_n)$ on $A$ induces weights  \[
\ov{w} = \left( \underbrace{w'_1/N_1,\ldots,w'_1/N_1}_{N_1},\underbrace{w'_2/N_2,\ldots,w'_2/N_2}_{N_2},\ldots,\underbrace{w'_n/N_n,\ldots,w'_n/N_n}_{N_n} \right) \textit{ on } A'. 
\]
Then we consider $(s'_1,\ldots,s'_N)$ generated in the way described in Lemma~\ref{lem:convergence_p}: For $j \in [N]$, $s'_j=\frac{d}{m \cdot \ov{w}_j}$ with probability $\frac{m \cdot \ov{w}_j}{d}$ and $0$ otherwise. Let $i \in [n]$ be the row $a_i/N_i^{1/p}$ corresponding to $a'_j$, i.e., $j \in (N_1+\ldots+N_{i-1},N_1+\ldots+N_i]$. Since $\ov{w}_j=w'_i/N_i$, $s'_j=\frac{d \cdot N_i}{m \cdot w'_i}$ with probability $\frac{m \cdot w'_i}{N_i \cdot d}$. So the contribution of all $j$'s corresponding to $i$ is
\begin{align*}
& \sum_{j=N_1+\ldots+N_{i-1}+1}^{N_1+\ldots+N_{i}} s'_j \cdot | (a'_j)^{\top} \cdot \beta - y'_j|^p \\
= & \sum_{j=N_1+\ldots+N_{i-1}+1}^{N_1+\ldots+N_{i}} 1(s'_j>0) \frac{d \cdot N_i}{m \cdot w'_i} \cdot  |a_i^{\top}/N_i^{1/p} \cdot \beta - y_i/N_i^{1/p}|^p \\
= & \left( \sum_{j} 1(s'_j>0) \right) \cdot \frac{d}{m \cdot w'_i} \cdot |a_i^{\top} \beta - y_i|^p.
\end{align*}
Next the random variable $\left( \sum_{j} 1(s'_j>0) \right)$ generated by $s'_j$ converges to a Poisson random variable with mean $N_i \cdot \frac{m \cdot w'_i}{N_i \cdot d} = \frac{m \cdot w'_i}{d}$ when $\eps \rightarrow 0$ and $N_i \rightarrow +\infty$. So $s_i \sim \frac{d}{m \cdot w'_i} \cdot \poiss(\frac{m \cdot w'_i}{d})$ generated in this theorem will converge to the coefficient $\left( \sum_{j} 1(s'_j>0) \right) \cdot \frac{d}{m \cdot w'_i}$ in the above calculation. This implies that $\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|^p$ generated in Lemma~\ref{lem:convergence_p} for $A'$ with weights $\ov{w}$ converges to $\wt{L}(\beta) = \sum_j s'_j \cdot | (a'_j)^{\top} \cdot \beta - y'_j|^p$ generated in this theorem when $\eps\rightarrow 0$. 

From all discussion above, we have the equivalences of $L(\beta)=\|A \beta - y\|_p^p=\|A' \beta - y'\|_p^p$ and $\wt{L}(\beta)=\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|^p=\sum_j s'_j \cdot | (a'_j)^{\top} \cdot \beta - y'_j|^p$. Then Lemma~\ref{lem:convergence_p} (with the same $\gamma$ and $\alpha=1$) provides the guarantee of $\wt{L}$ for all $\beta$. 
\end{proofof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Almost uniform Lewis weights}\label{sec:property_uniform_Lewis}
We finish the proof of Lemma~\ref{lem:convergence_p} in this section. Since we can always rotate $A$ and shift $y$, we assume $A^{\top} A = I_d$ and $\beta^*=0$ in this proof for convenience. 

Before we give the formal proof, we discuss a few properties and ingredients that will be used in it. Our proof heavily relies on the following two properties of $A$: (1) Given $A^{\top} A = I_d$, we simplify the leverage score of each row $a_i$ to its $\ell_2$ norm square. Moreover, Claim~\ref{clm:non_uniform_Lewis_w} implies the leverage scores are almost uniform: 
\begin{equation}\label{eq:leverage_score_ell2}
\|a_i\|_2^2 \approx_{\alpha^{C_p}} d/n \text{ for } C_p=4/p-1.
\end{equation}
(2) The importance weights of $\|A \beta\|^p_p$ are almost uniform (from Theorem~\ref{thm:Lewis_weight_importance})
\begin{equation}\label{eq:importance_ellp}
\forall \beta, \forall i \in [n], \frac{\|a_i^{\top} \beta\|_p^p}{\|A \beta\|_p^p} \le w_i \le \alpha \cdot d/n.
\end{equation}

The key property of $\beta^*$ that will be used in this proof is the partial derivative on every direction is 0, i.e., $\partial L(\beta^*)=\vec{0}$. Since $\beta^*$ is assumed to be $\vec{0}$, this is equivalent to
\begin{equation}\label{eq:prop_beta*}
\forall j \in [d], \sum_{i=1}^n p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j}=0.
\end{equation}

It will be more convenient to write $L(\beta)-L(\beta^*)$ in Lemma~\ref{lem:convergence_p} as $\sum_{i=1}^n \big( |a_i^{\top} \beta -y_i|^p - |y_i|^p \big)$ (given $\beta^*=0$) and vice versa for $\wt{L}(\beta)-\wt{L}(\beta^*)$. Then we will use the following claim to approximately bound $|a_i^{\top} \beta - y_i|^p - |y_i|^p$ in the comparison of $L$ and $\wt{L}$.
\begin{claim}\label{clm:taylor_exp_ellp}
For any real numbers $a$ and $b$, 
\[
|a-b|^p - |a|^p + p \cdot |a|^{p-1} \cdot \sign(a) \cdot b = O(|b|^p).
\]
In particular, this implies that
for any $\beta$, $a_i$, and $y_i$, we always have
\[
|a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta = O(|a_i^{\top} \beta|^p).
\]
\end{claim}

We will use the following two claims about random events associated with $s_1,\ldots,s_n$ to finish the proof. Recall that $s_i$ is generated according to the weight $w'_i$. So one property about $w'_i$ that will be used in this proof is $w'_i \approx_{\gamma} w_i$ and $w_i \approx_{\alpha} d/n$ imply $w'_i \approx_{\gamma \alpha} d/n$. The first claim considers the concentration for the approximation in terms of Clam~\ref{clm:taylor_exp_ellp}.
\begin{claim}\label{clm:concentration_single_beta}
Given any $\beta$, with probability at least $1-exp\big( -\Omega(\frac{\eps^2 m}{\alpha^{O(1)} \cdot \gamma \cdot d}) \big)$ (over $s_1,\ldots,s_m$),
\begin{align*}
& \sum_{i=1}^n s_i \cdot \left( |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right) \\
& = \sum_{i=1}^n \left( |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right) \pm \eps \|A \beta\|_p^p.
\end{align*}
\end{claim}
Note that the extra term on the R.H.S., $\sum_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta$, is always zero for any $\beta$ from \eqref{eq:prop_beta*}. So the last ingredient is to bound its counterpart on the left hand side.

\begin{claim}\label{clm:inner_product}
With probability at least $0.95$, 
\[
\sum_{i=1}^n s_i \cdot p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta = O(\sqrt{\frac{\alpha^{\frac{2+p}{p}} \cdot \gamma \cdot d^{2/p}}{m}} \cdot \|A \beta\|_p \cdot \|y\|_p^{p-1})
\] \emph{for all} $\beta$.
\end{claim}
While Claim~\ref{clm:concentration_single_beta} states the probability for one vector $\beta$, Claim~\ref{clm:inner_product} holds for all $\beta$ which saves the union bound in the net argument. We defer the proof of Claim~\ref{clm:taylor_exp_ellp} to Section~\ref{sec:proof_taylor_exp}, the proof of Claim~\ref{clm:concentration_single_beta} to Section~\ref{sec:proof_single_beta}, and the proof of Claim~\ref{clm:inner_product} to Section~\ref{sec:proof_inner_product} separately.

\begin{proofof}{Lemma~\ref{lem:convergence_p}}
Recall that we assume $\beta^*=0$ in this proof. Let $C_0=\Theta(1/\epsilon)$ with a sufficiently large constant, $\delta=\eps/50$ and $B_{\delta}$ be an $\delta$-net of the $\ell_p$ ball with radius $C_0 \|y\|_p$ (in $\ell_p$ norm) such that for any $\beta$ with $\|A \beta\|_p \le C_0 \|y\|_p$, $\exists \beta' \in B_{\delta}$ satisfies $\|A (\beta-\beta')\|_p \le \delta \|y\|_p$. We set $m=O \big( \frac{\alpha^{O(1)} \gamma \cdot d^2 \log 1/\eps}{\eps^2} \big)$ such that we have the following two properties of $s_1,\ldots,s_n$:
\begin{enumerate}
\item Claim~\ref{clm:concentration_single_beta} holds for all $\beta \in B_{\delta}$. 
\item For all $\beta$, we always have $\sum_i |a_i^{\top} \beta|^p \approx_{1+\epsilon} \sum_i s_i \cdot |a_i^{\top} \beta|^p $.
\end{enumerate} 
Moreover, since $\E_s[\wt{L}(\beta^*)]=L(\beta^*)$, we assume $\wt{L}(\beta^*):=\sum_{i=1}^n s_i |y_i|^p\le 20 \|y\|_p^p$ w.p. 0.95 by the Markov's inequality and the assumption $\beta^*=0$.

First, we argue the conclusion $\wt{L}(\beta) - \wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm O(\eps) \cdot (\|A \beta\|_p^p + \|y\|_p^{p})$ holds for all $\beta \in B_{\delta}$. As mentioned earlier, we rewrite $L(\beta)-L(\beta^*)$ as 
\begin{equation}\label{eq:lhs_cross}
\sum_{i=1}^n \left( |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right)
\end{equation}
since the cross term is zero from \eqref{eq:prop_beta*}. Then we rewrite $\wt{L}(\beta) - \wt{L}(\beta^*)$ as
\[
\underbrace{\sum_{i=1}^n s_i \cdot \left( |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right)}_{T_1} - \underbrace{\sum_{i=1}^n s_i \cdot p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta}_{T_2}.
\]
By Claim~\ref{clm:concentration_single_beta}, the first term $T_1$ is $\eps \cdot \|A \beta\|_p^p$-close to \eqref{eq:lhs_cross}. Moreover, by Claim~\ref{clm:concentration_single_beta}, the second term $T_2$ is always upper bounded by $\eps \cdot \|y\|_p^{p-1} \cdot \|A \beta\|_p \le \eps (\|y\|_p^p + \|A \beta\|_p^p)$ given our choice of $m$.

Second, we argue $\wt{L}(\beta) - \wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm O(\eps+\delta) \cdot \big( \|A \beta\|_p^p + \|y\|_p^p \big)$ holds for all $\beta$ with $\|A \beta\|_p \le C_0 \|y\|_p$. Let us fix such a $\beta$ and define $\beta'$ to be the closest vector to it in $B_{\delta}$ with $\|A \beta - A \beta'\|_p \le \delta \|y\|_p$. We rewrite 

\begin{equation}\label{eq:net_p_norm}
L(\beta) - L(\beta^*)=\|A \beta' + A (\beta-\beta') - y \|_p^p - L(\beta^*).
\end{equation}

 By triangle inequality, we bound 
\[\|A \beta' + A (\beta-\beta') - y \|_p \in \left[ \|A \beta' - y\|_p - \|A(\beta-\beta')\|_p, \|A \beta' - y\|_p + \|A(\beta-\beta')\|_p \right]
\] and use the approximation in Claim~\ref{clm:taylor_exp_ellp} with $a=\|A \beta' - y\|_p$ and $b=\|A (\beta-\beta')\|_p$ to approximate its $p$th power in \eqref{eq:net_p_norm} as
\begin{align*}
& L(\beta) - L(\beta^*)\\
= & \|A \beta' - y\|_p^p - L(\beta^*) \pm O\left(p \cdot \|A \beta'-y\|_p^{p-1} \cdot \|A (\beta-\beta')\|_p + \|A (\beta-\beta')\|_p^p\right)\\
= & \|A \beta' - y\|_p^p - L(\beta^*) \pm O\left( p \cdot \|A \beta'-y\|_p^{p-1} \cdot \delta \|y\|_p + \delta^p \cdot \|y\|_p^p \right).\\
= & \|A \beta' - y\|_p^p - L(\beta^*) \pm \delta \cdot O\left( \|A \beta'\|_p^{p-1} \cdot \|y\|_p + \|y\|_p^p + \|y\|_p^p \right) \tag{by triangle inequality}\\
= & L(\beta')-L(\beta^*) \pm \delta \cdot O(\|A \beta\|_p^{p-1} \cdot \|y\|_p + \|y\|_p^p).
\end{align*}
Now we approximate $\wt{L}(\beta) - \wt{L}(\beta^*)$ by defining a weighted $L_p$ norm $\|x\|_{s,p}$ for a vector $x \in \mathbb{R}^n$ as $(\sum_i s_i |x_i|^p)^{1/p}$. Note that $\|\cdot\|_{s,p}$ satisfies the triangle inequality. At the same time, $\wt{L}(\beta)=\|A \beta - y\|_{s,p}$ by the definition and $\|A \beta\|_p \approx_{1+\epsilon} \|A \beta\|_{s,p}$ by the 2nd property mentioned in this proof. By the same argument as above, we have
\[
\wt{L}(\beta)-\wt{L}(\beta^*)=\wt{L}(\beta') - \wt{L}(\beta^*) \pm \delta \cdot O(\|A \beta\|_{s,p}^{p-1} \cdot \|y\|_{s,p} + \|y\|_{s,p}^p).
\]
Since $\beta'$ has $\wt{L}(\beta') - \wt{L}(\beta^*) = L(\beta')-L(\beta^*) \pm O(\eps)\cdot (\|A \beta'\|_p^p +\|y\|^{p}_p)$, from all the discussion above, $\beta$ has
\begin{align*}
\wt{L}(\beta) - \wt{L}(\beta^*) & = L(\beta)-L(\beta^*)  \pm O(\eps)\cdot (\|A \beta'\|_p^p + \|y\|^{p}_p) \pm \delta \cdot O(\|A \beta\|_{s,p}^{p-1} \cdot \|y\|_{s,p} + \|y\|_{s,p}^p)\\
& = L(\beta)-L(\beta^*) \pm O(\eps)\cdot (\|A \beta'\|_p^p + \|y\|^{p}_p) \pm 20 \delta \cdot O(\|A \beta\|_{p}^{p-1} \cdot \|y\|_{p} + \|y\|_{p}^p)
\end{align*}
where we replace $\|\cdot\|_{s,p}$ using the bound of $\sum_i s_i |y_i|^p$ and property 2 of $s_1,\ldots,s_n$ mentioned above. Now we simplify the error to $(\eps+\delta) \cdot O\big\|A \beta\|_p^p + \|y\|_p^p \big)$: Consider the following two cases
\begin{enumerate}
\item $\|A \beta\|_p \ge 3 \|y\|_p$: This implies $\|A \beta'\|_p \le 2 \|A \beta\|_p$. So the whole error is upper bounded by $O(\eps+\delta) \|A \beta\|_p^p$.
\item $\|A \beta\|_p \le 3 \|y\|_p$: This implies $\|A \beta'\|_p \le 4 \|y\|_p^p$. So the whole error is upper bounded by $O(\eps+\delta) \|y\|_p^p$.
\end{enumerate}

The last case of $\beta$ is $\|A \beta\|_p > C_0 \|y\|_p$, where we bound $\wt{L}(\beta) - \wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm O(\eps) \cdot \|A \beta\|_p^p$. Note that $L(\beta):=\|A \beta - y\|_p^p$ is in $(\|A \beta\|_p \pm \|y\|_p)^p$ by the triangle inequality. From the approximation in Claim~\ref{clm:taylor_exp_ellp}, this is about
\[
\|A \beta\|_p^p \pm p \|A \beta\|_p^{p-1} \cdot \|y\|_p \pm O(\|y\|_p^p),
\]
which bounds 
\[
L(\beta)-L(\beta^*)=\|A \beta\|_p^p \pm p \|A \beta\|_p^{p-1} \cdot \|y\|_p \pm O(\|y\|_p^p)=\|A \beta\|_p^p \pm O(1/C_0) \|A \beta\|_p^p.
\] 

Similarly, we have $\wt{L}(\beta) - \wt{L}(\beta^*)=\|A \beta\|_{s,p}^p \pm p \|A \beta\|_{s,p}^{p-1} \cdot \|y\|_{s,p} \pm O(\|y\|_{s,p}^p)$ for the weighted $\ell_p$ norm defined as above. Recall that $\|y\|_{s,p}^p \le 20 \|y\|_p^p$ and $\|A \beta\|_{s,p}^p = (1+\pm \epsilon) \|A \beta\|_p^p$, so
\[
\wt{L}(\beta) - \wt{L}(\beta^*)= \|A \beta\|^p_p \pm \epsilon \|A\beta\|_p^p \pm O(1/C_0) \|A \beta\|_p^p.
\]
When $C_0 = \Theta(1/\eps)$ for a sufficiently large constant, the error is $O(\epsilon) \cdot \|A \beta\|_p^p$.

Finally we conclude that the error is always bounded by $\epsilon \cdot L(\beta):=\epsilon \cdot \|A \beta - y\|_p^p$. When $\|A \beta\|_p \le 2\|y\|_p$, $L(\beta) \ge \|y\|_p^p$ by the definition of $\beta^*$. So $\epsilon \cdot O(\|y\|_p^p + \|A \beta\|_p^p)=O(\epsilon) \cdot L(\beta)$. Otherwise, $L(\beta) \ge \|A \beta\|_p^p/2$ and $\|y\|_p^p \le \|A \beta\|_p^p$, we also have $\epsilon \cdot (\|y\|_p^p + \|A \beta\|_p^p)=O(\epsilon) \cdot L(\beta)$.
\end{proofof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Proof of Claim~\ref{clm:taylor_exp_ellp}}\label{sec:proof_taylor_exp}
Without loss of generality, we assume $a>0$. We consider two cases: 
\begin{enumerate}
\item $|b|<a/2$: We rewrite $|a-b|^p - |a|^p + p \cdot |a|^{p-1} \cdot b$ as $\int_{a-b}^{a} p \cdot a^{p-1} - p x^{p-1} \mathrm{d} x$. Now we bound $p \cdot a^{p-1} - p x^{p-1}$ using Taylor's theorem, whose reminder is $f'(\zeta) \cdot (a-x)$ for $f(t)=p \cdot t^{p-1}$ and some $\zeta \in (x,a)$. So we upper bound the integration by
\[
\int_{a-b}^{a} p \cdot a^{p-1} - p x^{p-1} \mathrm{d} x \le \int_{a-b}^a p (p-1) \cdot |\zeta_x|^{p-2} \cdot |a-x| \mathrm{d} x.
\]
Since $p \in (1,2]$, $x \in (a-b,a)$ and $\zeta_x \in (x,a)$, we always upper bound $|\zeta_x|^{p-2}$ by $\frac{1}{|a/2|^{2-p}}$. So this integration is upper bounded by $\frac{p(p-1) \cdot b^2}{2|a/2|^{2-p}}=O(|b|^{p})$ given $|b|<a/2$.
\item $|b| \ge a/2$: We upper bound 
\begin{align*}
|a-b|^p - |a|^p + p \cdot |a|^{p-1} \cdot b \le & (|a|+|b|)^p + |a|^p + p \cdot |a|^{p-1} \cdot |b| \\
\le & (3|b|)^p + (2|b|)^p + p \cdot 2^{p-1} \cdot |b|^p = O(|b|^p).
\end{align*}
\end{enumerate}

\subsection{Proof of Claim~\ref{clm:concentration_single_beta}}\label{sec:proof_single_beta}
Note that 
\[
\E \sum_{i=1}^n s_i \left( |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right)= \sum_{i=1}^n \left( |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right)
\]
given $s_i=\frac{d}{m \cdot w'_i}$ with probability $\frac{m \cdot w'_i}{d}$ (otherwise $0$). To bound the deviation, we plan to use the following version of Bernstein's inequality for $X_i=(s_i-1) \cdot \left( |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right)$:
\[  \Pr\Big(\sum_i X_i\geq t\Big)\leq\exp\bigg(-\frac{\frac12
    t^2}{\sum_i\E[X_i^2] + \frac13Mt}\bigg).\]
First of all, we bound $M=\sup |X_i|$ as
\begin{align*}
& \frac{d}{m \cdot w'_i} \left| |a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta \right|\\
\le & \frac{d}{m \cdot w'_i} \cdot C |a_i^{\top} \beta|^{p} \tag{by Claim~\ref{clm:taylor_exp_ellp}}\\
\le & \frac{d}{m} \cdot C \cdot \frac{|a_i^{\top} \beta|^{p}}{w'_i}\\
\le & \frac{d}{m} \cdot C \gamma \cdot \frac{|a_i^{\top} \beta|^{p}}{w_i} \tag{recall $w'_i \approx_{\gamma} w_i$}\\
\le & \frac{d}{m} \cdot C \gamma \cdot \|A \beta\|^{p}_p \tag{by Property~\eqref{eq:importance_ellp}}
\end{align*}
Next we bound $\sum_i \E[X_i^2]$ as
\begin{align*}
& \sum_{i=1}^n \E\left[ (s_i-1)^2 \cdot (a_i^{\top} \beta - y_i|^p - |y_i|^p + p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta )^2\right]\\
\le & \sum_{i=1}^n \E[(s_i-1)^2] \cdot C^2 \cdot |a_i^{\top} \beta|^{2p} \tag{by Claim~\ref{clm:taylor_exp_ellp}}\\
\le & \sum_{i=1}^n (1+\frac{d}{m \cdot w'_i}) \cdot C^2 \cdot (\frac{\alpha \cdot d}{n} \|A \beta\|_p^p) \cdot |a_i^{\top} \beta|^{p} \tag{by Property~\eqref{eq:importance_ellp}}\\
\le & \frac{2 d \cdot \alpha \gamma \cdot n}{m \cdot d} \cdot C^2 (\frac{\alpha \cdot d}{n} \|A \beta\|_p^p) \cdot \sum_{i=1}^n |a_i^{\top} \beta|^{p} \tag{by $w' \approx_{\alpha \gamma} d/n$}\\
= & \frac{2 C^2 \cdot \alpha^{2} \gamma \cdot d}{m} \|A \beta\|_p^{2p}.
\end{align*}
So for $m=\Omega(\alpha^{2} \gamma d/\eps^2)$ and $t=\eps \|A \beta\|_p^p$, we have the deviation is at most $t$ with probability $1 - exp(-\Omega(\frac{\eps^2 m}{\alpha^{2} \gamma \cdot d}))$.

\subsection{Proof of Claim~\ref{clm:inner_product}}\label{sec:proof_inner_product}
By Cauchy-Schwartz, we upper bound 
\begin{align*}
& \sum_{i=1}^n s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot \sum_j A_{i,j} \beta_j \\
= & \sum_j \beta_j \cdot (\sum_i s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j}) \\
\le & \left\| \left( \sum_i s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right)_{j \in [d]} \right\|_2 \cdot \|\beta\|_2.
\end{align*}
Now we bound the $\ell_2$ norm of those two vectors separately.
\begin{fact}
$\E \left\| \left( \sum_i s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right)_{j \in [d]} \right\|^2_2 \le \frac{2\alpha d}{m} \cdot \sum_i |y_i|^{2p-2}$.
\end{fact}
\begin{proof}
We rewrite the $\ell_2$ norm square as
\begin{align*}
& \E \left\| \left( \sum_i s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right)_{j \in [d]} \right\|^2_2 - \sum_j \E \left[ \sum_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right]^2 \tag{the make up term is always zero by Property~\eqref{eq:prop_beta*}}\\
= & \sum_j \E \left[ \left( \sum_i (s_i-1) p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right)^2 \right] \\
= & \sum_j \sum_i \E \left( (s_i-1) p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right)^2 \tag{use $\E[s_i]=1$ and the independence between different $s_i$'s}\\
\le & \sum_j \sum_i (\frac{d}{m \cdot w'_i}+1) \cdot |y_i|^{2p-2} \cdot A_{i,j}^2 \\
\le & \sum_i |y_i|^{2p-2} \cdot (\frac{d}{m \cdot w'_i}+1) \cdot \sum_j A_{i,j}^2 \tag{use the definition of leverage score in \eqref{eq:leverage_score_ell2} and $w'_i \approx_{\alpha \gamma} d/n$}\\
\le & \sum_i |y_i|^{2p-2} \cdot \frac{2\alpha \gamma n}{m} \cdot \frac{\alpha^{C_p} d}{n} \\
= & \frac{2\alpha^{1+C_p} \gamma d}{m} \cdot \sum_i |y_i|^{2p-2}.
\end{align*}
\end{proof}

\begin{fact}
Suppose $A^{\top} A=I_d$ and its leverage score is almost uniform: $\|a_i\|_2^2 \approx_{\alpha} d/n$. Then
$\|\beta\|_2 \le \|A \beta\|_p \cdot (\frac{\alpha^{C_p} \cdot d}{n})^{\frac{2-p}{2p}}$.
\end{fact}
\begin{proof}
Since $A^{\top} A=I_d$, $\|\beta\|_2=\|A\beta\|_2$. Then we upper bound $\|A \beta\|^2_2$:
\begin{align*}
\|A \beta\|_2^2 & = \sum_{i=1}^n |a_i^{\top} \beta|^2\\
& \le (\sum_i |a_i^{\top} \beta|^p) \cdot \max_i |a_i^{\top} \beta|^{2-p}\\
& \le \|A \beta\|_p^p \cdot (\frac{\alpha \cdot d}{n} \cdot \|A \beta\|_p^p)^{\frac{2-p}{p}} \tag{by Property~\eqref{eq:importance_ellp}}\\
& \le \|A \beta\|_p^2 \cdot (\frac{\alpha \cdot d}{n})^{\frac{2-p}{p}}.
\end{align*}
\end{proof}

Now we are ready to finish the proof. From the 1st fact, with probability 0.99,
\[
\left\| \left( \sum_i s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right)_{j \in [d]} \right\|_2 \le 10 \sqrt{\frac{2\alpha^{O(1)} \gamma d}{m} \cdot \sum_i |y_i|^{2p-2}}.
\]
From the 2nd fact, we always have
\[
\|\beta\|_2 \le \|A \beta\|_p \cdot (\frac{\alpha \cdot d}{n})^{\frac{2-p}{2p}}.
\]
So 
\begin{align*}
\sum_{i=1}^n s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot \sum_j A_{i,j} \beta_j \le & \left\| \left( \sum_i s_i p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot A_{i,j} \right)_{j \in [d]} \right\|_2 \cdot \|\beta\|_2\\
\le & 20 \sqrt{\frac{\alpha^{O(1)} \gamma d}{m}} \cdot \|y\|_{2p-2}^{p-1} \cdot \|A \beta\|_p \cdot (\frac{\alpha \cdot d}{n})^{\frac{2-p}{2p}}\\
\le & C \sqrt{\frac{\alpha^{O(1)} \gamma d}{m}} \cdot \|A \beta\|_p \cdot (\alpha \cdot d)^{\frac{2-p}{2p}} \cdot \|y\|_{2p-2}^{p-1} \cdot (1/n)^{\frac{2-p}{2p}}.
\end{align*}
Finally we use Holder's inequality to bound the last two terms $\|y\|_{2p-2}^{p-1} \cdot (1/n)^{\frac{2-p}{2p}}$ in the above calculation. We set $q_1=\frac{p}{2-p}$ and $q_2=\frac{p}{2p-2}$ (such that $1=1/q_1+1/q_2$) to obtain
\[
(\sum_i |y_i|^{2p-2}) \le (\sum_i 1)^{1/q_1} \cdot (\sum_i |y_i|^{(2p-2) \cdot q_2})^{1/q_2} = n^{\frac{2-p}{p}} \cdot \|y\|_p^{2p-2}.
\]
So we further simplify the above calculation
\[
C \sqrt{\frac{\alpha^{O(1)} \gamma d}{m}} \cdot \|A \beta\|_p \cdot (\alpha \cdot d)^{\frac{2-p}{2p}} \cdot \|y\|_{2p-2}^{p-1} \cdot (1/n)^{\frac{2-p}{2p}}\le C \sqrt{\frac{\alpha^{{O(1)}} \cdot \gamma \cdot d^{2/p}}{m}} \cdot \|A \beta\|_p \cdot \|y\|_p^{p-1}.
\]