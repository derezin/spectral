\documentclass{beamer}
%\beamertemplateshadingbackground{brown!70}{yellow!10}
\mode<presentation>
{
    %\usetheme{Warsaw}
    \usecolortheme{crane}
    % or ...

    \setbeamercovered{transparent}
    % or whatever (possibly just delete it)
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]{}

\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usepackage{mathtools}
\usepackage{relsize}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{colortbl}
%\usepackage{multicol}
\usepackage{cancel}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{xfrac}
\usepackage{forloop}% http://ctan.org/pkg/forloop
\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}
}

\input{../shortdefs}
\renewcommand{\Xinv}{(\X^\top\X)^{-1}}
\renewcommand{\Xinvr}{(\lambda\I+\X_{-1}^\top\X_{-1})^{-1}}

\title[]{Spectral sparsification via volume sampling}

\author[]{Micha{\l } Derezi\'{n}ski}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Preliminaries}

\begin{frame}
  \frametitle{Problem statement}
  Let $\X\in\R^{n\times d}$ be a matrix  and let $\x_i^\top$ denote the $i$th row of $\X$.\\
  W.l.o.g. we can assume that $\X^\top\X=\sum_{i=1}^n\x_i\x_i^\top=\I$\\[4mm]
  \textbf{Goal:} Find indices $\Ical\in\{1..n\}^k$ and weights $s_1,\dots,s_n\geq 0$ s.t.
  \begin{align*}
    (1-\epsilon)\X^\top\X\preceq \sum_{i=1}^ks_{\Ical_i}\x_{\Ical_i}\x_{\Ical_i}^\top
    \preceq (1+\epsilon)\X^\top\X
  \end{align*}
  with $k=O(d/\epsilon)$, or as small as possible.\\[3mm]
  
  Alternate variants of the problem:
  \begin{enumerate}
  \item What if you require that $s_{\Ical_i}=\frac{n}{k}$ (fixed rescaling)?
  \item What if you only care about the lower-bound?
  \item What if you only need $\epsilon=\frac12$?
  \end{enumerate}
  \vspace{3mm}
  
  \textbf{Key question:} Does one or two size $d$ volume samples suffice?\\
  (for any one of the variants)
\end{frame}

\begin{frame}
  \frametitle{Volume sampling}
  For simplicity, assume $\X^\top\X=\I$.\\[4mm]
  \textit{Volume sampling}:
  distribution over sets of size $d$, $S\subseteq\{1..n\}$, where
  \begin{align*}
    \Pr(S) = \frac{\det(\X_S^\top\X_S)}{\det(\X^\top\X)} = \det(\X_S)^2.
  \end{align*}
  \textbf{Note:} The marginal probability of index $i$ in volume sampling is:
  \begin{align*}
    \Pr(i\in S) = \x_i^\top(\X^\top\X)^{-1}\x_i = \|\x_i\|^2.
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Scaled vs unscaled row selection}
  \textit{Unscaled row selection:} we set $k=d$, $\Ical=S$, so that
  \begin{align*}
\sum_{i=1}^ks_{\Ical_i}\x_{\Ical_i}\x_{\Ical_i}^\top = \frac{n}{d}\X_S^\top\X_S
  \end{align*}
  \textit{Scaled row selection:} we set $k=d$, $\Ical=S$ and
  $s_{i}=\frac {1}{\|\x_{i}\|^2}$.\\
  In this case we can show that:
  \begin{align*}
    \E\Big[\sum_{i=1}^ds_{\Ical_i}\x_{\Ical_i}\x_{\Ical_i}^\top\Big] =\sum_{i=1}^n \overbrace{\Pr(i\in S)}^{\|\x_i\|^2}\frac{1}{\|\x_i\|^2}\x_i\x_i^\top=\X^\top\X
  \end{align*}
\end{frame}

\section{Special cases and lower bounds}

\begin{frame}
  \frametitle{Scaled standard basis vectors}
  \textbf{Assume:} For any $i$, we have $\x_i=\alpha_i\mathbf{e}_t$ for some $t$.\\[5mm]

  \textit{Scaled volume sampling} immediately gets the answer with $\epsilon=0$.\\[5mm]

\end{frame}

\begin{frame}
  \frametitle{Simplex}
  We choose $\X\in\R^{(d+1)\times d}$ s.t. $\X^\top\X=\I$ and $\X\X^\top=\I-\frac{1}{d+1}\one$.\\[5mm]

  \textbf{Fact} \ For any set $S$ of size $d$:
  \begin{align*}
    \lambda_{\min}\Big(\frac{d\!+\!1}{d}\X_S^\top\X_S\Big) = \frac1d.
  \end{align*}
  So, one volume sample does not suffice.
\end{frame}

\begin{frame}
  \frametitle{Analysis of the expanded simplex}
  Suppose that $n=d+d^a$ for $a\in [0,1]$.\\[3mm]
  \textbf{Question:}\\ How many points are we missing after $t$ volume
  samples?\\[3mm]
  Probability that a given point is missed by one size $d$ sample: $\approx \frac{d^a}{n}\geq d^{a-1}$\\[3mm]

  After $t$ rounds the number of missed points on average is:
  \begin{align*}
  d^a\,\big(d^{a-1}\big)^{t-1} = d^{ta -t+1}
  \end{align*}
  So if $a>\frac{t-1}{t}$, we expect to miss at least one point.
  
\end{frame}

\begin{frame}
  \frametitle{Smallest eigenvalue when missing one point}
  Let $\X\in\R^{n\times d}$ be i.i.d. $\Nc(0,1)$ entries, with $n=d+d^a$.
  \begin{align*}
    \text{Experiments show: }\quad
    \E\bigg[\lambda_{\min}\Big(\frac{n}{n\!-\!1}
    \X_{-n}^\top\X_{-n}\big(\X^\top\X)^{-1}\Big)\bigg] \approx \frac{d^a}{n}
  \end{align*}
Setting $a=\frac{t-1}{t}$, we get $\lambda_{\min}\approx d^{-\frac1t}$, so we need $t>\log d$.  \\[3mm]
  \centering
  \includegraphics[width=0.8\textwidth]{../figs/missing-one}

\end{frame}

\begin{frame}
  \frametitle{Adaptive volume sampling}
  Simple sampling procedure:
  \begin{enumerate}
  \item Sample $S\sim \det(\X_S^\top\X_S)$
  \item \textbf{Repeat:} \quad Sample $i\sim
    \x_i^\top(\X_S^\top \X_S)^{-1}\x_i$, \quad $S\leftarrow S\cup \{i\}$
  \end{enumerate}
  \vspace{4mm}
  
  Alternative rescaled variant:
  \begin{enumerate}
  \item Sample $S\sim \det(\X_S^\top\X_S)$,\quad $\A = \X_S^\top\X_S$
  \item \textbf{Repeat:} \quad Sample $i\sim
    \x_i^\top\A^{-1}\x_i$, \quad $\A\leftarrow \A +
    \frac{\x_i\x_i^\top}{\x_i^\top\A^{-1}\x_i}$     
  \end{enumerate}
  \vspace{3mm}
  
  (non-adaptive sampling would be to NOT update $\A$)
\end{frame}

\begin{frame}
  \frametitle{Performance of non-adaptive sampling}
    \includegraphics[width=\textwidth]{../figs/volume-plus}
\end{frame}

\begin{frame}
  \frametitle{Performance of adaptive sampling}
    \includegraphics[width=\textwidth]{../figs/volume-plusplus}
\end{frame}

\section{Basic results}

\begin{frame}
  \frametitle{Extreme eigenvalues for size $d$ volume sampling}
Assume $\X^\top\X=\I$. For a volume sampled set $S$ of size $d$:
  \begin{align*}
    \E\Big[\tr\big((\X_S^\top\X_S)^{-1}\big)\Big]\leq
    (n-d+1)\,\tr(\X^\top\X) \leq nd.
  \end{align*}
  Let $\lambda_1,\dots,\lambda_d$ be eigenvalues of
  $\frac{n}{d}\X_S^\top\X_S$.
  \begin{align*}
    \frac1{\lambda_d}\leq
    \frac{d}{n}\tr\big((\X_S^\top\X_S)^{-1}\big)
    \leq \frac{d}{n}\,2nd = 2d^2,
  \end{align*}
  with constant probability. So $\lambda_d\geq \frac1{2d^2}$.\\[3mm]
  \textbf{Note:} \ For largest eigenvalue we can use Theorem 1.5 from
  \textit{``A Matrix Chernoff Bound for Strongly Rayleigh Distributions
    and Spectral Sparsifiers from a few Random Spanning Trees''}:\\
  Let $\lambda_1,\dots,\lambda_d$ be eigenvalues of $\sum_{i\in
    S}\frac{1}{\|\x_i\|^2}\x_i\x_i^\top$. Then
with probability at least $1-1/\text{poly}(n)$ we have
$\lambda_1=O(\log d)$.
\end{frame}


\end{document}