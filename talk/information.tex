%!TEX root = regression.tex
\section{Information Lower Bound}
We prove an information lower bound for the query complexity of $\ell_1$ regression in this section.
\begin{theorem}\label{thm:information_lower_bound}
Given any small $\epsilon$ and $d$, let the dimension $n \times d$ matrix
\[
A = \begin{pmatrix} 
 1 & 0 & \cdots & 0 \\
  \vdots & 0 & \cdots & 0 \\
  1 & 0 & \cdots & 0 \\  
  0 & 1 & \cdots & 0 \\
  0 & \vdots & \cdots & 0 \\
  0 & 1 & \cdots & 0 \\  
  \vdots & \vdots & \vdots & \vdots\\
  0 & \cdots & 0 & 1\\
  0 & \cdots & 0 & \vdots\\
  0 & \cdots & 0 & 1\\  
 \end{pmatrix}
\] for a sufficiently large $n$. It takes $m=\Omega(d/\eps^2)$ queries to $y$ to produce $\tilde{\beta}$ satisfying $\|A \tilde{\beta}- y\|_1 \le (1+\epsilon) \cdot \|A \beta^* - y\|_1$ with probability 0.99.
\end{theorem}
%We defer the proof of Lemma~\ref{lem:information_lower_dim_1} to Section~\ref{sec:lower_bound_dim_1} and finish the proof of Theorem~\ref{thm:information_lower_bound}.
Essentially the $\ell_1$ regression $\|A \beta - y\|_1$ for $A$ specified in Theorem~\ref{thm:information_lower_bound} is the regression of $d$ subproblems of dimension $n' \times 1$ for $n'=n/d$. First of all, let us consider the 1-dimensional subproblem. 

For the 1-dimensional problem, we will use the following two distributions to generate $y' \sim \{\pm 1\}^{n'}$ in this proof:
\begin{enumerate}
\item $D_1:$ Each label $y'_i=1$ with probability $1/2+\epsilon$ and $-1$ with probability $1/2-\epsilon$.
\item $D_{-1}:$ Each label $y'_i=1$ with probability $1/2-\epsilon$ and $-1$ with probability $1/2+\epsilon$.
\end{enumerate}
\begin{claim}\label{clm:distinguish_two_dist}
Let $d=1$, $n' \ge 100 \log d/\epsilon^2$, and $A'=[1,\ldots,1]^{\top}$ whose dimension is $n' \times 1$. Let $L(\beta)$ for $\beta \in \mathbb{R}$ denote $\|A' \beta - y'\|_1$ for $y' \in \mathbb{R}^{n'}$ and $\beta^*$ be the minimizer of $L(\beta)$. If $y'$ is generated from $D_1$, with probability $1-\frac{1}{100d}$, $\beta^*=1$ and any $(1+\eps)$-approximation $\tilde{\beta}$ will be positive. 

Similarly, $\beta^*=-1$ and $\tilde{\beta}$ will be negative with probability $1-\frac{1}{100d}$ when $y' \sim D_{-1}$.
\end{claim}
\begin{proof}
Let us consider $y'$ generated from $D_1$ of dimension $n' \ge 100 \log d/\epsilon^2$. Let $n_1$ and $n_{-1}$ denote the number of $1$s and $-1$s in $y'$. From the standard concentration bound, with probability $1-\frac{1}{100 d}$, we have $n_1 \ge (1/2+\epsilon/2)n'$ for a random string $y'$ from $D_1$. So for the 1-dimensional problem where $A'=[1,\ldots,1]^{\top}$, $\beta^*$ minimizing $\|A' \beta-y'\|_1$ will equal $1$ with loss $L(\beta^*):=\|A' \beta^* - y'\|_1 = 2 \cdot n_{-1} \le (1-\epsilon)n$. Now let $\tilde{\beta}$ be an $(1+\eps)$-approximation solution, i.e., $\|A' \tilde{\beta}- y'\|_1$ is at most $(1+\epsilon) \cdot \|A' \beta^* - y\|_1$. 

We show $\tilde{\beta}$ must be positive when $n_1 \ge (1/2+\epsilon/2)n$. If $\tilde{\beta} \in [0,-1)$, the $\ell_1$ loss of $\tilde{\beta}$ is
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (1+\tilde{\beta}) = n - \tilde{\beta} (n_1 - n_{-1}) \ge n.
\]
Otherwise if $\tilde{\beta} \le -1$, the $\ell_1$ loss of $\tilde{\beta}$ becomes
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (-\tilde{\beta} - 1)=-\tilde{\beta} \cdot n + n_1 - n_{-1} \ge n.
\]
In both cases, $L(\tilde{\beta})$ does not give an $(1+\epsilon)$-approximation of $L(\beta^*)$. Similarly when $y$ is generated from $D_{-1}$ and $n_{-1} \ge (1/2+\epsilon/2)n$, $\tilde{\beta}$, which is an $(1+\eps)$-approximation of $\beta^*$, must be negative.
\end{proof}

The proof of Theorem~\ref{thm:information_lower_bound} relies on the following lower bound of dimension 1.
\begin{lemma}\label{lem:information_lower_dim_1}
Let us consider the following game between Alice and Bob:
\begin{enumerate}
\item Let Alice generate $\alpha \sim \{\pm 1\}$ randomly.
\item Then Alice generates $y \in \{\pm 1\}^{n'}$ from $D_\alpha$ defined above and allows Bob to query $m'$ entries in $y$.
\item After all queries, Bob wins the game only if he predicts $\alpha$ correctly.
\end{enumerate}
If Bob wants to win this game with probability 0.75, he needs to make $m'=\Omega(1/\eps^2)$ queries.
\end{lemma}
The proof of Lemma~\ref{lem:information_lower_dim_1} follows from the variation distance (a.k.a. statistical distance) or KL divergence. For completeness, we provide a proof in Section~\ref{sec:lower_bound_dim_1}. 

Now we finish the proof of Theorem~\ref{thm:information_lower_bound}. Before delving into the formal proof, we give a high-level overview. First of all, for each string in $b \in \{\pm 1\}^d$, we consider the dimension-$n$ distribution of $y$ generated as $D_b=D_{b_1} \circ D_{b_2} \circ \cdots \circ D_{b_d}$. Namely each chunk of $n/d$ bits of $y$ is generated from $D_1$ or $D_{-1}$ mentioned above. 

The 2nd observation is that when we generate $y \sim D_b$ for a random $b \in \{\pm 1\}^n$, an $(1+\eps)$-approximation algorithm could decode almost all entries of $b$ except a tiny fraction (w.h.p. from Claim~\ref{clm:distinguish_two_dist}). Then we show the existence of $b \in \{\pm 1\}^d$ and its flip $b^{(i)}$ on the $i$th coordinate such that (1) $P$ could decode entry $i$ in $b$ and $b^{(i)}$ (w.h.p. separately) when $y \sim D_b$ or $D_{b^{(i)}}$; (2) $P$ makes $O(m/d)$ queries for entry $i$. Such a pair of $b$ and $b^{(i)}$ provides a good strategy for Bob to win the game in Lemma~\ref{lem:information_lower_dim_1} with $O(m/d)$ queries, which gives a lower bound of $m$.

\begin{proofof}{Theorem~\ref{thm:information_lower_bound}}
For contradiction, we assume there is an algorithm $P$ that outputs an $(1+\eps/200)$-approximation with probability $0.99$ using $m$ queries on $y$. By Yao's minmax principle, we consider a deterministic algorithm $P$ in this proof. Now let us define the distribution $D_b$ over $\{\pm 1\}^n$ for $b \sim \{\pm 1\}^d$ as follows:
\begin{enumerate}
\item We sample $b \sim \{ \pm 1\}^d$.
\item We generate $y \sim D_b$ where $D_b = D_{b_1} \circ D_{b_2} \circ \cdots \circ D_{b_d}$.
\end{enumerate}
For any $b$, when $n>100d \log d/\eps^2$ and $n'=n/d$, with probability $0.99$, for each $i \in [d]$, $D_{b_i}$ will generate $n'$ bits where the number of bits equaling $b_i$ is in the range of $[(1/2+\eps/2)n',(1/2+3\eps/2)n']$. We assume this condition in the rest of this proof. From Claim~\ref{clm:distinguish_two_dist}, $\beta^*$ minimizing $\|A \beta - y\|_1$ will have $b_i=\sign(\beta^*_i)$ for every $i \in [d]$. 

Next, given $A$ and $y$, let $\tilde{\beta}$ be the output of $P$. We define $b'$ as $b'_i=\sign(\tilde{\beta}_i)$ for each coordinate $i$.
We show that $b'$ will agree with $b$ (the string generated $y$) on 0.99 fraction of bits when $P$ outputs an $(1+\eps/200)$-approximation. As discussed before, $\|A \tilde{\beta}-y\|_1$ is the summation of $d$ subproblems for $d$ coordinates separately, i.e., $L_1(\tilde{\beta}_1),\ldots,L_d(\tilde{\beta}_d)$. In particular, $\|A \tilde{\beta}-y\|_1 \le (1+\epsilon/200)\|A \beta^*-y\|_1$ implies
\[
\sum_{i=1}^d L_i(\tilde{\beta}_i) \le (1+\eps/200) \sum_{i=1}^d L_i(\beta^*_i).
\]
At the same time, we know $L_i(\tilde{\beta}_i) \ge L_i(\beta^*_i)$ and $L_i(\beta^*_i) \in [(1-\eps)n',(1-3\eps)n']$ for any $i$. This implies that for at least $0.99$ fraction of $i \in [d]$, $L_i(\tilde{\beta}_i) \le (1+\eps) L_i(\beta^*_i)$: Otherwise the approximation ratio is not $(1+\eps/200)$ given
\begin{equation}
0.99 \cdot (1-\eps)+0.01 \cdot (1-3\eps) \cdot (1+\eps) > (1+\eps/200) \cdot \big( 0.99 \cdot (1-\eps)+0.01(1-3\eps) \big).
\end{equation}
 From Claim~\ref{clm:distinguish_two_dist}, for such an $i$, $\tilde{\beta}_i$ will have the same sign with $\beta^*_i$. So $b'$ agree with $b$ on at least $0.99$ fraction of coordinates. 

For each $b$, let $m_i(b)$ denote the expected queries of $P$ on $y_{(i-1)(n/d)+1},\ldots,y_{i(n/d)}$ (over the randomness of $y \sim D_b$). Since $P$ makes at most $m$ queries, we have $\E_y[\sum_i m_i(b)] \le m$.

Now for each $b \in \{\pm 1\}^d$ and coordinate $i \in [d]$, we say the coordinate $i$ is good in $b$ if (1) $\E_{y}[m_i(b)] \le 60m/d$; and (2) $b'_i=b_i$ with probability 0.8 over $y \sim D_b$ when $b'=\sign(\tilde{\beta})$ defined from the output $\tilde{\beta}$ of $P$. 

Let $b^{(i)}$ denote the flip of $b$ on coordinate $i$. The main thing we plan to show is the existence of $b$ and a coordinate $i \in [d]$ such that $i$ is good in both $b$ and $b^{(i)}$: Let us consider the graph $G$ on the Boolean cube that corresponds to $b \in \{ \pm 1\}^d$ and has edges of $(b, b^{(i)})$ for every $b$ and $i$. To show the existence, for each edge $(b,b^{(i)})$ in the Boolean cube, we remove it if $i$ is not good in $b$ or $b^{(i)}$. We prove that after removing all bad events by a union bound, $G$ still has edges inside. 

For the 1st condition (1) $\E[m_i(b)] \le 60m/d$, we will remove at most $d/60$ edges from each vertex $b$. So the fraction of edges removed by this condition is at most $1/30$. For the 2nd condition, from the guarantee of $P$, we know
\[
\Pr_{b,y}[\tilde{\beta} \text{ is an $(1+\eps/200)$ approximation}] \ge 0.99.
\]
So for $0.8$ fraction of points $b$, we have $\Pr_{y \sim D_b}[\tilde{\beta} \text{ is an $(1+\eps/200)$ approximation}] \ge 0.95$ (Otherwise we get a contradiction given $0.8+0.2 \cdot 0.95=0.99$). Thus, when $y \sim D_b$ for such a string $b$, w.p. 0.95, $b'$ will agree with $b$ on at least $0.99$ fraction of coordinates from the above discussion. In another word, for such a string $b$, \[
\E_{y \sim D_b}[\sum_i 1(b_i=b'_i)] \ge 0.95 \cdot 0.99 n.
\] 
This indicates that there are at least $0.65$ fraction of coordinates in $[d]$ satisfy $\Pr_y[b_i=b'_i] \ge 0.8$ (otherwise we get a contradiction given $0.65+0.35 \cdot 0.8=0.93$). Hence, such a string $b$ will have $0.65$ fraction of \emph{good} coordinates.

Back to the counting of bad edges removed by the 2nd condition, we will remove at most $2 \cdot (0.2+0.8 \cdot 0.35)=0.96$ fraction of edges. Because $1/30+0.96<1$, we know the existence of $b$ and $b^{(i)}$ such that $i$ is a good coordinate in $b$ and $b^{(i)}$.

Now we use $b, b^{(i)}$ and $P$ to construct an algorithm for Bob to win the game in Lemma~\ref{lem:information_lower_dim_1}. From the definition, $P$ makes $60m/d$ queries in expectation on entries $y_{(i-1)(n/d)+1},\ldots,y_{i(n/d)}$ and outputs $b'_i=b_i$ with probability 0.8. Since halting $P$ at $20 \cdot 60m/d$ queries on entries $y_{(i-1)(n/d)+1},\ldots,y_{i(n/d)}$ will reduce the success probability to $0.8-0.05=0.75$. We assume $P$ makes at most $1200m/d$ queries with success probability 0.75. Now we describe Bob's strategy to win the game: 
\begin{enumerate}
\item Randomly sample $b_j \sim \{\pm 1\}$ for $j \neq i$.
\item Simulate the algorithm $P$: each time when $P$ asks the label $y_{\ell}$ for $\ell \in [(j-1)n/d+1,jn/d]$.
\begin{enumerate}
\item If $j=i$, Bob queries the corresponding label from Alice.
\item Otherwise, Bob generates the label using $D_{b_j}$ by himself.
\end{enumerate}
\item Finally, we use $P$ to produce $b' \in \{\pm 1\}^n$ and outputs $b'_i$.
\end{enumerate}
Bob wins this game with probability at least
\[
\Pr_{a}[a=-1] \cdot \Pr_{y}[b'_i=-1] + \Pr_{a}[a=1] \cdot \Pr_{y}[b'_i=1] \ge 0.75
\]
from the properties of $i$ in $b$ and $b^{(i)}$. So we know $1200m/d=\Omega(1/\eps^2)$, which lower bounds $m=\Omega(d/\eps^2)$.
\end{proofof}

\iffalse
We will use a packing set of $b$ in $\{\pm 1\}^d$ in this proof.
\begin{claim}
There exists $\mathcal{F} \subset \{1,2\}^d$  such that
\begin{enumerate}
\item $|\mathcal{F}| \ge 2^{\Omega(d)}$.
\item For each $b \in \mathcal{F}$, $\overline{b} \in \mathcal{F}$ where $\overline{b}$ denotes the flip operation on every coordinate of $b$ from 1 to 2.
\item For two distinct $b$ and $b'$ in $\mathcal{F}$, the Hamming distance between $b$ and $b'$ is at least $0.1 d$.
\end{enumerate}
\end{claim}
\begin{proof}
We can construct $\mathcal{F}$ by the following procedure:
\begin{enumerate}
\item We start with $S=\{\pm 1\}^d$.
\item While $S \neq \emptyset$: 
\begin{enumerate}
\item Add any $b \in S$ and its flip $\ov{b}$ to $S$.
\item Remove all $b' \in S$ with Hamming distance $<0.3d$ to $b$ or $\ov{b}$ in $S$.
\end{enumerate} 
\end{enumerate}
Because we will remove at most $2 \cdot 2^{0.5d}$ strings in $S$ every round, $|\mathcal{F}| = 2^{\Omega(n)}$.
\end{proof}


From all discussion above, we know that with probability $0.79$ over $D$, the algorithm $P$ outputs $\tilde{\beta}$, which could deduce the string $b$ generating $y$. By Fano's inequality \cite{Fano}, this implies the conditional entropy $H(b|\tilde{\beta}) \le H(0.21)+0.21 \cdot \log (|\mathcal{F}|-1)$. So the mutual information of $\tilde{\beta}$ and $b$ is
\[
I(b;\tilde{\beta})=H(b) - H(b|\tilde{\beta})\ge 0.5 \log |\mathcal{F}|=Omega(d).
\]
At the same time, by the data processing inequality, the algorithm $P$ makes $m$ queries and sees $y_{i_1},\ldots,y_{i_m}$, which indicates
\[
I(\tilde{\beta};b)\le I\left((y_{i_1},\ldots,y_{i_m});b\right)=\sum_{}
\]



We finish the proof of Lemma~\ref{lem:information_lower_dim_1} in this section. For contradiction, suppose there is an algorithm $P$ that achieves $(1+\eps)$-approximation with $m=0.01 \cdot \frac{1}{\epsilon^2}$ queries. Now we consider the following two distributions of $y$:
\begin{enumerate}
\item $D_1:$ Each label $y_i=1$ with probability $1/2+\epsilon$ and $-1$ with probability $1/2-\epsilon$.
\item $D_2:$ Each label $y_i=1$ with probability $1/2-\epsilon$ and $-1$ with probability $1/2+\epsilon$.
\end{enumerate}
Let us consider a label string $y$ generated from $D_1$ of dimension $n>10^3/\epsilon^2$. Let $n_1$ and $n_{-1}$ denote the number of $1$s and $-1$s in $y$. From the standard concentration bound, with probability $0.99$, we have $n_1 \ge (1/2+\epsilon/2)n$ for a random string $y$ from $D_1$. So $\beta^*$ minimizing $\|A \beta-y\|_1$ will equal $1$ with $\ell_1$ loss $L(\beta^*)=2 \cdot n_{-1} \le (1-\epsilon)n$. Now let $\tilde{\beta}$ be the output of $P$ satisfying $\|A \tilde{\beta}- y\|_1 \le (1+\epsilon) \cdot \|A \beta^* - y\|_1$. 

We claim $\tilde{\beta}$ must be positive when $n_1 \ge (1/2+\epsilon/2)n$. If $\tilde{\beta} \in [0,-1)$, the $\ell_1$ loss of $\tilde{\beta}$ is
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (1+\tilde{\beta}) = n - \tilde{\beta} (n_1 - n_{-1}) \ge n.
\]
Otherwise if $\tilde{\beta} \le -1$, the $\ell_1$ loss of $\tilde{\beta}$ becomes
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (-\tilde{\beta} - 1)=-\tilde{\beta} \cdot n + n_1 - n_{-1} \ge n.
\]
In both cases, $L(\tilde{\beta})$ does not give an $(1+\epsilon)$-approximation of $L(\beta^*)$. Similarly when $y$ is generated from $D_2$ and $n_{-1} \ge (1/2+\epsilon/2)n$, $\tilde{\beta}$, which is an $(1+\eps)$-approximation of $\beta^*$, must be negative.

Now let us consider the following game between Alice and Bob:
\begin{enumerate}
\item Let Alice generate $b \sim \{1,2\}$ randomly.
\item Then Alice generates $y$ from $D_b$ and allows Bob to query $m$ entries in $y$.
\item After all queries, Bob wins the game only if he predicts $b$ correctly.
\end{enumerate}

\begin{claim}\label{clm:win_prob}
Let $P$ be an $(1+\eps)$-approximation algorithm with at most $m$ queries and success probability 0.9. Then Bob has a strategy to win the above game with probability at least $0.85$ using $m$ queries.
\end{claim}
\begin{proof}
Bob's strategy is to run the linear regression algorithm $P$ with $A=[1,\ldots,1]^{\top}$ and claim $b=1$ when $\tilde{\beta}>0$ (otherwise claim $b=2$). From the above discussion, when $b=1$, with probability 0.99, $n_1 \ge (1/2+\epsilon)n$ such that any $(1+\eps)$ approximation shall have $\tilde{\beta}>0$. Similarly, when $b=2$, with probability 0.99, $n_{-1} \ge (1/2+\epsilon)n$ such that any $(1+\eps)$ approximation shall have $\tilde{\beta}<0$.

This indicates that Bob wins the game when (1) $P$ gives an $(1+\eps)$ approximation and (2) $n_1 \ge (1/2+\epsilon)n$ for $b=1$ or $n_{-1} \ge (1/2+\epsilon)n$ for $b=2$. So Bob wins the game with probability at least $0.85$ by a union bound.
\end{proof}
\fi

\subsection{Lower bound for one dimension}\label{sec:lower_bound_dim_1}
To finish the proof of Lemma~\ref{lem:information_lower_dim_1}, we show that Bob has no strategy to win the game with probability more than $0.75$ when $m \le \frac{0.001}{\eps^2}$. By Yao's minmax principle, we assume Bob's strategy is deterministic. Since Bob only receives a string $r$ of length $m$ from Alice, Bob's strategy depends on $r \in \{\pm 1\}^m$. So the best strategy of Bob to win the game is to compare $\Pr_{D_1}[r]$ and $\Pr_{D_{-1}}[r]$: If $\Pr_{D_b}[r]$ is larger, Bob claims $b$. 

For convenience, let $T \subseteq \{\pm 1\}^m$ denote those strings $r$ with $\Pr_{D_1}[r] \ge \Pr_{D_{-1}}[r]$. So Bob wins the game with probability 
\[
\frac{1}{2} \cdot \bigg( \Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] \bigg).
\]
Then the advantage comparing to randomly guess $b \sim \{1,2\}$ is 
\begin{align*}
& \frac{1}{2} \cdot \bigg( \Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] \bigg) - \frac{1}{2} \\
= & \frac{1}{2} \bigg( \Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] - 1 \bigg)\\
= & \frac{1}{2} \bigg( \Pr_{r \sim D_1}[r \in T] - \frac{\Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_1}[r \not\in T]}{2} + \Pr_{r \sim D_{-1}}[r \notin T] - \frac{\Pr_{r \sim D_{-1}}[r \in T] + \Pr_{r \sim D_{-1}}[r \not\in T]}{2}\bigg)\\
= & \frac{1}{4} \bigg( \Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] - \Pr_{r \sim D_1}[r \notin T]\bigg)\\
= & \frac{1}{2} \bigg( \Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T]\bigg).
\end{align*}
Furthermore, by the definition of $D_1$ and $D_{-1}$, $T=\{r| \sum_i r_i >0 \}$. In the rest of this proof, we will show $\Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T] \le 0.5$. For convenience, we assume $m$ is even:
\begin{align*}
& \Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T]\\
=& \sum_{\text{even } j \in [2,\ldots,m]} (1/2+\epsilon)^{\frac{m+j}{2}} \cdot (1/2+\epsilon)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}} - (1/2-\epsilon)^{\frac{m+j}{2}} \cdot (1/2+\epsilon)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}}\\
=& \sum_{\text{even } j \in [2,\ldots,m]} (1/4-\epsilon^2)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}}  \cdot \bigg( (1/2+\epsilon)^{j} - (1/2-\epsilon)^{j}  \bigg)\\
=& \sum_{\text{even } j \le 3 \sqrt{m}} (1/4-\epsilon^2)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}}  \cdot \bigg( (1/2+\epsilon)^{j} - (1/2-\epsilon)^{j}  \bigg) + \Pr_{r \sim D_1}[\sum_i r_i > 3 \sqrt{m}].
\end{align*}
By the Chebyshev inequality, the 2nd probability is upper bounded by $\frac{m \cdot (1-\epsilon^2)}{9m}\le 1/9$. We use the Stirling formula $\sqrt{2 \pi} \cdot n^{n+1/2} \cdot e^{-n}\le n!\le e \cdot n^{n+1/2} \cdot e^{-n}$ to simplify the first probability as
\begin{align*}
& \sum_{\text{even } j \le 3 \sqrt{m}} 2^{-(m-j)} \cdot \frac{e \cdot m^{m+1/2} }{2 \pi \cdot (\frac{m+j}{2})^{\frac{m+j}{2}} \cdot (\frac{m-j}{2})^{\frac{m-j}{2}}} \cdot \bigg( (1/2+\epsilon)^{j} - (1/2-\epsilon)^{j}  \bigg) \\
\le & \sum_{\text{even } j \le 3 \sqrt{m}} \frac{e}{2 \pi} \cdot \frac{\sqrt{m}}{\sqrt{(m+j)/2} \cdot \sqrt{(m-j)/2}} \cdot \bigg( (1+2\epsilon)^{j} - (1-2\epsilon)^{j} \bigg)\\
\le & \frac{e}{\pi \sqrt{m}} \sum_{\text{even } j \le 3 \sqrt{m}} \bigg( (1+2\epsilon)^{j} - (1-2\epsilon)^{j} \bigg).
\end{align*}
Now we use the fact $m=\frac{0.001}{\eps^2}$ and $j\le 3\sqrt{m}<0.1/\eps$ to upper bound it by
\[
\frac{e}{\pi \sqrt{m}} \sum_{\text{even } j \le 3\sqrt{m}} \bigg( (1+2\epsilon)^{j} - (1-2\epsilon)^{j} \bigg) \le \frac{e}{\pi \sqrt{m}} \sum_{\text{even } j \le 3\sqrt{m}} \bigg( 1+3\epsilon \cdot j - (1-2\epsilon \cdot j) \bigg).
\]
This is at most $\frac{e}{\pi \sqrt{m}} \cdot \sum_j 5 \eps \cdot j=\frac{e}{\pi \sqrt{m}} \cdot 5 \eps \cdot (3\sqrt{m}/2) \cdot (3\sqrt{m}+2)/2 \le \frac{13e}{\pi} \cdot \eps \cdot \sqrt{m} \le 0.38$ when $\eps<0.01$. From all discussion above, we have
\[
\Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T] \le 0.38+1/9 = 0.5.
\]

\iffalse

\textcolor{blue}{In my understanding, the right goal would be to show $m=\Omega(1/\eps^2)$ for $\|A \tilde{\beta}-y\|_1 \le (1+\eps) \cdot \|A \beta^* - y\|_1$. But I donot how to do it at this moment.}

First we argue that $\tilde{\beta}$ satisfying $\|A \tilde{\beta}-A \beta^*\|_1 \le \eps \cdot \|A \beta^* - y\|_1$ must have $\sign(\tilde{\beta})=\sign(\beta_0)$ for the $\beta_0$ generating $y$. When $n>>1/\epsilon^2$, we have $|\beta_0-\beta^*| \le O(\frac{1/\eps}{\sqrt{n}})$ because $\beta^*=median(y_1,\ldots,y_n)$. At the same time, we know $\|A \beta_0 - y\|_1 = \sqrt{\pi/2} \cdot 1/\eps \cdot n + o(n)$. This further implies $\|A \beta^* - y\|_1 \ge \|A \beta_0 - y\|_1 - \|A \beta^* - A \beta_0\|_1 = \sqrt{\pi/2} \cdot 1/\eps \cdot n - o(n)$. If $\|A \tilde{\beta}-A \beta^*\|_1 \le \eps/2 \cdot \|A \beta^* - y\|_1$, we have $|\tilde{\beta}-\beta^*| \le \sqrt{\pi/2}/2 - o(1)$. 
%If $\beta^*=1$ and $\tilde{\beta}<1/2$, we have $\|A \tilde{\beta} - y\|_1 \ge \|A \beta^* - y\|_1 + \frac{1}{2} \cdot (|\{g_i \ge 0\}|-|\{g_i < - 1/4\}|) \ge \sqrt{2/\pi} \cdot 1/\eps \cdot n + \eps n/10$.

The above discussion implies that we can figure out $\beta_0$ given $\tilde{\beta}$. In the language of information theory, this means we could obtain constant information (like 1-bit) about $\beta_0 \sim \{\pm 1\}$. On the other hand, each query of $y_i$ only has information $O(\eps^2)$ by the Shannon-Hartley theorem. These two together imply that $\Omega(1/\eps^2)$ query is necessary.

\begin{theorem}[The Shannon-Hartley Theorem \cite{Hartley,Shannon49}]\label{thm:Shannon_Hartley}
Let $S$ be a real-valued random variable with $\E[S^2]=\tau^2$ and $T \sim N(0,\sigma^2)$. The mutual information $I(S;S+T)\le \frac{1}{2} \log (1+\frac{\tau^2}{\sigma^2})$.
\end{theorem}

\fi