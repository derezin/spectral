%!TEX root = regression.tex
\section{Concentration bound}
We consider the $\ell_1$ loss function $L(\beta)=\|A \beta - y\|_1$ in this section. The main result is to show that with support size $\wt{O}(d/\eps^2)$, $\wt{L}(\beta)$ satisfies the centered uniform approximation in Definition~\ref{def:uniform_approx} such that $\wt{\beta}=\arg\min \wt{L}(\beta)$ provides an $(1+\eps)$-approximation by Lemma~\ref{lem:approx_ratio}.

\begin{theorem}\label{thm:concentration_contraction}
Given $p=1$, the data matrix $A$, and $\epsilon$, let $(w'_1,\ldots,w'_n)$ be a $\gamma$-approximation of the Lewis weights $(w_1,\ldots,w_n)$ of $A$, i.e., $w'_i \approx_{\gamma} w_i$ for all $i \in [n]$. Let $u = \Theta\left( \frac{\eps^2}{\log \gamma d/\eps} \right)$ and $p_i=\gamma w'_i/u$ such that for each $i\in [n]$, with probability $p_i$, we set $s_i=1/p_i$ (when $p_i \ge 1$, $s_i$ is always 1). 

Let $y$ be a fixed unknown vector. With probability $0.8$ over $s$, $\wt{L}(\beta):=\sum_{i=1}^n s_i \cdot |a_i^\top \beta - y_i|$ satisfies centered uniform approximation. Namely, 
\[
\wt{L}(\beta)-\wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \epsilon L(\beta) \text{ for all } \beta.
\]
\end{theorem}

First of all, we bound the support size of $s$. Since $\sum_i w_i=d$ and $w'_i \approx_{\gamma} w_i$, let $m:=\sum_i p_i$, which is equal to $\gamma/u \cdot \sum_i w'_i \le \gamma^2 d/u$. So with probability 0.99, the support size of $s$ is $O(m)=O(\frac{d \gamma^2 \log d/\eps}{\eps^2})$. 

%Our goal is to bound the sample complexity $m$ to give concentration bounds of $\delta_i(\beta)-|y_i|$ for all $\beta$. Specifically, for a fixed matrix $A$ and a vector $y$, we want $m$ indices $i_1,\ldots,i_m$ with weight $w_1,\ldots,w_m$ to satisfy
%\begin{equation}\label{prop:concentration}
%\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) = \sum_{j=1}^m w_j \big( \delta_{i_j}(\beta) - |y_{i_j}| \big) \pm \eps \cdot \sum_{i=1}^n \bigg( |y_i| + |\delta_i (\beta)| \bigg) \text{ for all } \beta.
%\end{equation}


%be the sequence of probabilities
%\[
%q_i \ge \ov{w}_i/u \text{ for } u =\Theta\left( \frac{\eps^2}{\log (m/\eps+d/\eps)} \right) \text{ where } m = \sum_i q_i.
%\]
\iffalse
\textcolor{red}{Michal:
  One way to write the result in a general form is: Consider a non-negative function $f:\mathbb{R}^n\rightarrow\mathbb{R}_{\geq 0}$ such that for all $\alpha\in\mathbb{R}^n$:
  \begin{align*}
    f(\alpha)\leq \|\alpha\|_1\quad\text{and}\quad \big|f(\alpha)-\|\alpha\|_1\big|\leq K.
  \end{align*}
  Then, our result from Theorem \ref{thm:concentration_contraction} could be written as follows:
  \begin{align*}
    f(SA\beta) = (1\pm\epsilon)\cdot f(A\beta) \pm \epsilon\cdot K\quad\text{for}\quad f(\alpha) = \big|\|\alpha-y\|_1-\|y\|_1\big|,\quad K=2\|y\|_1.
  \end{align*}
Further,  note that for $f(\alpha)=\|\alpha\|_1$ we get $K=0$ and standard $l_1$-concentration.
}

\textcolor{blue}{Xue: I am afraid that this won't work for all $y$ until $m=\Omega(n)$. My point is that in that scenario, we could choose $y$ after fixing $S$, which is the main trouble. For example, suppose the Lewis weight is uniform and we generate $S$ with support size $m=o(n)$. Now we only consider a fix $\beta$ such that $\sum_{j: s_j=0} |a_j^{\top} \beta| \ge \frac{1}{2} \|A \beta\|_1$. Then I could choose $y$ such that $y_j=a_j^{\top}\beta/2$ for $j$ with $s_j\neq 0$ and $y_j=0$ otherwise. So $\sum_{j=1}^m w_j \big( \delta_{i_j}(\beta) - |y_{i_j}| \big)=0$ but $\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big)\ge \frac{1}{2}\|A \beta\|_1$.} 
\fi

In the rest of this section, we show the centered uniform approximation property to finish the proof of Theorem~\ref{thm:concentration_contraction}. Before showing the formal proof in Section~\ref{sec:concentration_contraction}, we discuss the main technique tool --- a concentration bound for all $\beta$ with bounded $\|A (\beta-\beta^*)\|_1$. 

\begin{theorem}\label{thm:concentration_deviation}
Given $A \in \mathbb{R}^{n \times d}$ and $\epsilon$, let $w_1,\ldots,w_n$ be the Lewis weight of each row $A_i$. Let $p_1,\ldots,p_n$ be a sequence of numbers upper bounding $w$, i.e.,
\[
p_i \ge w_i/u \text{ for } u =\Theta\left( \frac{\eps^2}{\log (m+d/\eps)} \right) \text{ where } m = \sum_i p_i.
\]
Suppose the coefficients $(s_1,\ldots,s_n)$ in $\wt{L}(\beta):=\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|$ is generated as follows: For each $i \in [n]$, with probability $p_i$, we set $s_i=1/p_i$ (when $p_i \ge 1$, $s_i$ is always 1). Given any subset $B_{\beta} \subset \mathbb{R}^d$ of $\beta$,
\[
\E \left[ \sup_{\beta \in B_{\beta}} \left\{ \wt{L}(\beta)-\wt{L}(\beta^*)-\big( L(\beta)-L(\beta^*) \big) \right\} \right] \le \eps \cdot \sup_{\beta \in B_{\beta}} \|A (\beta-\beta^*)\|_1.
\]
%\E \left[ \sup_{\beta \in B_{\beta}} \left\{ \sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \right\} \right] \le \eps \cdot \sup_{\beta \in B_{\beta}} \|A \beta\|_1.
%Namely if we use $B_{\beta}$ to denote the set of $\beta$ --- $\{\beta|\sum_{i=1}^n \delta_i(\beta) \le B \sum_{i=1}^n |y_i|\}$, \[\E_S\left[ \sup_{\beta \in B_{\beta}}\bigg| \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) - \sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg| \le \eps \cdot \sum_{i=1}^n |y_i|\right]\]
\end{theorem}
To prove the main Theorem~\ref{thm:concentration_contraction}, we could apply this Theorem to $B_{\beta}:=\big\{ \beta \big| \|A (\beta-\beta^*)\|_1 \le 3 L(\beta^*) \big\}$ to conclude the error is at most $\epsilon \cdot \|A (\beta-\beta^*)\|_1 \le 3 \epsilon L(\beta^*)$, which is at most $3 \epsilon L(\beta)$ by the definition of $\beta^*$. However, this only gives the centered approximation for bounded $\beta$. To bound the error in terms of $\eps L(\beta)$ for \emph{all} $\beta \in \mathbb{R}^d$, we will partition $\beta \in \mathbb{R}^d$ into several subsets and apply Theorem~\ref{cor:high_prob_guarantee} to those subsets separately. So we need the guarantee of Theorem~\ref{thm:concentration_deviation} holds for multiple subsets simultaneously.

While Theorem~\ref{thm:concentration_deviation} implies that after rescaling $u$ to $u=u/\delta^2$, with probability $1-\delta$,
\[
\wt{L}(\beta)-\wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot \sup_{\beta \in B_{\beta}}\|A (\beta-\beta^*)\|_1 \text{ for all } \beta \in B_{\beta},
\] 
we state the following corollary for a better dependency on $\delta$. 
\begin{corollary}\label{cor:high_prob_guarantee}
Given $A \in \mathbb{R}^{n \times d}$, $\epsilon$ and $\delta$, let $w_1,\ldots,w_n$ be the Lewis weight of each row $A_i$. Let $p_1,\ldots,p_n$ be a sequence of numbers upper bounding $w$, i.e.,
\[
p_i \ge w_i/u \text{ for } u =\Theta\left( \frac{\eps^2}{\log (m/\delta+d/\eps\delta)} \right) \text{ where } m = \sum_i p_i.
\]
Suppose the coefficients $(s_1,\ldots,s_n)$ in $\wt{L}(\beta):=\sum_{i=1}^n s_i |a_i^{\top} \beta - y_i|$ is generated as follows: For each $i \in [n]$, with probability $p_i$, we set $s_i=1/p_i$ (when $p_i \ge 1$, $s_i$ is always 1). Given any subset $B_{\beta} \subset \mathbb{R}^d$ of $\beta$, with probability at least $1-\delta$,
\[
\wt{L}(\beta)-\wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot \sup_{\beta \in B_{\beta}}\|A (\beta-\beta^*)\|_1 \text{ for all } \beta \in B_{\beta}.
\]
\end{corollary}

In the rest of this section, we will use the following bounds about $\delta=O(\eps)$, $m:=\sum_i p_i \le \gamma^2 d / u$ and $u$ in Theorem~\ref{thm:concentration_contraction}: Since $\log m/\eps=\Theta(\log d\gamma /\eps)$, we can choose the constant of $u$  to be small enough such that for a given constant $C$,
\begin{equation}\label{eq:density_u}
u \le \frac{\eps^2}{C \log (m/\eps +d/\eps)} \text{ and } p_i \ge w_i/u.
\end{equation}

Moreover, because we can always shift $y$ to $y- A \beta^*$ and $\beta^*$ to $\vec{0}$, we will assume $\beta^*=0$ in this proof such that $\wt{L}(\beta^*)=\sum_i s_i \cdot |y_i|$ and $L(\beta^*)=\sum_i |y_i|$. For convenience, we define 
\[
\delta_i(\beta):=|a_i^{\top} \beta - y_i| \text{ such that } \wt{L}(\beta)-\wt{L}(\beta^*)=\sum_i s_i (\delta_i(\beta)-|y_i|)
\] given the assumption $\beta^*=0$ and vice versa for $L(\beta)-L(\beta^*)$. 

We defer the proof of Theorem~\ref{thm:concentration_contraction} to Section~\ref{sec:concentration_contraction}. The proof of Theorem~\ref{thm:concentration_deviation} and the proof of Corollary~\ref{cor:high_prob_guarantee} are based on the chaining arguments introduced in \cite{CP15_Lewis} and Gaussian comparison theorem~\cite{LTbook}, which will be deferred to Section~\ref{sec:proof_thm_deviation} and Section~\ref{sec:proof_cor_high_prob} separately.

\subsection{Proof of Theorem~\ref{thm:concentration_contraction}}\label{sec:concentration_contraction}
Recall that we assume $\beta^*=0$ and $L(\beta)^*=\|y\|_1$. First of all, notice that
\[
\E_S[\sum_{j=1}^n s_j \cdot |y_j|]=\sum_i |y_i|.
\]
By the Markov inequality, $\E_S[\sum_{j=1}^n s_j \cdot |y_j|] \le 12 \|y\|_1$ holds with probability $1-1/12$. 

We apply Corollary~\ref{cor:high_prob_guarantee} multiple times with different choices of $B_{\beta}$. We choose $t=O(1/\eps^2)$ and $B_i=\big\{ \beta \big| \|A \beta\|_1 \le (3+\eps \cdot t) \|y\|_1 \big\}$ for $i=0,1,\ldots,t$. Then we apply Corollary~\ref{cor:high_prob_guarantee} with $\delta=\frac{1}{100t}$ to guarantee that with probability $0.99$, for all $B_i$, we have (recall the assumption $\beta^*=0$)
\[
\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) = \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big)  \pm \eps \cdot \sup_{\beta \in B_i} \|A \beta\|_1 \text{ for all } \beta \in B_i.
\]
Further more, with probability $0.99$ over $S$, we have the $\ell_1$ concentration:
\[
\forall \beta, \|S A \beta\|_1 = (1\pm \eps) \cdot \|A \beta\|_1.
\]

Now we argue the concentration holds for all $\beta$ after rescaling $\eps$ by a constant factor. 
\begin{enumerate}
\item $\beta$ has $\|A \beta\|_1 < 3 \|y\|_1$: From the concentration of $B_0$, the error is at most $\eps \cdot 3 \|y\|_1 \le 3\eps \cdot L(\beta)$.
\item $\beta$ has $\|A \beta\|_1 \in \big[(3+\eps \cdot i) \cdot \|y\|_1, (3+ \eps \cdot (i+1))\cdot \|y\|_1 \big)$ for $i<t$: Let $\beta'$ be the rescaling of $\beta$ with $\|A \beta'\|_1=(3+\eps \cdot i) \|y\|_1$. Then we rewrite $\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big)$ as
\[
\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) = \sum_{i=1}^n \big( \delta_i(\beta') - |y_i| \big) \pm \|A \beta-\beta'\|_1 = \sum_{i=1}^n \big( \delta_i(\beta') - |y_i| \big) \pm \eps \|y\|_1.
\]
Similarly, we rewrite the left hand side as
\begin{align*}
\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) & =\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta') - |y_{j}| \big) \pm \|S A (\beta-\beta')\|_1 \\
& =\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta') - |y_{j}| \big) \pm (1+\eps ) \cdot \|A(\beta-\beta')\|_1,
\end{align*}
where we use the $\ell_1$ concentration in the last step. Next we use the guarantee of $\beta'$ to bound the error to be $O(\eps) \cdot (\|A \beta\|_1 +\|y\|_1) = O(\eps) \cdot L(\beta)$ since $L(\beta) \ge \|A \beta\|_1/2$ by the triangle inequality.
\item $\beta$ has $\|A \beta\|_1 \ge \frac{25}{\eps} \cdot \|y\|_1$: We always have $\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) = \|A \beta\|_1 \pm 2 \|y\|_1$. On the other hand, the $\ell_1$ concentration and the norm of $\|Sy\|_1$ imply $\|SA\beta\|_1 - \|S y\|_1 = (1 \pm \eps)\|A \beta\|_1 \pm 12 \|y\|_1$. Since $\|A \beta\|_1 \ge \frac{25}{\eps} \cdot \|y\|_1$, we have
\[ \sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) = \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \pm 2\eps \|A \beta\|_1.
\]
Again, the error $2\eps \|A \beta\|_1 = O(\eps) \cdot L(\beta)/2$ by the triangle inequality.
\end{enumerate}

\subsection{Proof of Theorem~\ref{thm:concentration_deviation}}\label{sec:proof_thm_deviation}
We need a few ingredients about Gaussian process to finish the proof of Theorem~\ref{thm:concentration_deviation}.
We will use the following Gaussian comparison theorem for Gaussian processes (e.g. Theorem 7.2.11 and Exercise 8.6.4 in \cite{Vershynin}).
\begin{theorem}[Slepian-Fernique]\label{thm:Slepian_Fernique}
Let $v_0,\ldots,v_n$ and $u_0,\ldots,u_n$ be two sets of vectors in $\mathbb{R}^d$ where $v_0=u_0=\vec{0}$. Suppose that 
\[
\|v_i-v_j\|_2 \ge \|u_i-u_j\|_2 \text{ for all } i,j=0,\ldots,n.
\]
Then $\E_g\left[\max_i \big| \langle v_i, g \rangle \big| \right] \ge C_0 \cdot \E_g \left[\max_i \big| \langle u_i,g \rangle \big| \right]$ for some constant $C_0$.
\end{theorem}
Moreover, we will compare the higher moments for a better concentration in Corollary~\ref{cor:high_prob_guarantee}.
\begin{corollary}[Corollary 3.17 of \cite{LTbook}]\label{cor:comparison_higher}
Let $v_0,\ldots,v_n$ and $u_0,\ldots,u_n$ be two sets of vectors satisfying the conditions in the above Theorem. Then for any $\ell>0$, 
\[4^{\ell} \cdot \E_g\left[\max_i \big| \langle v_i, g \rangle \big|^{\ell} \right] \ge \E_g \left[\max_i \big| \langle u_i,g \rangle \big|^{\ell} \right].\]
\end{corollary}

We will use the following concentration bound for $\ell_1$ ball (e.g., Lemma 8.2 in \cite{CP15_Lewis}) when the Lewis weight is small for all rows. For completeness, we provide a proof to Section~\ref{sec:ell_1_concentration}.
\begin{lemma}\label{lem:Gaussian_proc_bounded_Lewis}
Let $A$ be a matrix with Lewis weight upper bounded by $u$. For any set $S \subseteq \mathbb{R}^{d}$,
\[
\E_g\left[\max_{\beta \in S} \bigg| \langle g, A \beta \rangle \bigg| \right] \lesssim \sqrt{u \cdot \log n} \cdot \max_{\beta \in S} \|A \beta\|_1.
\]
\end{lemma}
The last ingredient is the following higher moments bound for Guassian process (e.g., Exercise 8.6.6 in~\cite{Vershynin}).
\begin{lemma}\label{lem:Gaussian_high_moment}
Let $T$ be a subset of $\mathbb{R}^n$. For any $p>1$, we always have 
\[\E_g[ \sup_{x \in T} |\langle g,x\rangle|^p]^{1/p} \le C \sqrt{p} \cdot \E_g[ \sup_{x \in T} |\langle g,x\rangle|].\]
\end{lemma}

We finish the proof of Theorem~\ref{thm:concentration_deviation} here and defer the proof of Corollary~\ref{cor:high_prob_guarantee} to Section~\ref{sec:proof_cor_high_prob}.
%we plan to bound the deviation \[
%\wt{L}(\beta)-\wt{L}(\beta^*)=\bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|\] for all $\beta \in B_{\beta}$ simultaneously. In particular, 
\begin{proofof}{Theorem~\ref{thm:concentration_deviation}}
In this proof, we rewrite the expectation on L.H.S. as
\[
\E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \bigg| \right].
\]
Since $\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big)$ is the expectation, from the standard symmetrization and Gaussinization \cite{LTbook} (which is shown in  Section~\ref{sec:sym_gau} for completeness), this is upper bounded by
\[
\sqrt{2 \pi} \E_S \left[ \E_{g} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right] \right].
\]

Then we fix $S$ and plan to apply the Gaussian comparison Theorem~\ref{thm:Slepian_Fernique} to the following process.
\[
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j} g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right] = \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \bigg\langle g, \big(s_j \cdot ( \delta_{j}(\beta) - |y_{j}|)\big)_{j \in [n]} \bigg\rangle \bigg| \right].
\]
Now we verify the condition of the Gaussian comparison Theorem~\ref{thm:Slepian_Fernique} to upper bound it by $C_2 \cdot \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \bigg\langle g, SA\beta \bigg\rangle \bigg| \right]$. Note that for any $\beta$ and $\beta'$, the $j$th term of $\beta$ and $\beta'$ in the above Gaussian process is upper bounded by
\[
\left| s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - s_j \cdot \big( \delta_{j}(\beta') - |y_{j}|\big) \right| \le s_j \cdot \left| \delta_j(\beta)-\delta_j(\beta') \right| \le s_j \cdot \left| a_j^{\top} \beta - a_j^{\top} \beta' \right|.
\]
At the same time, for the $\vec{0}$ vector, we always have
\[
\left| s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - 0 \right| \le s_j \cdot \left| |a_j^{\top} \beta - y_j| - |y_j| \right| \le s_j \cdot |a_j^{\top} \beta|.
\]
Hence, the Gaussian comparison Theorem~\ref{thm:Slepian_Fernique} implies that the Gaussian process is upper bounded by
\[
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j} g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right] \le \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, S A \beta \rangle \bigg| \right].
\]

Before we apply Lemma~\ref{lem:Gaussian_proc_bounded_Lewis} to bound it, we need an extra property that $S A$ has bounded Lewis weight. Similar to the proof of Lemma~7.4 in \cite{CP15_Lewis}, we add a matrix $A'$ into the Gaussian process to have bounded Lewis weight. Recall that $u \le \frac{\eps^2}{C \log (m + d/\eps)}$ for a large constant $C$. By sampling the rows in $A$ with the Lewis weight and scaling every sample by a factor of $u$, we have the following properties of $A'$ (see Lemma B.1 in \cite{CP15_Lewis} for the whole argument using properties of the Lewis weight),
\begin{enumerate}
\item $A'$ has $\tilde{O}(d/u)$ rows and each row has Lewis weight at most $u$.
\item The Lewis weight $W'$ of $A'$ satisfies $A'^{\top} \ov{W'}^{1-2/p} A' \succeq A^{\top} \ov{W}^{1-2/p} A$.
\item $\|A' x\|_1 = O(\|A x\|_1)$ for all $x$.
\end{enumerate}
Let $A''$ be the union of $SA$ and $A'$ (so the $j$th row of $A''$ is $s_j \cdot a_j$ for $j \le [n]$ and $A'[j,*]$ for $j \ge n+1$). Because the first $n$ entries of $A'' \cdot \beta$ are the same as $SA\beta$ for any $\beta$ and the rest entries can only increase the energy, we have
\[
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, S A \beta \rangle \bigg| \right] \le \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, A'' \cdot \beta \rangle \bigg| \right].
\]

To apply Lemma~\ref{lem:Gaussian_proc_bounded_Lewis}, let us verify the Lewis weight of $A''$ is bounded. First of all, $A''^{\top} \overline{W}''^{1-2/p} A'' \succeq A^{\top} \overline{W}^{1-2/p} A$ from Lemma 5.6 in \cite{CP15_Lewis}. Now for each row $s_j a_j$ in $A''$, its Lewis weight is upper bounded by
\[
\left( \frac{1}{q_j} a_j^{\top} (A^{\top} \overline{W}^{1-2/p} A)^{-1} \cdot \frac{1}{q_j} a_j \right)^{1/2} \le w_j/q_j \le u
\]
by the definition of $q_j$. At the same time, the Lewis weight of $A'$ is already bounded by $u$ from the definition, which shows the rows in $A''$ corresponding to $A'$ are also bounded by $u$. So Lemma~\ref{lem:Gaussian_proc_bounded_Lewis} implies 
\[
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, A'' \beta \rangle \bigg| \right] \le \sqrt{u \log (m+d/u)} \cdot \sup_{\beta \in B_{\beta}} \|A'' \beta\|_1 \le \sqrt{u \cdot \log (m+d/u)} \cdot \left( \sup_{\beta \in B_{\beta}} \|S A \beta\|_1 + O(\sup_{\beta \in B_{\beta}} \|A \beta\|_1) \right).
\]
by the definition of $A''$ and the last property of $A'$. Finally, we bring the expectation of $S$ back to bound $\E_S[\sup_{\beta \in B_{\beta}} \|S A \beta\|_1] \le (1+\eps)\sup_{\beta \in B_{\beta}} \|A \beta\|_1$ by the $\ell_1$ concentration.

From all discussion above, this deviation $\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, A'' \beta \rangle \bigg| \right]$ is upper bounded by $\eps/100 \cdot \sup_{\beta \in B_{\beta}} \|A \beta\|_1$ given our choice of $u$.
\end{proofof} 

\subsection{Symmetrization and Gaussinization}\label{sec:sym_gau}
We start with a standard symmetrization by replacing $\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big)]$ by its expectation $\E_{S'}\bigg[\sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg]$:
\[
\E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \E_{S'}\bigg[\sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg] \bigg| \right].
\]
Using the covexity of the absolute function, we move out the expectation over $S'$ and upper bound this by
\begin{align*}
& \E_{S,S'}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right] \\
= & \E_{S,S'}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right].
\end{align*}
Because $S$ and $S'$ are symmetry and each coordinate is independent, this expectation is equivalent to
\begin{align*}
& \E_{S,S',\sigma}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - \sigma_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right] \\
\le & \E_{S,S',\sigma}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| + \bigg|\sum_{j=1}^n  \sigma_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right]\\
\le & 2 \E_{S,\sigma} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\end{align*}
Then we apply Gaussianization: Since $\E[|g_j|]=\sqrt{2/\pi}$, the expectation is upper bounded by
\[
\sqrt{2 \pi} \E_{S,\sigma} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \E[|g_j|] \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\]
Using the covexity of the absolute function again, we move out the expectation over $g_j$ and upper bound this by
\[
\sqrt{2 \pi} \E_{S,\sigma,g} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n |g_j| \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\]
Now $|g_j| \sigma_j$ is a standard Gaussian random variable, so we simplify it to
\[
\sqrt{2 \pi} \E_{S,g} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\]

\subsection{Proof of $\ell_1$ concentration}\label{sec:ell_1_concentration}
We finish the proof of Lemma~\ref{lem:Gaussian_proc_bounded_Lewis} in this section.
\begin{proof}
%Observe that for each coordinate $i$, 
%\[\left| (\delta_i(\beta)-|y_i|) - (\delta_i(\beta')-|y_i|) \right| \le |a_i^{\top} \beta - a_i^{\top} \beta'|.
%\] So we apply Theorem~\ref{thm:Slepian_Fernique} to the Guassian process of $\big(\delta_i(\beta) - |y_i|\big)_{i \in [n]}$ and $A \beta = \big( a_i^{\top} \beta \big)_{i \in [n]}$:
%\[\E_g\left[\max_{\beta \in B_{\beta}}  \bigg| \sum_i g_i \big( \delta_i(\beta)-|y_i| \big) \bigg| \right] \le C_0 \cdot \E_g \left[ \max_{\beta \in B_{\beta}} \bigg| \langle g, A \beta \rangle \bigg| \right].
%\]

%\textcolor{blue}{Xue: The rest follows from Section 8 of \cite{CP15_Lewis}.} 

%Then we consider the new Gaussian process. 

Since $\langle g, A \beta \rangle$ is equivalent to $\langle \Pi g, A \beta \rangle$ for any projection matrix $\Pi \in \mathrm{R}^{n \times n}$ of the column space spanned by $A$, we consider the weighted projection $A \cdot (A^\top \ov{W}^{-1} A)^{-1} \cdot A^\top \ov{W}^{-1}$. This corresponds to the natural inner product induced by the Lewis weight. Then we upper bound the inner product in the Gaussian process by $\|\cdot\|_1 \cdot \|\cdot\|_{\infty}$:
\[
\E_g \left[ \max_{\beta \in S} \big| \langle \Pi g, A \beta \rangle \big| \right] \le \E_g \left[ \max_{\beta \in S} \|\Pi g\|_{\infty} \cdot \|A \beta\|_1 \right].
\]
%Since $\beta \in B_{\beta}$ always has $\sum_i |a_i^{\top} \beta - y_i| \le B \sum_i |y_i|$, this gives $\|A \beta\|_1=\sum_i |a_i^{\top} \beta|$ is upper bounded $(B+1) \sum_i |y_i|$ by the triangle inequality.

So we further simplify the Gaussian process as
\[
\E_g \left[ \max_{\beta \in B_{\beta}} \|\Pi g\|_{\infty} \right] \cdot \max_{\beta \in S} \|A \beta\|_1 \le \sqrt{2 \log n} \cdot \max_{i \in [n]} \|\Pi_i\|_2 \cdot \max_{\beta \in S} \|A \beta\|_1,
%\E_g \left[ \max_{\beta \in B_{\beta}} \|\Pi g\|_{\infty} \right] \cdot (B+1) \sum_i |y_i| \le \sqrt{2 \log n} \cdot \max_{i \in [n]} \|\Pi_i\|_2 \cdot (B+1) \sum_i |y_i|,
\]
where we observe the entry $i$ of $\Pi g$ is a Gaussian variable with stand deviation $\|\Pi_i\|_2$ and apply a union bound over $n$ Gaussian variables. In the rest of this proof, we bound $\|\Pi_i\|^2_2$:
\begin{align*}
& \sum_j (A \cdot (A^\top \ov{W}^{-1} A)^{-1} \cdot A^\top \ov{W}^{-1})_{i,j}^2 \\
= & \sum_j (\ov{w}_i^{-1} \cdot a_i^{\top} \cdot (A^\top \ov{W}^{-1} A)^{-1} \cdot a_j)^2\\
\le & u \sum_j (\ov{w}_i^{-1} \cdot a_i^{\top} \cdot (A^\top \ov{W}^{-1} A)^{-1} \cdot a_j w_j^{-1/2})^2\\
\le & u \ov{w}_i^{-2} \cdot \sum_j a_i^{\top} \cdot (A^\top \ov{W}^{-1} A)^{-1} \cdot a_j w_j^{-1} a_j^{\top} \cdot (A^\top \ov{W}^{-1} A)^{-1} \cdot a_i\\
\le & u \ov{w}_i^{-2} \cdot a_i^{\top} (A^\top \ov{W}^{-1} A)^{-1} \cdot \left( \sum_j a_j w_j^{-1} a_j^{\top} \right) \cdot (A^\top \ov{W}^{-1} A)^{-1} \cdot a_i \\
\le & u \ov{w}_i^{-2} a_i^{\top} (A^\top \ov{W}^{-1} A)^{-1} \cdot a_i \\
\le & u \ov{w}_i^{-2} \cdot \ov{w}_i^2 = u.
\end{align*}
\end{proof}

\subsection{Proof of Corollary~\ref{cor:high_prob_guarantee}}\label{sec:proof_cor_high_prob}
The proof follows the same outline of the proof of Theorem~\ref{thm:concentration_deviation} except using Corollary~\ref{cor:comparison_higher} to replace Theorem~\ref{thm:Slepian_Fernique} with a higher moment. We choose the moment $\ell=\log 1/\delta$ in the rest of this section.
\begin{align*}
& \E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \bigg|^{\ell} \right] \\
= & \E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \E_{S'} \sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|^{\ell} \right]\\
& (\notag{\text{Use the convexity of $|\cdot|^{\ell}$ to move $\E_{S'}$ out}})\\
\le & \E_{S,S'}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|^{\ell} \right]\\
& (\notag{\text{Use the symmetry of $S$ and $S'$}})\\
\le & \E_{S,S',\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot (s_j-s'_j) \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right]\\ 
& (\notag{\text{Split $S$ and $S'$}})\\
\le & \E_{S,S',\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{j=1}^n \epsilon_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|^{\ell} \right]\\
& (\notag{\text{Pay an extra $2^{\ell}$ factor to bound the cross terms}})\\
\le & 2^{\ell} \cdot \E_{S,\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right] + 2^{\ell} \cdot \E_{S',\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right]\\
\le & 2^{\ell+1} \cdot \E_{S,\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right]\\
& (\notag{\text{Gaussianize it}})\\
\le & 2^{\ell+1} \cdot \sqrt{\pi/2}^{\ell} \cdot \E_{S,g}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right].
\end{align*}
Then we replace the vectors $\big(s_j \cdot (\delta_j(\beta)-|y_j|)\big)_j$ by $SA\beta$ using the Gaussian comparison Corollary~\ref{cor:comparison_higher} (we omit the verification here because it is the same as the verification in the proof of Theorem~\ref{thm:concentration_deviation}).
\[
C_1^{\ell} \cdot \E_S \E_g \left[ \sup_{\beta}  \bigg| \langle g, SA\beta \rangle \bigg|^{\ell}\right].
\]
The proof of Lemma~7.4 in \cite{CP15_Lewis} shows 
\[
\E_S \E_g \left[ \sup_{\beta}  \bigg| \langle g, SA\beta \rangle \bigg|^{\ell}\right] \le C_2^{\ell} \cdot \eps^{\ell} \cdot \delta \cdot 
\sup_{\beta \in B_{\beta}} \|A \beta\|_1^{\ell}.
\]
From all discussion above, we have
\[ \E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \bigg|^{\ell} \right] \le (C_1 C_2)^{\ell} \cdot \eps^{\ell} \delta \cdot \sup_{\beta \in B_{\beta}} \|A \beta\|_1^{\ell}.\]
This implies with probability $1-\delta$, $\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) = \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \pm (C_1 C_2) \cdot \eps \sup_{\beta \in B_{\beta}} \|A \beta\|_1$ for all $\beta \in B_{\beta}$. Finally, we finish the proof by rescaling $\eps$ by a factor of $C_1 \cdot C_2$.

\iffalse
\paragraph{\textcolor{blue}{Xue: This is the plan in my mind. Please take it as a grain of salt because we may need to adjust parameters and inequalities later.}} We will start by reproving the results in Section 8 of \cite{CP15_Lewis}. Let $u$ be an upper bound on the Lewis weight of $A$. For example, let us set $u=d/n$ such that the Lewis weight is uniform to gain some intuition. Then Lemma 8.4 in \cite{CP15_Lewis} indicates that when we randomly pick each coordinate $i$ with probability $1/2$ (say the moment $\ell=O(\log n/\delta)$, $\sigma_i=1$ means we keep it otherwise $\sigma_i=-1$ means we discard it), with high probability, 
\[
\max_{x: \|Ax\|_1=1} \sum_{i=1}^n \frac{\sigma_i+1}{2}  \cdot |a_i^{\top} x| \le \frac{1}{2} + O(\sqrt{\frac{d \log n}{n}}).
\]
If we choose $\eps=O(\sqrt{\frac{d \log n}{n}})$, this means $m \approx n/2=\Theta(d \log n/\eps^2)$ is enough to give the concentration for all $x$. This saves an extra $d$ factor compare to the union bound. Formally Lemma 7.4 in \cite{CP15_Lewis} shows how to turn Lemma 8.4 (which samples about half rows) into a concentration bound for any number $m$.

Back to our problem, instead of considering the ball $\bigg\{x \bigg| \|Ax\|_1=1\bigg\}$, we will consider the ball $B_{\beta}:=\bigg\{ \beta \bigg | \sum_{i=1}^n \delta_i(\beta) \le B \sum_{i=1}^n |y_i| \bigg\}$. Furthermore, after normalizing, the error term in \eqref{prop:concentration} is exactly $\eps/B$ which corresponds to error $O(\sqrt{\frac{d \log n}{n}})$ in $\ell_1$ concentration. 

However, one big change is that we will use $g \sim N(0,1)^n$ instead of $\sigma \in \{\pm 1\}^n$ in this calculation. The main reason is that we need some comparison theorem (for our choice of $\delta_i(\beta)$ rather than $|a_i^\top x|$), which I only know the version for Gaussian variables and may be false for subGaussian variables. In the rest of this section, we plan to reprove Lemma 8.4 and 7.4 in \cite{CP15_Lewis}.
\fi