\documentclass{beamer}
%\beamertemplateshadingbackground{brown!70}{yellow!10}
\mode<presentation>
{
    %\usetheme{Warsaw}
    \usecolortheme{crane}
    % or ...

    \setbeamercovered{transparent}
    % or whatever (possibly just delete it)
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]{}

\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usepackage{mathtools}
\usepackage{relsize}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{colortbl}
%\usepackage{multicol}
\usepackage{cancel}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{xfrac}
\usepackage{forloop}% http://ctan.org/pkg/forloop
\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}
}

\input{../shortdefs}
\renewcommand{\Xinv}{(\X^\top\X)^{-1}}
\renewcommand{\Xinvr}{(\lambda\I+\X_{-1}^\top\X_{-1})^{-1}}

\title[]{Loss bounds for volume sampling}

\author[]{Micha{\l } Derezi\'{n}ski}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Rescaled volume sampling}

\begin{frame}
\frametitle{Rescaled volume sampling}
Let $\X\in\R^{n\times d}$ be any matrix and $q = (q_1...q_n)$ be any distribution\\
For any $\pi\in[n]^k$, define matrix $\Q_\pi\in\R^{n\times n}$ s.t.
\vspace{-2mm}
\[
\Q_\pi=\sum_{i=1}^k\frac{1}{q_{\pi_i}}\e_{\pi_i}\e_{\pi_i}^\top=\!\begin{pmatrix}
\sfrac{s_{\pi\!,1}}{q_1}\!\!&&0\\
&\ddots&\\
0&&\!\!\sfrac{s_{\pi\!,n}}{q_n}
\end{pmatrix}\quad\text{where}\quad s_{\pi\!,j}=|\{i:\pi_i=j\}|.
\]
% \[\Xt = \begin{pmatrix}\frac{1}{\sqrt{q_{1}}}\x_{1}^\top\\
% \cdots\\
% \frac{1}{\sqrt{q_{n}}}\x_{n}^\top
% \end{pmatrix}.\]
We define $q$-rescaled size $k$ volume sampling of $\X$:
\begin{align*}
\text{for $\pi\in[n]^k$,}\quad
P(\pi) \propto \det\big(
%\overbrace{\sum \frac{1}{q_{\pi_i}}\x_{\pi_i}\x_{\pi_i}^\top}^{
\X^\top\!\Q_\pi\X\big) \prod_{i=1}^kq_{\pi_i}
\end{align*}
% \begin{align*}
% P(\pi) \propto \det\big(
% %\overbrace{\sum \frac{1}{q_{\pi_i}}\x_{\pi_i}\x_{\pi_i}^\top}^{
% \Xt_{\pi}^\top\Xt_{\pi}\big) \prod_{i=1}^kq_{\pi_i}
% \end{align*}
\textbf{Simple implementation using rejection sampling}:\\
1. Sample $\hat{\pi}_1...\hat{\pi}_k\in[n]$ i.i.d. from $q$,\\
2. w.p. $\det(\X^\top\!\Q_{\hat{\pi}}\X)\,/ D$, set $\pi=\hat{\pi}$ (otherwise go to step 1), where
\[D \geq \max_{\tilde{\pi}\in[n]^k}\det(\X^\top\!\Q_{\tilde{\pi}}\X)\]
\end{frame}

\begin{frame}
\frametitle{The normalization factor (Cauchy-Binet formula)}
For any $\X\in\R^{n\times d}$ and $q_1...q_n$ such that $\sum_i q_i=1$, we have
\begin{align*}
\sum_{\tilde{\pi}\in[n]^k}\!\det\!\Big(\sum_{i=1}^k\frac{1}{q_{\tilde{\pi}_i}}\x_{\tilde{\pi}_i}\x_{\tilde{\pi}_i}^\top\Big)\prod_{i=1}^kq_{\tilde{\pi}_i} = k(k\!-\!1)...(k\!-\!d\!+\!1)\det(\X^\top\X)
\end{align*}
For $k=d$, this reduces to simple Cauchy-Binet, because:
\begin{align*}
\det\!\Big(\sum_{i=1}^k\frac{1}{q_{\tilde{\pi}_i}}\x_{\tilde{\pi}_i}\x_{\tilde{\pi}_i}^\top\Big) = \det(\X^\top\Q_{\tilde{\pi}}\X) \overset{(*)}{=} \Big(\prod_{i=1}^kq_{\tilde{\pi}_i}\Big)^{-1}\det(\X_{\tilde{\pi}})^2
\end{align*}
Note that $(*)$ is only valid for $k=d$\\[4mm]
Thus any rescaled volume sampling becomes standard volume sampling in this case
\end{frame}

\begin{frame}
  \frametitle{Note: equivalent sampling procedure}
  Suppose we sample $\tilde{\pi}\in[n]^d$ with distribution:
  \begin{align*}
   \text{(standard volume sampling)}\quad P(\pi) = \frac{\det(\X_{\tilde{\pi}}^\top\X_{\tilde{\pi}})}{d! \det(\X^\top\X)}
  \end{align*}
  And sample $\bar{\pi}_1,\dots,\bar{\pi}_{k-d}$ i.i.d. from
  $q$.\\[3mm]
  Then $\pi = [\tilde{\pi},\bar{\pi}]$ is $q$-rescaled size $k$ volume sampling.
\end{frame}

\section{Loss bounds}

\begin{frame}
  \frametitle{Square loss under rescaled volume sampling}
  Let $\X\in\R^{n\times d}$ and $\y\in\R^n$\\
We define the least squares solution after sampling $\pi\in[n]^k$:
\begin{align*}
\w_\pi^* &\defeq \argmin_\w L_\pi(\w)\\[-4mm]
\text{where }
L_\pi(\w)&\defeq\|\Q_\pi^{\sfrac{1}{2}}(\X\w - \y)\|^2=\sum_{i=1}^ns_i\,\overbrace{\frac{1}{q_i}(\x_i^\top\w-\y_i)^2}^{\ell_i(\w)}
\end{align*}
Note that the loss for the original problem can be recovered as
\begin{align*}
\underset{i\sim q}{\E}[\ell_i(\w)] =
  \sum_{i=1}^n\frac{q_i}{q_i}(\x_i^\top\w-\y_i)^2 = \overbrace{\|\X\w-\y\|^2}^{L(\w)}.
\end{align*}
\textbf{Goal:} bound $\E[L(\w_\pi^*)]$ for $q$-rescaled size $k$
volume sampling.\\[3mm]
\textbf{Note:} In particular, we consider $q =
(\frac{l_1}{d},\dots,\frac{l_n}{d})$,\\
where $l_i= \x_i^\top(\X^\top\X)^{-1}\x_i$ is the $i$-th leverage score.
\end{frame}

\begin{frame}
  \frametitle{General loss expectation formula}
  Let $L_\pi^* = L(\w_\pi^*)$ and $\U =
  \X(\X^\top\X)^{-\frac12}$ be orthogonalization of $\X$.\\[3mm]
  \textbf{Theorem 1}.\quad For any $q$-rescaled volume sampling of any
  size:
  \begin{align*}
\E[L(\w_\pi^*)] &= (d\!+\!1)\,L(\w^*) - \E\Big[L_\pi^*\ \tr\big((\U^\top\Q_\pi\U)^{-1}\big)\Big]
  \end{align*}
  \vspace{2mm}

\textbf{Fact 2.}\quad  For $q$-rescaled size $k$  volume sampling:
  \begin{align*}
\E[L_\pi^*] = (k\!-\!d)\,L(\w^*)
  \end{align*}

\end{frame}

\begin{frame}
  \frametitle{Simple bound using Theorem 1}
  Suppose that $\pi\in[n]^k$ and
    $q=(\frac{l_1}{d},\dots,\frac{l_n}{d})$. Note that $l_i=\|\u_i\|^2$.\\
Then  $\tr(\U^\top\Q_\pi\U)=\sum_{i}s_i\frac{d}{l_i}\tr(\u_i\u_i^\top) =
  d\,k$, and by Jensen:
  \begin{align*}
\tr\big((\U^\top\Q_\pi\U)^{-1}\big)\geq
    \frac{d^2}{\tr(\U^\top\Q_\pi\U)} = \frac{d}{k}.
  \end{align*}
Using this and Fact 2 we have:
  \begin{align*}
    \frac{\E\big[L_\pi^*\,\tr\big((\U^\top\Q_\pi\U)^{-1}\big)\big]}{L(\w^*)}
    &\geq\frac{d}{k}\,\frac{\E[L_\pi^*]}{L(\w^*)}=
\frac{d}{k}\,(k-d)=d\,\Big(1-\frac{d}{k}\Big) 
\end{align*}
Now Theorem 1 leads to the following suboptimal guarantee:
\begin{align*}
\frac{\E[L(\w_\pi^*)]}{L(\w^*)} \leq (d+1) - d \Big(1-\frac{d}{k}\Big)
  = 1 + \frac{d^2}{k}
\end{align*}
  
\end{frame}

\section{The simplex}

\begin{frame}
\frametitle{Projection matrix of a simplex}
Suppose that $\U\in\R^{(d+1)\times d}$ forms a simplex and
$\U^\top\U=\I$. Assume that $\U$ is row-centered, i.e. $\sum_i
\u_i=\zero$.\\
Then  $\u_i^2 = \frac{d}{d+1}$, and if $i\neq j$ then
\begin{align*}
\u_i^\top\u_j \stackrel{\text{symm.}}{=}  
\frac{1}{d}\u_i\underbrace{\Big(\sum_{j\neq
i}\u_j\Big)}_{-\u_i} =
  -\frac{1}{d}\u_i^2 =-\frac{1}{d+1}
\end{align*}
So the projection matrix is
\begin{align*}
\P = \U\U^\top = 
\begin{pmatrix}
\frac{d}{d+1}&&-\frac{1}{d+1}\\
&\ddots&\\
-\frac{1}{d+1}&&\frac{d}{d+1}
\end{pmatrix} = \I - \frac{1}{d\!+\!1}\one
\end{align*}
The rows of $\P$ form a $d$-dimensional simplex embedded into
$d\!+\!1$-dimensional space.
$\P$ is row and column-centered
\end{frame}

\begin{frame}{A centered $\U$ matrix}

Form
$$
\begin{pmatrix}
a & b & b & b\\
b & a & b & b\\
b & b & a & b\\
b & b & b & a\\
c & c & c & c
\end{pmatrix}
$$
where
$b=\frac{1-\frac{1}{\sqrt{d+1}}}{d}\quad
a=b-1=\frac{1-d-\frac{1}{\sqrt{d+1}}}{d}\quad
c=\frac{1}{\sqrt{d+1}}
$

Padding (preserves centering)
$$\Ut=
\frac{\begin{pmatrix}\U \\ \U\end{pmatrix}}{\sqrt{2}}
\quad
\Pt=\frac{\begin{pmatrix} \P\P\\\P\P\end{pmatrix}}{2}
\quad \text{leverage scores are halved}
$$
% Lower bound conjecture
% $\Omega((d+1)(\frac{d}{d+1})^{k(d+1)}+\frac{d}{k})$
\end{frame}

\begin{frame}
  \frametitle{Loss bound on the simplex}
  \textbf{Theorem 3}\quad If $\U$ is the simplex then for $k\geq 2d$
  \begin{align*}
    \frac{\E[L(\w_\pi^*)]}{L(\w^*)} = 1 +
    (d+1)\Big(\frac{d}{d+1}\Big)^{k-d} + \Theta\Big(\frac{d}{k}\Big)
  \end{align*}
  Proof uses Theorem 1 and then a bunch of combinatorics which heavily
  rely on the symmetries of the simplex.\\[4mm]
  \textbf{Conjecture 4}\quad Simplex achieves the worst-case ratio $\frac{\E[L(\w_\pi^*)]}{L(\w^*)}$
\end{frame}

\section{Open problems}

\begin{frame}
  \frametitle{Discussion on September 5th}
  \begin{enumerate}
    \item What is expected (or w.h.p.) loss bound for size $d$ volume
      sampling plus i.i.d. leverage scores?\\[5mm]
    \item What if we sample $c$ independent size $d$ volume samples?\\
      \textbf{Conjecture}. For $c=2$, $\E[L(\w_\pi^*)]=O(1)L(\w^*)$.\\[5mm]
      \item What if  we do size $d+1$ volume sampling on
        $(\X|\one)\in\R^{n\times(d+1)}$?
  \end{enumerate}
\end{frame}

\section{Appendix: proof of Theorem 1}

\begin{frame}
\frametitle{Key lemma}
Let us denote $L_\pi^*\defeq L_\pi(\w_\pi^*)$ as the loss
of the best.\\[4mm]
\textbf{Lemma 5}\\
For any $\X\in\R^{n\times d}$, $\pi\in[n]^k$ and $i\in[n]$, we have
\begin{align*}
\det(\X^\top\Q_\pi\X)\,\ell_i(\w_\pi^*) =
  \det(\X^\top\Q_{\pi,i}\X)\,(L_{\pi,i}^*-L_\pi^*)
\end{align*}
or equivalently,
\begin{align*}
L_{\pi,i}(\w_\pi^*) - L_{\pi,i}(\w_{\pi,i}^*) = \frac{1}{q_i}\,\x_i^\top(\X^\top\Q_\pi\X)^{-1}\x_i\,\ell_i(\w_\pi^*)
\end{align*}
Note that if $k=d$, then $L_\pi^* = 0$ and $L_{\pi,i}(\w_\pi^*) =
\ell_i(\w_\pi^*)$, so we get
\begin{align*}
\det(\X^\top\Q_\pi\X)\,\ell_i(\w_\pi^*) = \det(\X^\top\Q_{\pi,i}\X)\,L_{\pi,i}^*=\det\big((\X|\y)^\top\Q_{\pi,i}(\X|\y)\big)
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Loss expectation proof}
%We assume that $q$ is uniform (works for simplex and $k=d$)
For $\pi\in[n]^k$ and any dist. $q=(q_1..q_n)$, let $q(\pi) = \prod_{i=1}^{|\pi|}q_{\pi_i}$.
\begin{align*}
\hspace{-8mm}C\,\E[L(\w_\pi^*)] &=
 \sum_{\pi\in[n]^k}q(\pi)\,\det(\U^\top\Q_\pi\U)
\sum_{i=1}^nq_i\ell_i(\w_\pi^*)\quad\Big(C=\frac{k!}{(k-d)!}\Big)\\
&=\sum_{\pi\in[n]^k}q(\pi)\sum_{i=1}^nq_i\,\det(\U^\top\Q_\pi\U)\ell_i(\w_\pi^*)\\
&=\sum_{\pi\in[n]^k}q(\pi)\sum_{i=1}^nq_i\,\det(\U^\top\Q_{\pi,i}\U)(L_{\pi,i}^*
  - L_\pi^*)\\
&=\!\!\bigg[\!\!\!\!\sum_{\ \ \pi\in[n]^{k+1}\!\!\!\!}\!\!\!\!\! L_\pi^*q(\pi)\det(\U^\top\Q_\pi\U)\bigg] 
\!-\! \bigg[\!\!\!\!
\sum_{\ \ \pi\in[n]^k\!\!}\!\!\! L_\pi^*\sum_{i=1}^nq(\pi,i)\det(\U^\top\Q_{\pi,i}\U)\!\bigg]
\end{align*}
Note that if $k=d$ then $2$-nd term is $0$.
\end{frame}

\begin{frame}
\frametitle{Computing the $1$-st term (and proving Fact 2)}
We use ``base $\times$ height'' and Cauchy-Binet:
\begin{align*}
\underbrace{\sum_{\ \ \pi\in[n]^{k+1}\!\!\!\!}\!\!\!\!\!
  q(\pi)L_\pi^*\det(\U^\top\Q_\pi\U)}_{\frac{(k+1)!}{(k+1-d)!}\,\E_{k+1}[L_\pi^*]} &= \sum_{\ \ \pi\in[n]^{k+1}\!\!\!\!}\!\!\!\!\!
  q(\pi)\det\big((\U|\y)^\top\Q_\pi(\U|\y)\big)\\[-4mm]
&=\frac{(k+1)!}{((k\!+\!1)-(d\!+\!1))!}\det\big((\U|\y)^\top(\U|\y)\big)\\
& = \frac{(k+1)!}{(k\!-\!d)!}\,L(\w^*)\underbrace{\det(\U^\top\U)}_{1}
\end{align*}
\vspace{-5mm}

Now, we plug it into the loss expectation:
\begin{align*}
\E[L(\w_\pi^*)] &= \frac{1}{C} \frac{(k+1)!}{(k\!-\!d)!} L(\w^*) -
  [2\text{-nd term}]\\
&=(k+1)\,L(\w^*) - [2\text{-nd term}]
\end{align*}
Note that for $k=d$ we recover the loss expectation formula.
\end{frame}

\begin{frame}
\frametitle{Computing the $2$-nd term}
Given $\pi\in[n]^k$, we have
\begin{align*}
\hspace{-8mm}\sum_{i=1}^nq(\pi,i)\det(\U^\top\!\Q_{\pi,i}\U) &=
q(\pi)\sum_{i=1}^{n}q_i\det(\U^\top\!\Q_\pi\U)\Big(1
\!+\! \frac{1}{q_i}\u_i^\top(\U^\top\!\Q_\pi\U)^{-1}\u_i\Big)\\
&=q(\pi)\det(\U^\top\Q_\pi\U)\sum_{i=1}^n\Big(q_i + \u_i^\top(\U^\top\Q_\pi\U)^{-1}\u_i\Big)\\
&=q(\pi)\det(\U^\top\Q_\pi\U)\Big(1+\tr\big(\U(\U^\top\Q_\pi\U)^{-1}\U^\top\big)\Big)\\
&=q(\pi)\det(\U^\top\Q_\pi\U) \Big(1+\tr\big((\U^\top\Q_\pi\U)^{-1}\big)\Big)
\end{align*}
So we can rewrite the $2$-nd term as follows:
\begin{align*}
{\scriptsize\text{[$2$-nd term]}}&=\frac{1}{C}\sum_{\pi\in[n]^k}L_\pi^*\,
  q(\pi)\det(\U^\top\Q_\pi\U)
  \Big(1+\tr\big((\U^\top\Q_\pi\U)^{-1}\big)\Big)\\
&=(k\!-\!d)\,L(\w^*) + 
\frac{1}{C}\!\!\sum_{\pi\in[n]^k}\!\!\!L_\pi^*\, q(\pi)\det(\U^\top\Q_\pi\U)\,
  \tr\big((\U^\top\Q_\pi\U)^{-1}\big)
\end{align*}
\end{frame}


\end{document}