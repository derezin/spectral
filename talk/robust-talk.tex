\documentclass[usenames,dvipsnames]{beamer}
%\beamertemplateshadingbackground{brown!70}{yellow!10}
\mode<presentation>
{
    \usetheme{Warsaw}
    %\usecolortheme{crane}
    % or ...

%    \setbeamercovered{transparent}
    % or whatever (possibly just delete it)
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]{}
\usefonttheme{serif}
\setbeamertemplate{headline}{}

%\usepackage[dvipsnames]{xcolor}

\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usepackage{mathtools}
\usepackage{relsize}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{colortbl}
%\usepackage{multicol}
\usepackage{cancel}
\usepackage{multirow}
\usepackage{forloop}% http://ctan.org/pkg/forloop


\newcommand{\setdiff}{\Delta}
\newcommand{\hf}{\wh{f}}
\newcommand{\hg}{\wh{g}}
\newcommand{\hh}{\wh{h}}
\newcommand{\wh}{\widehat}
\newcommand{\wt}{\widetilde}
\newcommand{\ov}{\overline}

\def\bx{{\bf x}}

\def\P{\mbox{\rm P}}
\newcommand{\eps}{\varepsilon}
\newcommand{\Ot}{\widetilde{O}}

\newcommand{\norm}[1]{\|#1\|}
\newcommand{\abs}[1]{|#1|}
\newcommand{\NP}{\mbox{\rm NP}}
\newcommand{\AC}{\mbox{\rm AC}}
\newcommand{\opt}{\mathsf{opt}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\rev}{\mathsf{rev}}
\newcommand{\Tr}{\mathsf{Tr}}
\newcommand{\val}{\mathsf{val}}
\newcommand{\supp}{\mathsf{supp}}
\newcommand{\diag}{\mathsf{diag}}
\newcommand{\sign}{\mathsf{sign}}
\newcommand{\poly}{\mathsf{poly}}
\newcommand{\poiss}{\mathsf{Poisson}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\cov}{\mathsf{cov}}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\E}{\mathbb{E}}
\newcommand{\empt}{\mathsf{empt}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\FF}{\mathcal{F}}
\renewcommand{\i}{\mathbf{i}}
\newcommand{\midd}{\mathsf{mid}}
\newcommand{\Mc}{\mathcal{M}}


\DeclareMathOperator*{\argmax}{arg\,max}

\DeclareMathOperator{\Gaussian}{Gaussian}
\DeclareMathOperator{\rect}{rect}
\DeclareMathOperator{\sinc}{sinc}
\DeclareMathOperator{\Comb}{Comb}
\DeclareMathOperator{\signal}{signal}


\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}
}

%\input{../shortdefs}

\title[]{Query Complexity of
  Least Absolute Deviation Regression
  via Robust Uniform Convergence}

\author[]{Xue Chen\thanks{George Mason University $\rightarrow$ USTC, China} and Micha{\l }
  Derezi\'{n}ski\thanks{UC Berkeley
    $\rightarrow$ University of Michigan}}
\institute{Conference on Learning Theory (COLT 2021)}
\date{}
\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
  \frametitle{Linear regression}
\centerline{\includegraphics[width=.6\textwidth]{figs/regression-simple}}
\vfill
\begin{align*}
\textcolor{OliveGreen}{\beta^*}=\argmin_\beta L(\beta),\qquad\text{where}\quad
  L(\beta)=\sum_i \ell(a_i^\top \beta-y_i)
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Linear regression with hidden labels}
  \centerline{
    \only<1>{\includegraphics[width=.6\textwidth]{figs/regression-support}}%
    \only<2>{\includegraphics[width=.6\textwidth]{figs/regression-subset}}
    }
\vfill
\onslide<2>

\begin{enumerate}
\item Sample a subset of points $\{a_i\}_{i\in S}$,
\item Query the corresponding labels $\{y_i\}_{i\in S}$,
\item Construct an estimate $\textcolor{blue}{\wt{\beta}}$ by solving the subproblem
  \end{enumerate}
\vfill  
\end{frame}

\begin{frame}
  \frametitle{Query complexity}
\centerline{\includegraphics[width=.6\textwidth]{figs/regression-all}}
\vspace{5mm}

\textbf{Question}: How many queries needed to construct $\textcolor{blue}{\wt{\beta}}$ such
that
\begin{align*}
  L(\textcolor{blue}{\wt{\beta}}) \leq (1+\epsilon)\cdot L(\textcolor{ForestGreen}{\beta^*})\quad\text{with
  probability $1-\delta$}
  \end{align*}
\end{frame}


\begin{frame}
  \frametitle{Main results}
Prior work: Only for \textit{least squares} regression, i.e.,
  $\ell(x) = x^2$\\[7mm]
\textbf{Our results}: Query complexity of \underline{robust} regression\\[4mm]
\pause

%\begin{itemize}
%\item least absolute deviations, $l(x) = |x|$, in $d$ dimensions:
\begin{theorem}[least absolute deviations]
For $d$-dimensional regression with $\ell_1$ loss, $\ell(x) = |x|$,
  \begin{align*}
 \Omega(d/\epsilon^2) \quad \leq\quad \textit{Query Complexity}
    \quad\leq\quad
    O(d\log(d/\epsilon)/\epsilon^2)
  \end{align*}
  \end{theorem}
  \vspace{3mm}
  \pause
  
\begin{theorem}[$l_p$ regression]
For $d$-dimensional regression with $\ell_p$ loss, $\ell(x) = |x|^p$, $p\in(1,2)$,
  \begin{align*}
\textit{Query Complexity}
    \quad\leq\quad
    O(d^2\log(d/\epsilon)/\epsilon^2)
  \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Key Techniques}
  \textbf{Algorithm}: 
  \begin{quote}Importance sampling via \underline{Lewis Weights}
    \end{quote}
  \vspace{7mm}
  
  \textbf{Analysis}:
  \begin{quote}
    Correcting outliers via \underline{Robust Uniform Convergence}
    \end{quote}
\begin{align*}
\sup_\beta \frac{|L(\beta)- \wt{L}(\beta)-\Delta|}{L(\beta)}
    \ \leq\ \epsilon \quad\text{where}\quad\Delta = L(\beta^*)-\wt{L}(\beta^*)
  \end{align*}
\end{frame}

\end{document}