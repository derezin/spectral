%!TEX root = regression.tex
\section{Concentration for Huber loss}
Let $\tau$ be the parameter of the Huber loss function in this section. Given a number $x$, let $H(x)$ denote the Huber loss function where $H(x)=|x|^2/2 \tau$ when $|x|\le \tau$ and $H(x)=|x|-\tau/2$ otherwise. Given a vector $y \in \mathbb{R}^n$, let $\|y\|_H$ denote $\sum_{i=1}^n H(y_i)$.

Given two variables (or numbers) $a$ and $b$, we say $a \approx_{\alpha} b$ for $\alpha \ge 1$ only if $b/\alpha \le a \le \alpha b$. In this section, we always consider matrix $A$ whose leverage score is almost uniform: $a_i^{\top} (A^{\top} A)^{-1} a_i \approx_{\alpha} d/n$ for some fixed $\alpha$.

Now we consider the following algorithm to find a good solution for $\|A \beta - y\|_H$ with small label complexity.
\begin{enumerate}
\item We sample a diagonal matrix $S$ from the leverage score with support size $O(\frac{d \log d}{\eps^2})$.
\item Let $C_H$ be a large constant and $U$ be a upper bound of $\min_{\beta} \|A \beta - y\|_2$.
\item We sample a diagonal matrix $S_H$ from the uniform distribution with support size $m=\poly(d,\log nU/\tau,\eps)$.
\item We output $\tilde{\beta}_H$ from
\[
\arg\min_{\beta} \|S_H A \beta- S_H y\|_H \text{ subject to } \|S A \beta - S y\|_2 \le C_H \cdot n^{3/2} \cdot U.
\]
\end{enumerate}

\textcolor{blue}{Xue: We may need to assume $U$ here because it may be impossible to UPPER bound $\min_{\beta} \|A \beta - y\|_2$.}

\begin{theorem}\label{thm:approx_huber_loss}
With probability $0.75$, $\tilde{\beta}_H$ satisfies
\[
\|A \tilde{\beta}_H - y\|_H \le C \cdot \min_{\beta} \|A \beta - y\|_H
\]
for some constant $C$.
\end{theorem}

In this rest of this section, we finish the proof of Theorem~\ref{thm:approx_huber_loss}. To show the correctness of the algorithm, we state a few properties about $S$ and $S_H$. First of all, we consider the concentration of one fixed $\beta$ in the Huber loss function.
\begin{lemma}\label{lem:Huber_importance}
Given a matrix $A$ and $\alpha \ge 1$, suppose its leverage score is almost uniform: $a_i^{\top} (A^{\top} A)^{-1} a_i \approx_{\alpha} d/n$. For any fixed $\beta$, for $m=O(\alpha^{4.5} d^{1.5} \log \frac{1}{\delta} /\eps^2)$ random points $j_1,\ldots,j_m$ in $[m]$, with probability $1-\delta$, 
\[
\frac{n}{m} \sum_{i=1}^m H\big( a_{j_i}^{\top} \beta\big) \in [1-\eps, 1+\eps] \cdot \|A\beta\|_H.
\]
\end{lemma}
We plan to apply a net argument with the above lemma to all possible $\tilde{\beta}_H$. To bound $\beta_H$ that minimizes $\|A \beta-y\|_H$, we will use Lemma 4.4 in \cite{CW15}.
\begin{lemma}\label{lem:relation_ell2_huber}
For any $A \in \mathbb{R}^{n \times d}$ and $y \in \mathbb{R}^n$, let $\beta_{\ell_2}=\arg\min_{\beta} \|A \beta-y\|_2$ and $\beta_H=\arg\min_{\beta} \|A \beta - y\|_H$. We always have $\|A \beta_{\ell_2} - y\|_2 \le \|A \beta_{H} - y\|_2 \le C \cdot n^{3/2} \cdot \|A \beta_{\ell_2} - y\|_2$. 
\end{lemma}
The last ingredient in the argument is the following concentration bound.
\begin{lemma}\label{lem:properties_sketching}
When the support size of $S$ and $S_H$ are large enough, with probability 0.99, we have
\begin{enumerate}
\item $\|S A \beta\|_2 \in [1-\eps,1+\eps] \cdot \|A \beta\|_2$ for all $\beta$.
\item $\|S_H A \beta\|_H \in [1-\eps,1+\eps] \cdot \|A \beta\|_H$ for all $\beta$ with $\|A \beta\| \le 3C_H \cdot n^{3/2} \cdot U$.
\end{enumerate}
\end{lemma}
We defer the proofs of Lemma~\ref{lem:Huber_importance} and Lemma~\ref{lem:properties_sketching} to Section~\ref{sec:proof_huber_import} and~\ref{sec:net_huber} separately. One extra property about the Huber loss that will be used in this proof is a weaker triangle inequality:
\begin{fact}\label{fact:triangle_huber}
For any two numbers $a$ and $b$, $H(a+b) \le 2 H(a)+ 2 H(b)$. Moreover, for any two vectors $u$ and $v$, $\|u+v\|_H \le 2 \|u\|_H + 2\|v\|_H$.
\end{fact}

\begin{proofof}{Theorem~\ref{thm:approx_huber_loss}}
For convenience, let $\beta_H=\arg\min_{\beta} \|A \beta - y\|_H$. We first argue $\beta_H$ is in the set $\|S A \beta - S y\|_2 \le C_H \cdot n^{3/2} \cdot U$ of the program. By Lemma~\ref{lem:relation_ell2_huber}, $\|A \beta_{H} - y\|_2 \le C \cdot n^{3/2} \cdot U$ from our definition of $U$. At the same time, $\E_S[\|S A \beta_H - S y\|^2_2]=\|A \beta_H - y\|^2_2$ implies that $\|S A \beta_H - S y\|_2 \le 3 \|A \beta_H - y\|_2$ with probability $8/9$ (by Markov's inequality). So with probability $8/9$, we have $\|S A \beta_H - S y\|_2 \le 3 C \cdot n^{3/2} \cdot U$.

Suppose the output $\wt{\beta}_H \neq \beta_H$. From the definition, we have
\[
\|S_H A \wt{\beta}_H - S_H y\|_H \le \|S_H A \beta_H - S_H y\|_H
\text{ and } \|S A \wt{\beta}_H - S y\|_2 \le C_H \cdot n^{3/2} \cdot U.
\]
In the rest of this proof, we use Lemma~\ref{lem:properties_sketching} to argue $\|A \wt{\beta}_H - y\|_H = O(\|A \beta_H - y\|_H)$. To apply the concentration for $\|\cdot\|_H$, we bound $\|A (\wt{\beta}_H - \beta_H)\|_2$ at first. Note that
\begin{align*}
\|A (\wt{\beta}_H - \beta_H)\|_2 & \le \frac{1}{1-\eps} \| S A \wt{\beta}_H - S A \beta_H\|_2 \\
& \le \frac{1}{1-\eps} (\| S A \wt{\beta}_H - S y\|_2 + \| S A \beta_H - S y\|_2) \\
& \le \frac{1}{1-\eps} \cdot 2 \cdot C_H \cdot n^{3/2} \cdot U.
\end{align*}
Thus Lemma~\ref{lem:properties_sketching} indicates that $\|S_H A (\wt{\beta}_H - \beta_H)\|_H \approx \|A (\wt{\beta}_H - \beta_H)\|_H$. Now we are ready to finish the proof. For convenience, we use $a \lesssim b$ to denote $a=O(b)$ in this calculation.
\begin{align*}
\|A \wt{\beta}_H - y\|_H & \le 2 \big( \|A (\wt{\beta}_H - \beta_H) \|_H + \|A \beta_H - y\|_H \big) \tag{Fact~\ref{fact:triangle_huber}}\\
& \lesssim \|S_H A (\wt{\beta}_H - \beta_H) \|_H + \|A \beta_H - y\|_H \tag{Lemma~\ref{lem:properties_sketching}}\\
& \lesssim \|S_H A \wt{\beta}_H - S y\|_H + \|S_H A \beta_H - S y\|_H + \|A \beta_H - y\|_H \tag{Fact~\ref{fact:triangle_huber}}\\
& \lesssim 2 \| S_H A \beta_H -y \|_H + \|A \beta_H - y \|_H \tag{Definition of $\wt{\beta}_H$}.
\end{align*}
Finally, we note that $\E[\| S_H A \beta_H -y \|_H]=\|A\beta_H - y\|_H$. So by Markov's inequality again, $\|A \wt{\beta}_H - y\|_H = O(\|A \beta_H - y\|_H)$.
\end{proofof}

\subsection{Proof of Lemma~\ref{lem:properties_sketching}}\label{sec:net_huber}
\textcolor{blue}{Xue: This is a sketch.} The proof strategy is the same as Lemma 4.5 in \cite{CW15}. For $\beta$ with small $\|\cdot\|_2$  norm (say less than $\tau$), the Huber loss function is always in the quadratic form. So we could use the $\ell_2$ concentration. Then for $\beta$ whose $\ell_2$ norm is between $\tau$ and $O(n^{3/2} \cdot U)$, we apply a net argument.

\subsection{Proof of Lemma~\ref{lem:Huber_importance}}\label{sec:proof_huber_import}
We finish the proof of Lemma~\ref{lem:Huber_importance} in this section. Since the leverage score is upper bounded by $\alpha d/n$. From the equivalence of the leverage score and $\ell_2$ importance sampling, we know
\[
\forall x \in \mathbb{R}^d, \forall j \in [n], \frac{|a_j^{\top} x|^2}{\|Ax\|_2^2}\le \alpha d/n.
\]

Let $w_1,\ldots,w_n$ be the $\ell_1$ Lewis weight of $A$. From Claim~\ref{clm:non_uniform_Lewis_w} and the assumption on the leverage score, $w_i \approx_{\alpha} d/n$. Furthermore, from Lemma~\ref{lem:non_uniform_import_p}, we know the $\ell_1$ importance weight
\begin{equation}\label{eq:L_1_Lewis_al}
\forall x \in \mathbb{R}^d, \forall j \in [n], \frac{|a_j^{\top} x|}{\|Ax\|_1}\le \alpha^3 d/n.
\end{equation}

Since $\beta$ is fixed, we only need to upper bound $\frac{n \cdot H\big( a_j^{\top} x \big)}{\|A\beta\|_H}$ for all $j \in [n]$ before applying the concentration bound. Let $j_{\max}$ be the entry in $A \beta$ with the largest absolute value. Because the Huber loss function is monotone, in the rest of this proof, we will bound 
\begin{equation}\label{eq:import_HL_loss}
\frac{n \cdot H\big( (A\beta)_{j_{\max}} \big)}{\|A\beta\|_H} = \frac{H\big( a_{j_{\max} }^{\top} \beta \big)}{\E_j\big[ H\big( a_j^{\top} \beta\big) \big]}.
\end{equation}
We consider 3 cases based on the relation between $\tau$, $\big| a_{j_{\max} }^{\top} \beta \big|$ and $\E_j\big[ \big| a_j^{\top} \beta \big| \big]$. One property that we will extensively use in this proof is that $\big| a_{j_{\max} }^{\top} \beta \big| \le \alpha^3 d \cdot \E_j\big[ \big| a_j^{\top} \beta \big| \big]$ from \eqref{eq:L_1_Lewis_al}.

\begin{enumerate}
\item $\tau \ge \big| a_{j_{\max} }^{\top} \beta \big|$: In this case, $H\big( a_{j_{\max} }^{\top} \beta \big)=\|a_{j_{\max} }^{\top} \beta\|_2^2/2\tau$ and $\E_j\big[ H\big( a_j^{\top} \beta\big) \big]=\|A \beta\|_2^2/2\tau$. So \eqref{eq:import_HL_loss} is upper bounded by the Leverage score, which is $\alpha \cdot d$.

\item $\tau \le \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2$: From the definition, $H\big( a_{j_{\max} }^{\top} \beta \big)=|a_{j_{\max} }^{\top} \beta| - \tau/2 \le |a_{j_{\max} }^{\top} \beta|$. Then we switch from the expectation to the summation for convenience:
\[
\sum_j H\big( a_j^{\top} \beta\big) \ge \sum_{j:|a_j^{\top} \beta| \ge \tau } H\big( a_j^{\top} \beta\big)  \ge \sum_{j:|a_j^{\top} \beta| \ge \tau } \big| a_j^{\top} \beta\big| - \tau/2.
\]
Since $\sum_{j: |a_j^{\top} \tau|<\tau} |a_j^{\top} \beta| \le \{\# j:|a_j^{\top} \beta| < \tau \} \cdot \tau$, we further have \[
\sum_{j:|a_j^{\top} \beta| \ge \tau } \left( \big| a_j^{\top} \beta\big| - \tau/2 \right) = n \cdot  \E_j\big[ \big| a_j^{\top} \beta \big| \big] - \tau/2 \cdot \{\# j:|a_j^{\top} \beta| \ge \tau \}- \tau \cdot \{\# j:|a_j^{\top} \beta| < \tau \} \ge n \cdot  \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2.
\]
So \eqref{eq:import_HL_loss} is upper bounded by $\frac{|a_{j_{\max} }^{\top} \beta|}{\E_j\big[ \big| a_j^{\top} \beta \big| \big]/2} \le 2 \alpha^3 \cdot d$ from the Lewis weight.

\item $\tau \in \bigg[ \sqrt{\big| a_{j_{\max} }^{\top} \beta \big| \cdot \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2}, \big| a_{j_{\max} }^{\top} \beta \big|\bigg)$: Similar to the 2nd case, we bound $H\big( a_{j_{\max} }^{\top} \beta \big)=|a_{j_{\max} }^{\top} \beta| - \tau/2 \in \bigg[|a_{j_{\max} }^{\top} \beta|/2, |a_{j_{\max} }^{\top} \beta| \bigg]$ by 
\[
\left[\tau/|a_{j_{\max} }^{\top} \beta|, 2 \right] \cdot |a_{j_{\max} }^{\top} \beta|^2/2\tau \subset \left[\sqrt{\E_j\big[ \big| a_j^{\top} \beta \big| \big]/2 \cdot |a_{j_{\max} }^{\top} \beta|}, 2\right] \cdot |a_{j_{\max} }^{\top} \beta|^2/2\tau.
\]
Then we lower bound $\|A \beta\|_H$ by comparing it with $\|A \beta\|_2^2/2 \tau$. Since the above lower bound on $H\big( a_{j_{\max} }^{\top} \beta \big)$ works for any $j$, we have
\[
\|A \beta\|_H \ge \sqrt{\E_j\big[ \big| a_j^{\top} \beta \big| \big]/2 \cdot |a_{j_{\max} }^{\top} \beta|} \cdot \|A \beta\|_2^2/2\tau.
\]
Since $\E_j\big[ \big| a_j^{\top} \beta \big| \big]/|a_{j_{\max} }^{\top} \beta| \ge \frac{1}{\alpha^3 d}$, \eqref{eq:import_HL_loss} is upper bounded by $O(\alpha^{2.5} d^{1.5})$ given $|a_{j_{\max} }^{\top} \beta|^2/\|A \beta\|_2^2 \le \alpha d/n$ from the leverage score.
 
\item $\tau \in \bigg(\E_j\big[ \big| a_j^{\top} \beta \big| \big]/2, \sqrt{\big| a_{j_{\max} }^{\top} \beta \big| \cdot \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2}\bigg)$: Similar to the 3rd case, we still have $H\big( a_{j_{\max} }^{\top} \beta \big)=|a_{j_{\max} }^{\top} \beta| - \tau/2 \le |a_{j_{\max} }^{\top} \beta|$. Unlike the 3rd case, we lower bound $\|A \beta\|_H$ by $\|A \beta\|_1$ here. For convenience, we switch from the expectation to the summation:
\[
\sum_j H\big( a_j^{\top} \beta\big) \ge \sum_{j:|a_j^{\top} \beta| \ge  \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2} H\big( a_j^{\top} \beta\big).
\]
Now we argue this is $\Omega(\frac{\|A \beta\|_1}{\alpha^{1.5} \cdot d^{0.5}})$: for any $j$ with $|a_j^{\top} \beta| \ge  \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2$, its Huber loss is either $|a_j^{\top} \beta| - \tau/2 \ge |a_j^{\top} \beta|/2$ or $|a_j^{\top} \beta|^2/2\tau$ by the definition. Since 
\[
\tau/\E_j\big[ \big| a_j^{\top} \beta \big| \big] \le \sqrt{\big| a_{j_{\max} }^{\top} \beta \big|/\E_j\big[ \big| a_j^{\top} \beta \big| \big]} \le \alpha^{1.5} \cdot d^{0.5},
\] for Huber loss in the quadratic form, we always have $|a_j^{\top} \beta|^2/2\tau \ge \frac{|a_j^{\top} \beta|_1}{4 \alpha^{1.5} \cdot d^{0.5}}$ for $j$ with $|a_j^{\top} \beta| \ge  \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2$. At the same time, we also have $\underset{j:|a_j^{\top} \beta| \ge  \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2}{\sum} |a_j^{\top} \beta| \ge \|A \beta\|_1/2$. So
\[
\sum_{j:|a_j^{\top} \beta| \ge  \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2} H\big( a_j^{\top} \beta\big) \ge \sum_{j:|a_j^{\top} \beta| \ge  \E_j\big[ \big| a_j^{\top} \beta \big| \big]/2} \frac{|a_j^{\top} \beta|_1}{4 \alpha^{1.5} \cdot d^{0.5}} \ge \|A \beta\|_1/2 \cdot \frac{1}{4 \alpha^{1.5} d^{0.5}}.
\]
So \eqref{eq:import_HL_loss} is upper bounded by $\frac{8 \alpha^{1.5} d^{0.5} \cdot |a_{j_{\max} }^{\top} \beta|}{\E_j\big[ \big| a_j^{\top} \beta \big| \big]} \le O(\alpha^{4.5} \cdot d^{1.5})$ from the Lewis weight again.
\end{enumerate}
From all discussion above, \eqref{eq:import_HL_loss} is always upper bounded by $O(\alpha^{4.5} d^{1.5})$. From the Chernoff bound, this shows $m=O(\alpha^{4.5} d^{1.5} \log \frac{1}{\delta}/\eps^2)$ is sufficient.

\textcolor{blue}{Xue: The net argument in \cite{CW15} is quite complicated --- it takes a whole Section 4 in \cite{CW15} to develop it. I haven't figured out how to generalize this concentration bound to Lemma~\ref{l:uniform-convergence}.}