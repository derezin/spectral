\documentclass{beamer}
%\beamertemplateshadingbackground{brown!70}{yellow!10}
\mode<presentation>
{
    %\usetheme{Warsaw}
    \usecolortheme{crane}
    % or ...

    \setbeamercovered{transparent}
    % or whatever (possibly just delete it)
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]{}

\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usepackage{mathtools}
\usepackage{relsize}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{colortbl}
%\usepackage{multicol}
\usepackage{cancel}
\usepackage{multirow}
\usepackage{forloop}% http://ctan.org/pkg/forloop
\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}
}

\input{../shortdefs}
\def\Xi{\X_{-i}\X_{-i}^\top}
\renewcommand{\Xinv}{(\X\X^\top)^{-1}}
\def\xinv{\x_i^\top\Xinv\x_i}
\def\Xinvr{(\lambda\I+\X_{-1}\X_{-1}^\top)^{-1}}
\def\detX{\det(\X\X^\top)}
\def\detXS{\det(\X_S\X_S^\top)}
\def\detXT{\det(\X_T\X_T^\top)}

\title[]{Label complexity of $\ell_1$-regression}

\author[]{Micha{\l } Derezi\'{n}ski}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
  \frametitle{Setup}
  Let $A$ be an $n\times d$ matrix and $\beta$ be an $n$-dimensional
  vector.\\
  Let $\beta^*$ be the minimizer of $\ell_1$ regression:
  \begin{align*}
    \beta^* = \argmin_\beta \|A\beta - y\|_1.
  \end{align*}
Suppose that $y$ is hidden. Given $A$, our goal is to construct a random
$n\times n$ diagonal matrix $S$ with few non-zero diagonal
entries such that:
\begin{align*}
  \min_\beta \|S(A\beta-y)\|_1 \leq (1+\epsilon) \|A\beta^*-y\|_1.
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Special case: Median estimation}
Suppose that $A\in\R^{n\times1}$ is an all-ones vector. Then:
\begin{align*}
  \|A\beta - y\|_1 = \sum_{i=1}^n |\beta - y_i|,\quad \text{so}\quad \beta^*=\text{Median}(y).
\end{align*}

Since $A$ provides no information, all we can do is uniform
sampling.\\
Let $S$ be a uniform sampling sketch of size $k$. Then:
\begin{align*}
  \argmin_\beta \|S(A\beta-y)\|_1 = \text{Median}(y_S).
\end{align*}
\textbf{Question:} What are the upper/lower bounds for median
estimation in terms of the $\ell_1$ error.
\end{frame}

\begin{frame}
  \frametitle{Median estimation via uniform sampling}
  W.l.o.g.~assume that $y_1<y_2<...<y_n$\\
  Suppose that we sample $i_1,...,i_k$ i.i.d.~uniformly at random from
  $[n]$\\
  Let $m\in[n]$ be the median index of $y$ (i.e., roughly $n/2$). Define:\\
$k_{-}=|\{j: i_j\!<\!m\!-\!\epsilon n\}|$, $k_{+}=|\{j:i_j\!>\!m\!+\!\epsilon n\}|$,
 $k_0=k-k_{-}-k_{+}$\\
 Suppose that $k= 2ct/\epsilon$. Then, setting $c=\ln(4t)$,  we have:
 \begin{align*}
   \Pr\big[k_0< t\big] \leq  t(1-2\epsilon)^{2c/\epsilon} \leq t
   e^{-c}\leq \frac14.
 \end{align*}
 Moreover, interpreting $k_+-k_-$ as a random walk, we have:
\begin{align*}
 \Pr\Big[|k_{+}-k_{-}| > 2\sqrt k\Big] \leq
  \Pr\Big[(k_{+}-k_{-})^2>4\,\E[(k_{+}-k_{-})^2]\Big]\leq \frac14.
\end{align*}
So, setting $t=O(\frac1\epsilon\log\frac1\epsilon)$, we have $t>2\sqrt
k$, so with prob.~1/2,
\begin{align*}
k_0>|k_{+}-k_{-}|,\quad\text{so}\quad
  \text{Median}(y_{i_1},...,y_{i_k})\in \big[y_{m-\epsilon
  n},y_{m+\epsilon n}\big]
\end{align*}
\end{frame}

\begin{frame}
  \frametitle{Median estimation: $\ell_1$ error}
  Suppose that we find $\hat\beta \in \big[y_{m-\epsilon
    n},y_{m+\epsilon n}\big]$.
  \begin{align*}
\hspace{-4mm}    \|A\hat\beta-y\|_1 - \|A\beta^*-y\|_1 \leq \sum_{i=m-\epsilon
    n}^{m+\epsilon n} |\hat\beta-y_i| - |\beta^*-y_i|\leq 2\epsilon
    n\, |\hat\beta-\beta^*|
  \end{align*}
  Note that we also have:
  \begin{align*}
    \|A\beta^* - y\|_1 \geq
    (\tfrac12-\epsilon)n\,|\hat\beta-\beta^*| =
    \frac{1-2\epsilon}2\, n\,|\hat\beta-\beta^*|.
  \end{align*}
  Putting this together, we get:
  \begin{align*}
\|A\hat\beta-y\|_1 \leq \Big(1 + \frac{4\epsilon}{1-2\epsilon}\Big)\cdot \|A\beta^*-y\|_1.
  \end{align*}
  So we need $k=\tilde O(\frac1{\epsilon^2})$ samples
  to get a $1+\epsilon$ approximation.
\end{frame}

\begin{frame}
  \frametitle{Next steps}

  \begin{enumerate}
  \item Extend this to arbitrary $A$ with dimension $d=1$. What
    sampling is needed?
  \item What breaks down for $d=2$? Look for a lower bound.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Iteratively reweighted least squares}
  Consider the following procedure:
  \begin{align*}
    \beta_{t+1} = \argmin_\beta
    \sum_{i}w_i(a_i^\top\beta-y_i)^2\qquad\text{for}\quad w_i=\tfrac1{|a_i^\top\beta_t-y_i|}.
  \end{align*}
  Note that $\beta_{t+1} = (A^\top D_w A)^{-1}A^\top D_w y$, and
  moreover we have:
  \begin{align*}
    \beta_t\overset{t\rightarrow\infty}\longrightarrow \beta^* \in \argmin_\beta\|A\beta-y\|_1.
  \end{align*}
  Finally, $\beta^*$ is the stationary point of this transformation,
  so:
  \begin{align*}
    \beta^* = (A^\top D_w A)^{-1}A^\top D_w y,\quad\text{for}\quad w_i =\tfrac1{|a_i^\top\beta^*-y_i|}.
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Subsampling reweighted least squares}
  Suppose that we sample according to leverage scores of the
  reweighted problem with $w_i =\frac1{|a_i^\top\beta^*-y_i|}$:
  \begin{align*}
    p_i = w_i\cdot a_i^\top(A^\top D_w A)^{-1}a_i/d.
  \end{align*}
Let $\hat\beta$ be the subsampled least squares solution. Then:
\begin{align*}
  \sum_i w_i (a_i^\top \hat\beta - y_i)^2\leq (1+\epsilon) \|A\beta^*-y\|_1.
\end{align*}
\end{frame}

\begin{frame}
  \frametitle{Reduction by shrinking outliers}
Let $S$ be the diagonal subsampling and rescaling matrix.\\
Gradients of the full and of the subsampled loss are
  given by:
  \begin{align*}
g(\beta) = A^\top \sign(A \beta- y),\qquad    \hat g(\beta) = A^\top S\, \sign (A \beta- y).
  \end{align*}
  Let $\beta^*$ and $\hat\beta$ satisfy $g(\beta^*)=\hat
  g(\hat\beta)=0$. Suppose that for some index $i\in [n]$ we replace
  $y_i$ with $y_i'$ so that:
  \begin{align*}
\hspace{-4mm}   \sign(a_i^\top\beta^*-y_i') =    \sign(a_i^\top\hat\beta-y_i') =
    \sign(a_i^\top\beta^*-y_i) = \sign(a_i^\top \hat\beta-y_i) 
  \end{align*}
  Then, the new problem preserves $g(\beta^*)=\hat g(\hat\beta)=0$ and
  moreover:
  \begin{align*}
    \|A\hat\beta - y'\|_1 - \|A\beta^*-y'\|_1 =  \|A\hat\beta - y\|_1 - \|A\beta^*-y\|_1
  \end{align*}
  If moreover $|a_i^\top\beta^* -y_i'|\leq|a_i^\top\beta^* -y_i|$, and
  we show that $\|A\hat\beta - y'\|_1 \leq
  (1+\epsilon)\|A\beta^*-y'\|_1$, then this implies:
  \begin{align*}
    \|A\hat\beta - y\|_1 \leq (1+\epsilon)\|A\beta^*-y\|_1
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Idea: surrogate loss}
Let $L(\beta) = \|A\beta -y\|_1$. Define a surrogate loss
$L'(\beta) = \|A\beta-y'\|_1$ so that
$|a_i^\top\beta^*-y_i'|\leq|a_i^\top\beta^*-y_i|$, and the two
residuals have the same sign for all $i$.\\
Suppose we show that with high probability:
\begin{align*}
  L(\hat\beta) - L(\beta^*) &\leq C\cdot \big(L'(\hat\beta)
  - L'(\beta^*)\big)
  \\
  \text{and}
  \quad
  L'(\hat\beta) &\leq (1+\epsilon)\cdot L'(\beta^*).
\end{align*}
Then it follows that:
\begin{align*}
  L(\hat\beta)-L(\beta^*) \leq C\cdot\big( L'(\hat\beta) - L'(\beta^*)\big)\leq
  C\epsilon\cdot L'(\beta^*) \leq C\epsilon\cdot L(\beta^*)
\end{align*}
\emph{Goal}: Design the surrogate loss so that the residual weights
$|a_i^\top\beta^*-y_i'|/L'(\beta^*)$ are uniform or dominated by Lewis weights.
\end{frame}

\begin{frame}
  \frametitle{Idea: Estimating the loss difference}
  Let $\hat L(\beta) = \|S(A\beta - y)\|_1$ with $S$ as in the
  first stage of \cite{dasgupta2009sampling}\\
  \textbf{Lemma} \ For any $\beta$, with probability
  $1-e^{-\Theta(\frac{r\epsilon^2}{d\sqrt d})}$, we have:
%  s.t. $L(\hat\beta)\leq C\cdot L(\beta^*)$, we have:
  \begin{align*}
(*)\qquad L(\beta) - L(\beta^*) \leq   \hat L(\beta) - \hat L(\beta^*)
+\epsilon\cdot\|A(\beta-\beta^*)\|_1.
  \end{align*}
\textbf{Proof} \  Let $X_i = (1-s_i)\cdot \Delta_i$ where
$s_i=\frac{b_i}{p_i}$, 
  $b_i=\mathrm{Bernoulli}(p_i)$ and $\Delta_i=|a_i^\top\beta-y_i| -
  |a_i^\top\beta^*-y_i|$. Note that:
  \begin{align*}
    \sum_i X_i
    &= L(\beta) -
      L(\beta^*) - \big(\hat L(\beta) - \hat L(\beta^*)\big)
    \quad\text{and}\quad
    \E\Big[\sum_i X_i\Big] = 0.
  \end{align*}
  We next use the following version of Bernstein's inequality:
\[  \Pr\Big(\sum_i X_i\geq t\Big)\leq\exp\bigg(-\frac{\frac12
    t^2}{\sum_i\E[X_i^2] + \frac13Mt}\bigg),\]
where $|X_i|\leq M$ almost surely.
\end{frame}

\begin{frame}
  \frametitle{Applying Bernstein's inequality}
Using the triangle inequality, and (14,15) from
 \cite{dasgupta2009sampling}, we have:
  \begin{align*}
    &|X_i|
    \leq \frac{|\Delta_i|}{p_i}\leq
      \frac{|a_i^\top(\beta - \beta^*)|}{p_i} \leq
      C_1d\sqrt d\cdot \frac{\|A(\beta-\beta^*)\|_1}{r},
  \end{align*}
where $r$ is the sample size. Next, similarly as in (18), we have:
  \begin{align*}
    \sum_i\E[X_i^2]\leq C_2d\sqrt d\cdot \frac{\|A(\beta-\beta^*)\|_1^2}{r}.
  \end{align*}
  Applying Bernstein's inequality, we get:
  \begin{align*}
    \hspace{-7mm}
    \Pr\Big(\sum_iX_i\geq \epsilon\cdot L(\beta^*)\Big)
    \leq e^{-\Theta\big(\frac{r\epsilon^2 \|A(\beta-\beta^*)\|_1^2}{d\sqrt d \|A(\beta-\beta^*)\|_1^2\cdot (1+\epsilon)}\big)}
    \leq e^{-\Theta(\frac{r\epsilon^2}{d\sqrt d})}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Using the estimate of the loss difference}
Let $B$ and $B_\epsilon$ be as in (8) of
  \cite{dasgupta2009sampling}, but centered at $A\beta^*$.\\
  Since the size of the net is $(\frac{36}{\epsilon})^d$, as long as
  $r\geq C d^{2.5}\log(\frac1{\epsilon\delta})/\epsilon^2$, with
  probability $1-\delta$, all points from $B_\epsilon$ satisfy $(*)$. \\[2mm]
 Note that $\hat\beta\in\argmin_\beta \hat
  L(\beta)$ with constant probability lies in $B$, and that for
  all $\beta\in B$ we have $\|A(\beta-\beta^*)\|_1\leq C\cdot L(\beta^*)$.\\
Choose $y_\epsilon=A\beta_\epsilon\in B_\epsilon$ such that
  $\|A(\hat\beta-\beta_\epsilon)\|_1\leq \epsilon\cdot
  L(\beta^*)$. Then, with constant probability:
  %  So $\hat
  % L(\hat\beta) - \hat L(\beta^*)\leq 0$, and we get:
  \begin{align*}
    L(\hat\beta) - L(\beta^*)
    &\leq \|A(\hat\beta-\beta_\epsilon)\|_1 +  L(\beta_\epsilon) -
      L(\beta^*)
    \\
    &\overset{(a)}\leq O(\epsilon)\cdot L(\beta^*) +
      \hat L(\beta_\epsilon) -\hat L(\beta^*)
\\
&\leq O(\epsilon)\cdot L(\beta^*) + \|SA(\hat\beta-\beta_\epsilon)\|_1
     + \hat L(\hat\beta) - \hat L(\beta^*)
    \\
    & \overset{(b)}{\leq} O(\epsilon)\cdot L(\beta^*),      
  \end{align*}
  where in $(a)$ we used $(*)$ for $\beta_\epsilon$ and in $(b)$ we
  used $\ell_1$-subspace embedding and the fact 
  that $\hat L(\hat\beta) - \hat L(\beta^*)\leq 0$.
\end{frame}

\begin{frame}
  \frametitle{Extending to $\ell_p$ for $p>1$}
  Note that the analysis relies on the fact that:
  \begin{align*}
|a_i^\top\beta-y_i| - |a_i^\top\beta^* - y_i| \leq |a_i^\top(\beta-\beta^*)|.
  \end{align*}
  This inequality no longer holds for $p>1$. We can only use a weaker
  bound, which does not eliminate the dependence on $y$:
  \begin{align*}
    |a_i^\top\beta-y_i|^p - |a_i^\top\beta^* - y_i|^p
    &\leq
    2^{p-1}\Big(|a_i^\top(\beta-\beta^*)|^p + |a_i^\top\beta^* -
      y_i|^p\Big)
    \\
    &\quad - |a_i^\top\beta^* - y_i|^p.
  \end{align*}
  However, we know that the same type of sampling probabilities achieve
  a $(1+\epsilon)$-approximation for $p=2$\\[5mm]

  \emph{Question}: How can we unify the analysis for $1\leq p\leq 2$?
\end{frame}

\begin{frame}
  \frametitle{Extending to $\ell_2$}
  Note that we have:
  \begin{align*}
    (a_i^\top\beta-y_i)^2-(a_i^\top\beta^*-y_i)^2 =
    (a_i^\top\beta)^2-(a_i^\top\beta^*)^2
    +2y_ia_i^\top(\beta^*-\beta)
  \end{align*}
   Let $X_i = (1-s_i)\cdot \Delta_i$ where
$s_i=\frac{b_i}{p_i}$, 
  $b_i=\mathrm{Bernoulli}(p_i)$ and
  $\Delta_i=y_ia_i^\top(\beta^*-\beta)$. Suppose that for any $\beta$,
  we have $(a_i^\top\beta)^2/p_i\leq f(d)\cdot \|\beta\|^2/m$.
  \begin{align*}
    \sum_i\E[X_i^2]
    &\leq
    \sum_i\frac C{p_i}\cdot y_i^2(a_i^\top(\beta^*-\beta))^2
    \\
&\leq C\sum_iy_i^2\frac{f(d)\|\beta^*-\beta\|^2}{m}
    \\
    &\leq
    C f(d)\|y\|^2 \|\beta^*-\beta\|^2/m
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Handling $\ell_p$}
  For simplicity, suppose that $y\geq a\geq  0$, and let
  $p\in[1,2]$. Then:
  \begin{align*}
a\cdot p (y-a)^{p-1}\leq y^{p} - (y-a)^{p}\leq a\cdot py^{p-1}.
  \end{align*}
For $p=1$, the upper and lower bounds are the same.\\
For $p=2$, the exact value is the average of the bounds.\\[3mm]

Moreover, for any $p\in[1,2]$ and $\epsilon>0$, we have one of two cases:
\begin{enumerate}
\item Either: $y^p - (y-a)^{p}\leq a^{p}\cdot O(p/\epsilon^{p-1})$,
  \item Or: $y^{p} - (y-a)^{p} = (1\pm\epsilon)\cdot a\cdot p  y^{p-1}$.
\end{enumerate}

\end{frame}

\begin{frame}
  \frametitle{Handling $\ell_p$}
  We suppose that the following holds:
  \begin{align*}
    \|A\beta-y\|_p^p-\|y\|_p^p&\approx p (y^{p-1})^\top A\beta\\
        \|S(A\beta-y)\|_p^p-\|S y\|_p^p&\approx p (y^{p-1})^\top S^p A\beta
  \end{align*}
  We now wish to bound the following:
  \begin{align*}
    (y^{p-1})^\top (I-S^p) A\beta = \sum_{i}(1-\tfrac{b_i}{mq_i})y_i^{p-1}a_i^\top\beta
  \end{align*}
  % Consider the case of $p=2$:
  % \begin{align*}
  %   \Big|\sum_{i}(1-\tfrac{b_i}{mq_i})y_ia_i^\top\beta\Big|\leq
  %   \sqrt{\sum\nolimits_i\big|1-\tfrac{b_i}{mq_i}\big|\cdot y_i^2}\cdot
  %   \sqrt{\sum\nolimits_i\big|1-\tfrac{b_i}{mq_i}\big|\cdot|a_i^\top\beta|^2}
  % \end{align*}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{References}
  \scriptsize 
  \bibliographystyle{alpha}
  \bibliography{../pap}
\end{frame}

\end{document}