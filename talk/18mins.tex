\documentclass[usenames,dvipsnames]{beamer}
%\beamertemplateshadingbackground{brown!70}{yellow!10}
\mode<presentation>
{
    \usetheme{Warsaw}
    %\usecolortheme{crane}
    % or ...

%    \setbeamercovered{transparent}
    % or whatever (possibly just delete it)
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]{}
\usefonttheme{serif}
\setbeamertemplate{headline}{}

%\usepackage[dvipsnames]{xcolor}

\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usepackage{mathtools}
\usepackage{relsize}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{colortbl}
%\usepackage{multicol}
\usepackage{cancel}
\usepackage{multirow}
\usepackage{forloop}% http://ctan.org/pkg/forloop


\newcommand{\setdiff}{\Delta}
\newcommand{\hf}{\wh{f}}
\newcommand{\hg}{\wh{g}}
\newcommand{\hh}{\wh{h}}
\newcommand{\wh}{\widehat}
\newcommand{\wt}{\widetilde}
\newcommand{\ov}{\overline}

\def\bx{{\bf x}}

\def\P{\mbox{\rm P}}
\newcommand{\eps}{\varepsilon}
\newcommand{\Ot}{\widetilde{O}}

\newcommand{\norm}[1]{\|#1\|}
\newcommand{\abs}[1]{|#1|}
\newcommand{\NP}{\mbox{\rm NP}}
\newcommand{\AC}{\mbox{\rm AC}}
\newcommand{\opt}{\mathsf{opt}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\rev}{\mathsf{rev}}
\newcommand{\Tr}{\mathsf{Tr}}
\newcommand{\val}{\mathsf{val}}
\newcommand{\supp}{\mathsf{supp}}
\newcommand{\diag}{\mathsf{diag}}
\newcommand{\sign}{\mathsf{sign}}
\newcommand{\poly}{\mathsf{poly}}
\newcommand{\poiss}{\mathsf{Poisson}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\cov}{\mathsf{cov}}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\E}{\mathbb{E}}
\newcommand{\empt}{\mathsf{empt}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\FF}{\mathcal{F}}
\renewcommand{\i}{\mathbf{i}}
\newcommand{\midd}{\mathsf{mid}}
\newcommand{\Mc}{\mathcal{M}}


\DeclareMathOperator*{\argmax}{arg\,max}

\DeclareMathOperator{\Gaussian}{Gaussian}
\DeclareMathOperator{\rect}{rect}
\DeclareMathOperator{\sinc}{sinc}
\DeclareMathOperator{\Comb}{Comb}
\DeclareMathOperator{\signal}{signal}


\AtBeginSection[]
{
\begin{frame}<beamer>
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}
}

%\input{../shortdefs}

\title[]{Query Complexity of
  Least Absolute Deviation Regression
  via Robust Uniform Convergence}

\author[]{Xue Chen\thanks{George Mason University $\rightarrow$ USTC, China} and Micha{\l }
  Derezi\'{n}ski\thanks{UC Berkeley $\rightarrow$
    University of Michigan}}
\institute{Conference on Learning Theory (COLT 2021)}
\date{}
\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
  \frametitle{Linear regression}
\centerline{\includegraphics[width=.6\textwidth]{figs/regression-simple}}

\vfill

\vspace{3mm}
Given a loss function $\ell(\cdot)$ such as $\ell(x)=x^2$ and $\ell(x)=|x|$,
\begin{align*}
\text{let }  L(\beta)=\sum_{i=1}^n \ell(a_i^\top \beta-y_i) \text{ and } \textcolor{OliveGreen}{\beta^*}=\argmin_\beta L(\beta).
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Linear regression with hidden labels}
  \centerline{
    \only<1>{\includegraphics[width=.6\textwidth]{figs/regression-support}}%
    \only<2>{\includegraphics[width=.6\textwidth]{figs/regression-subset}}
    }
\vfill
\onslide<2>

\begin{enumerate}
\item Given $A=(a_1,\ldots,a_n)$, query a subset of labels $\textcolor{blue}{\{y_i\}_{i\in S}}$,
\item Construct an estimate $\textcolor{blue}{\wt{\beta}}$ minimizing the generalization error to all points
  \end{enumerate}
\vfill  
\end{frame}

\begin{frame}
  \frametitle{Query complexity}
\centerline{\includegraphics[width=.6\textwidth]{figs/regression-all}}
\vspace{5mm}

\textbf{Question}: How many queries needed to construct $\textcolor{blue}{\wt{\beta}}$ such
that
\begin{align*}
 \text{w.h.p.}, \qquad L(\textcolor{blue}{\wt{\beta}}) \leq (1+\epsilon)\cdot L(\textcolor{ForestGreen}{\beta^*})?
  \end{align*}
\end{frame}


\begin{frame}
  \frametitle{Our results}
\textbf{Prior work}: Only for \textit{least squares} regression, i.e.,
  $\ell(x) = x^2$\\[1mm]
\center \small{To name a few: \textcolor{blue}{[Drineas-Mahoney-Muthukrishnan 2006, \ldots, Derezi\'nski-Warmuth 2018, Chen-Price 2019]}} \\[3mm]
%Dasgupta-Drineas-Harb-Kumar-Mahoney 2009
\begin{block}{Our focus}
Query complexity of \underline{robust} regression: \\ \center $\ell(x)=|x|$, least absolute deviation (a.k.a. $\ell_1$ loss)
\end{block}

\centerline{\includegraphics[scale=0.6]{figs/ell1_vs_ell2.png}}
\end{frame}

\begin{frame}
\frametitle{Main result (I)}
\textbf{Our 1st result}: Query complexity of \underline{robust} regression\\[4mm]

%\begin{itemize}
%\item least absolute deviations, $l(x) = |x|$, in $d$ dimensions:
\begin{theorem}[least absolute deviations]
For $(1+\epsilon)$-approximation of $d$-dimensional regression with $\ell_1$ loss, $\ell(x) = |x|$, and success prob.~$1-\delta$,
  \begin{align*}
 \Omega\left(\frac{d+\log \frac{1}{\delta}}{\epsilon^2} \right) \quad \leq\quad \textit{Query Complexity}
    \quad\leq\quad
    O\left( \frac{d \log \frac{d}{\eps \delta}}{\epsilon^2} \right)
  \end{align*}
  \end{theorem}

  \vspace{3mm}
  \pause

  \begin{itemize}
	  \item Tight up to a log factor.
	  \item Admits an efficient algorithm in time $\tilde{O}(nnz(A)+d^{\omega})$.
	  \item The dependence is $\log \frac{1}{\delta}$ on the failure prob.~$\delta$, much better than $\Theta(\frac{1}{\delta})$ for $\ell_2$ loss.
	  \item Independently, \textcolor{blue}{[Parulekar-Parulekar-Price 21]} showed $\Omega(d \log \frac{1}{\delta})$ and a similar upper bound.
  \end{itemize}
\end{frame}

\begin{frame}  
\frametitle{Main result (II)}
\textbf{Our 2nd result}: Query complexity of \underline{$\ell_p$ loss} regression\\[4mm]

\begin{theorem}[$\ell_p$ regression]
For $(1+\epsilon)$-approximation of $\ell_p$ loss regression whose $\ell(x) = |x|^p$ for $p\in(1,2)$ and success prob.~$1-\delta$,
  \begin{align*}
\textit{Query Complexity}
    \quad\leq\quad
    O(d^2\log(d/\epsilon)/\epsilon^2\delta)
  \end{align*}
  \end{theorem}

  \vspace{3mm}
  \pause

 The two upper bounds for $\ell_1$ and $\ell_p$ losses are obtained under the same framework: \\
  	\centerline{ \emph{Lewis weights sampling \& robust uniform convergence}.}
\end{frame}

\begin{frame}
  \frametitle{Our framework}
  \textbf{Main algorithmic tool}: 
  \begin{quote}Importance sampling via \underline{Lewis Weights} \textcolor{blue}{[Lewis 1978, Cohen-Peng 2015]}
    \end{quote}
  \vspace{1mm}    
  \begin{block}{Definition: Lewis weights}
  	Given $A \in \mathbb{R}^{n \times d}$ and the norm $\ell_p$, its Lewis weights $(w_1,\ldots,w_n)$ are the \emph{unique} solution of this \emph{set} of equations:
  	\[
  		\forall i\in [n], \quad a_i^{\top} \cdot (A^{\top} \cdot \textrm{diag}(w_1,\ldots,w_n)^{1-2/p} \cdot A)^{-1} \cdot a_i=w_i^{2/p}.
  	\]
  \end{block}
  \pause
  \vspace{3mm}      
  \begin{itemize}
    \item Equivalent to the leverage scores when $p=2$.
    \item Subspace embedding \textcolor{blue}{[Lewis 1978, Talagrand 1990]}.
    \item \textcolor{blue}{Cohen \& Peng (2015)} provided efficient approx.~algorithms.
  	\item For convenience, focus on $\ell_1$ loss and $\ell_1$ Lewis weights.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Our algorithm}
  \textbf{Description}: 
  \begin{itemize}
  \item Given $A$, apply \textcolor{blue}{[Cohen-Peng 2015]} to compute a 2-approximation of $A$'s Lewis weights and produce a sparse diagonal matrix $W \in \mathbb{R}^{n \times n}$.
  \item Return $\tilde{\beta}:=\underset{\beta}{\arg\min} \|W A \beta - W y\|_1$.
  \end{itemize}
  \begin{block}{Rest of the talk}
  	Question: \emph{$y$ is \textcolor{brown}{unknown}} --- why is it a $(1+\epsilon)$-approximation (w.h.p.)?
  \end{block}

\pause
  
  \textbf{Previous results}:  
  \begin{itemize}
  \item $y$ is known {\small \textcolor{blue}{[subspace embedding \& randomized numerical linear algebra]}}: Compute the Lewis weights of $[A; y]$.
  \item $y$ is unknown {\small \textcolor{blue}{[folklore]}}: 8-approximation with prob.~0.6. %Dasgupta-Drineas-Harb-Kumar-Mahoney 2009
  \end{itemize}
\end{frame}

\begin{frame}  
  \frametitle{Our analysis}
  \begin{quote}
  	New idea: \underline{Robust Uniform Convergence}.
  \end{quote}

\vspace{2mm}
\begin{block}{Recall \underline{Uniform Convergence} in statistical learning}
Given $L(\beta)$, $\tilde{L}(\beta)$ satisfies \emph{Uniform Convergence} (with multi.~error $\epsilon$) only if \\
\center for all $\beta$, $\tilde{L}(\beta) = (1\pm \epsilon) L(\beta)$.
\end{block}
{\small Then $\tilde{\beta}:=\arg\min \tilde{L}(\beta)$ is a $1+O(\epsilon)$ approximation.}

\vspace{3mm}
\pause 

\begin{quote}
\bf{Issue}: \emph{Uniform Convergence} requires $\Omega(n)$ queries for unbounded $y$.
\end{quote}

\begin{block}{Example}
$d=1, A=(1,\ldots,1)$, $y=(+\infty,0,\ldots,0)$ and~$L(\beta)=\|A \beta - y\|_1$ such that $\beta^*=0$ and $L(\beta^*)=y_1$. 

On the other hand, $\tilde{L}(\beta)=\|W A \beta\|_1$ unless we query $y_1$.
\end{block}
\end{frame}

\begin{frame}
\frametitle{New definition}
{\bf Intuition}: $\ell_1$ loss is \emph{insensitive} to outliers \\

$\Rightarrow$ The outliers not shown in $\tilde{L}$ (like $y_1$ in the last ex.) are ignored by $\beta^*:=\arg\min \|A \beta - y\|_1$.

\vspace{3mm}

\pause 
\begin{block}{Robust Uniform Convergence}
{\small Given $L(\cdot)$ and $\tilde{L}(\cdot)$, $\tilde{L}(\cdot)$ satisfies \emph{\underline{Robust} Uniform Convergence} if }

\vspace{2mm}

\centerline{for all $\beta$, \qquad $\tilde{L}(\beta) + \textcolor{brown}{\Delta} = (1\pm \epsilon) L(\beta)$,} 

\vspace{2mm}

{\small where $\textcolor{brown}{\Delta}:=L(\beta^*)-\tilde{L}(\beta^*)$ (denotes the outliers not shown in $\tilde{L}$ informally).}
\end{block}

\vspace{5mm}

Next: (1) It implies a $1+\epsilon$ approximation. \\
(2) How to prove this convergence for all $\beta$.
\end{frame}

\begin{frame}
\frametitle{Approximation ratio}
\begin{block}{Robust Uniform Convergence}
%For all $\beta$, \qquad $\tilde{L}(\beta) + \textcolor{brown}{\Delta} = (1\pm \epsilon) L(\beta)$, \quad where $\textcolor{brown}{\Delta}:=L(\beta^*)-\tilde{L}(\beta^*)$ is fixed.
\centerline{For all $\beta$, $\tilde{L}(\beta) + \textcolor{brown}{\Delta} = (1\pm \epsilon) L(\beta)$ with $\textcolor{brown}{\Delta}:=L(\beta^*)-\tilde{L}(\beta^*)$.} 
\end{block}

\vspace{5mm}

We bound the approx.~ratio of $\tilde{\beta}:=\arg\min \tilde{L}(\beta)$:
\begin{align*}
L(\tilde{\beta}) & \le \frac{\tilde{L}(\tilde{\beta}) + \Delta}{1 - \epsilon} \\
& \le \frac{\tilde{L}(\beta^*) + \Delta}{1 - \epsilon} \\
& \le \textcolor{brown}{\frac{1+\epsilon}{1-\epsilon}} \cdot L(\beta^*).
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Prove the Convergence}
\begin{block}{Robust Uniform Convergence}
\centerline{For all $\beta$, $\tilde{L}(\beta) + \textcolor{brown}{\Delta} = (1\pm \epsilon) L(\beta)$ with $\textcolor{brown}{\Delta}:=L(\beta^*)-\tilde{L}(\beta^*)$.} 
\end{block}

\vspace{2mm}

Main ingredients: \\
\centerline{ The contraction principle \footnote{\textcolor{blue}{\small [Slepian 60s, Sudakov 70s, Fernique 70s, Ledoux-Talagrand 1990]}} + subspace embedding \footnote{\textcolor{blue}{\small [Talagrand 1990, Cohen-Peng 2015]}}.}

\vspace{2mm}

\begin{enumerate}
\item Rewrite it as $\tilde{L}(\beta)-\tilde{L}(\beta^*)=L(\beta)-L(\beta^*) \pm \epsilon \cdot L(\beta)$.
\item Consider the concentration of L.H.S for $\ell_1$ loss:
\begin{align*}
\tilde{L}(\beta)-\tilde{L}(\beta^*) & = \sum_{i=1}^N W_i \cdot \textcolor{brown}{\left( |a_i^{\top} \beta -y_i|-|a_i^{\top} \beta^* -y_i| \right)}.
\end{align*}
% & = \sum_{i=1}^N W_i \cdot |a_i^{\top} \beta - y_i| - \sum_{i=1}^N W_i \cdot |a_i^{\top} \beta^* - y_i|\\
All \textcolor{brown}{vectors} in the sum \emph{contract} from $( |a_i^{\top} \beta - a_i^{\top} \beta^* | )_{i \in [N]}$.
%$( |a_i^{\top} \beta -y_i|-|a_i^{\top} \beta^* -y_i| )_{i \in [N]}$
\end{enumerate}
\end{frame}
\iffalse
\begin{frame}
\frametitle{More about our analysis}
\begin{block}{Lower bound of $\ell_1$ loss}
$ \Omega\left(\frac{d+\log \frac{1}{\delta}}{\epsilon^2} \right)$ follows from the classical biased coin problem.
\end{block}
Independently, \textcolor{blue}{[Parulekar-Parulekar-Price 21]} showed $\Omega(d \log \frac{1}{\delta})$.

\pause 

\begin{block}{Upper bound of $\ell_p$ loss}
\small 

$\tilde{L}(\beta)-\tilde{L}(\beta^*) = \sum_{i=1}^N W_i \cdot \textcolor{brown}{\left( |a_i^{\top} \beta -y_i|^p - |a_i^{\top} \beta^* -y_i|^p \right)}$ --- the contraction principle can NOT remove the cross terms between $a_i^{\top} \beta$ and $y_i$ for $p>1$.
\end{block}
Our bound $O(d^2\log(d/\epsilon)/\epsilon^2\delta)$ is obtained via investigating properties of the Lewis weights.

\vspace{4mm}

\begin{quote}
\centerline{Open: A upper bound like $\tilde{O}(\frac{d}{\eps^{2/p}})$ for $p \in [1,2]$?}
\end{quote}

\end{frame}
\fi
\begin{frame}
\frametitle{Summary}
\begin{itemize}
\item An efficient $(1+\eps)$-approximation algorithm for the least absolute deviation regression with $\tilde{\Theta}(\frac{d}{\eps^2})$ queries.
\item Lewis Weights: Extremely useful for $\ell_p$ subspace embedding and importance sampling.
\item Robust Uniform Convergence: A correction term removes the effect of outliers.
\end{itemize}

\vspace{3mm}

\begin{block}{Open Questions}
\begin{enumerate}
\item A upper bound like $\tilde{O}(\frac{d}{\eps^{2/p}})$ for $p \in [1,2]$?
\item How about Huber, Tukey, and other loss functions?
\item Non-linear regression problems like logistic regress?
\end{enumerate}
\end{block}

\vspace{3mm}

\centerline{\huge Thank you!}
\end{frame}
\end{document}