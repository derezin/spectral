function [S,ZS,ZS0]= VolumeSamplePlus(X, s)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [n,d] = size(X);
    %Z = inv(X'*X);
    %p = 1 - sum(X.*(X*Z),2);

    S0 = VolumeSample(X,d,0);
    
    
    XS0 = X(S0,:);
    ZS0 = inv(XS0'*XS0);
    if s>d
        q = sum(X.*(X*ZS0),2);
        q = q / sum(q);
        T = randsample(1:n,s-d,true,q);
        XT = diag(1./sqrt(n*q(T))) * X(T,:);
        ZS = inv(XS0'*XS0 + XT'*XT);
        S = [S0,T];
    else
        ZS = ZS0;
        S = S0;
    end
    ZS = (s/n)*ZS;
end