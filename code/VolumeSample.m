function S = VolumeSample(X, s,lambda,Z,p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
    [a,b] = size(X);
    n = max(a,b);
    d = min(a,b);
    if a > b, X = X'; end
    
    if nargin < 4
        Z = inv(X*X'+lambda * eye(d));
    end
    if nargin < 5
        p = 1 - sum(X.*(Z*X))';
    end
    S = true(n,1);
    all_ind = 1:n;
    for t=n:-1:s+1
        if sum(p(S)) > 0
            i = randsample(all_ind(S),1,true,p(S));
        else
            i = randsample(all_ind(S),1,true);
        end
        v = Z*X(:,i) / sqrt(p(i));
        S(i) = false;
        p = max(0, p - (X'*v).^2);
        Z = Z + v*v';
    end
    S = all_ind(S);
end

