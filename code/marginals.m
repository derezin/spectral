% marginals of adaptive leverage scores

n=4,d=2,k=7

X=randn(n,d);
[n,d] = size(X);
P=X*pinv(X'*X)*X';
lev = diag(P)./d;
%lev = rand(n,1); lev = lev./sum(lev); 
%lev = ones(n,1) / n;
Ilev = diag(1./sqrt(lev));
Xlev = Ilev*X;

Z = X'*X;

Ptot = 0;
Pmarg = zeros(k,1);
r = 1;

S = cartesian(1:n,k);
for i=1:length(S)
    %PS = prod(lev(S(i,:))) * det( Xlev(S(i,1:d),:) )^2 / det(Z); %'*Xlev(S(i,1:d),:) );
    PS = det( X(S(i,1:d),:) )^2 / det(Z);
    if PS > 0
        for t=d+1:k
            Xtm = Xlev(S(i,1:t-1),:);
            xt = X(S(i,t),:)';
            Xtminv = pinv(Xtm'*Xtm);
            PS = PS * xt'*Xtminv*xt / trace(Xtminv*Z);
        end        
    end
    for t=1:k
        if S(i,t)==r
            Pmarg(t) = Pmarg(t) + PS;
        end
    end    
    Ptot = Ptot + PS;
end

origFormat = get(0, 'format');
format('long');

% A new family of Cauchy-Binet formulas
Cauchy_Binet = Ptot ./ factorial(d)

Pmarg = Pmarg ./ Ptot;

% Computing the marginal probability of the r-th row in each step
Marginals = Pmarg ./ lev(r)

set(0,'format', origFormat);
