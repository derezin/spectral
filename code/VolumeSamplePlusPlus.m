function [S,ZS,ZS0]= VolumeSamplePlusPlus(X, s)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [n,d] = size(X);

    S0 = VolumeSample(X,d,0);
    
    
    XS0 = X(S0,:);
    ZS0 = inv(XS0'*XS0);
    ZS = ZS0;
    S = S0;

    for k=d+1:s        
        q = sum(X.*(X*ZS),2);
        q = q / sum(q);
        i = randsample(1:n,1,true,q);
        S = [S,i];
        xi = X(i,:)'; %(1/sqrt(n*q(i))) * X(i,:)';
        v = ZS*xi;
        ZS = ZS - (1/(1+v'*xi))*v*v';
        %ZS = inv((d/s)*XS0'*XS0 + (1/s)*XT'*XT);
    end
    
    
    ZS = (s/n)*ZS;
end