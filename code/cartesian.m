function C = cartesian(seq,n)
    
    [F{1:n}] = ndgrid(seq);

    for i=n:-1:1
        G(:,i) = F{i}(:);
    end

    C = unique(G , 'rows');
end