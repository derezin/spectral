function [] = testdplusk(sample_fun,d_max,reps)

    r = 10;

    d_list = ceil(logspace(1,2.4,20));
    d_num = length(d_list);
    E_small = zeros(d_num,1);
    E_missed = zeros(d_num,1);

    a_list = [1/2,2/3,3/4,1];
    a_num = length(a_list);

    set(0,'DefaultAxesFontSize', 15);
    set(0,'DefaultTextFontSize', 15);
    figure('Color','white','Position', [500, 500, 800, 500])

    for k=1:a_num
        a = a_list(k);
        for j=1:d_num    
            d = d_list(j);
            [j,d_num,d]
            n = d+ ceil(d^a);
            sm = 10;
            extra = 2*d;
            for i=1:r
                if mod(i,5)==1
                    X = randn(n,d);
                end
                [S,ZS] = VolumeSamplePlusPlus(X,d+extra);
                ee = eig(ZS);
                E_small(j) = E_small(j) + sum(ee > sm)/r;
                E_missed(j) = E_missed(j) + (n-length(unique(S)))/r;
            end
        end
        subplot(2,2,k);
        ph_list = [];
        legend_list = {};
        hold on
        ph_list(end+1) = plot(d_list,E_small,'b','LineWidth',2);
        legend_list{end+1} = sprintf('eigs < 1/10');
        ph_list(end+1) = plot(d_list,E_missed,'b--','LineWidth',2);
        legend_list{end+1} = sprintf('points missed');
        hold off
        legend(ph_list,legend_list,'Location','best');
        xlabel('dimension d');
        ylabel('number of ...');
        title(sprintf('n = d + d^{%.2f}',a));
    end

end
