% Testing the d+sqrt(d) example

r = 20;

d_list = ceil(logspace(1,3.3,20));
d_num = length(d_list);
E_small1 = zeros(d_num,1);
E_small2 = zeros(d_num,1);
E_small3 = zeros(d_num,1);
E_missed2 = zeros(d_num,1);
E_missed3 = zeros(d_num,1);

for j=1:d_num    
    d = d_list(j);
    [j,d_num,d]
    n = d+ceil(d^(2/3)); %+ 2*ceil(sqrt(d));
    sm = 1/10;
    for i=1:r
        if mod(i,5)==1
            X = randn(n,d);
            X = diag(1./sqrt(sum(X.^2,2))) * X;
            [U,~,~] = svd(X,'econ');            
            Un = diag(1./sqrt(sum(U.^2,2))) * U;
            %Un = sqrt(n/d) * U;
            %Z = inv(U'*U);
            %p = 1 - sum(U'.*(Z*U'))';
        end
%         S = VolumeSample(U,d,0,Z,p);
%         S2 = VolumeSample(U,d,0,Z,p);
%         S3 = VolumeSample(U,d,0,Z,p);
        S = randperm(n,d);
        S2 = randperm(n,d);
        S3 = randperm(n,d);
        US = Un(S,:); % or Un
        US2 = Un(S2,:);
        US3 = Un(S3,:);
        ee = eig(US'*US);
        E_small1(j) = E_small1(j) + sum(ee < sm)/r;
        ee2 = eig((1/2)*(US'*US + US2'*US2));
        E_small2(j) = E_small2(j) + sum(ee2 < sm)/r;        
        E_missed2(j) = E_missed2(j) + (n-length(unique([S,S2])))/r;
        ee3 = eig((1/3)*(US'*US + US2'*US2 + US3'*US3));
        E_small3(j) = E_small3(j) + sum(ee3 < sm)/r;     
        E_missed3(j) = E_missed3(j) + (n-length(unique([S,S2,S3])))/r;     
    end
end

set(0,'DefaultAxesFontSize', 15);
set(0,'DefaultTextFontSize', 15);
figure('Color','white','Position', [500, 500, 500, 300])

ph_list = [];
legend_list = {};
% ph_list(end+1) = plot(d_list,sqrt(d_list),'r');
% legend_list{end+1} = 'sqrt(d)';
hold on
% ph_list(end+1) = plot(d_list,E_small1,'r','LineWidth',2);
% legend_list{end+1} = sprintf('1xd, eigs < 1/10');
ph_list(end+1) = plot(d_list,E_small2,'b','LineWidth',2);
legend_list{end+1} = sprintf('2xd, eigs < 1/10');
% ph_list(end+1) = plot(d_list,E_missed2,'b--','LineWidth',2);
% legend_list{end+1} = sprintf('2xd, missed');
ph_list(end+1) = plot(d_list,E_small3,'m','LineWidth',2);
legend_list{end+1} = sprintf('3xd, eigs < 1/10');
% ph_list(end+1) = plot(d_list,E_missed3,'m--','LineWidth',2);
% legend_list{end+1} = sprintf('3xd, missed');
hold off
legend(ph_list,legend_list,'Location','best');
xlabel('dimension d');
ylabel('number of small eigenvalues');
