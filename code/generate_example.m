% generate_example

%regression_example('../talk/figs/regression-points.eps','simple',0,1,0);
regression_example('../talk/figs/regression-simple.eps','simple',1,1,0);
regression_example('../talk/figs/regression-support.eps','simple',0,0,0);
regression_example('../talk/figs/regression-subset.eps','subset',0,0,0);
%regression_example('../talk/figs/regression-most.eps','subset',1,1,0);
regression_example('../talk/figs/regression-all.eps','subset',1,1,0);
