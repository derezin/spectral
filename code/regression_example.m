function regression_example(filename, model, withline, withlabels, withmax)
% Linear Regression Example

% model = 'subset';
% withline = 1;
% withlabels = 1;

X = [0.8876
    0.6286
    0.7022
    0.0303
    0.9136
    0.9864
    0.3937
    0.3197
    0.7213
    0.0174]';

y = [0.9103
    0.8082
    1.5340
    0.5635
    1.6822
    1.0946 %0.7 %
    1.0945
    0.4945
    1.2081
    0.4200];


% n = 10;
% X = rand(1,n);
% w_star = 1;
% eps = 1;
% y = X' * w_star + eps * rand(n,1);

n=length(y);

w = inv(X*X')*X*y;

set(0,'DefaultAxesFontSize', 20);
set(0,'DefaultTextFontSize', 20);
myfig = figure('Color','white','Position', [100, 100, 500, 400]);

pcol = [0.8,0.8,0.8];

hold on

x_list = [0,max(X)];
if withline
    plot(x_list,w'*x_list,'Color',[0,0.8,0],'LineWidth',3);
end;

if withlabels
    line([X;X],[zeros(1,n);y'],'Color','black','LineWidth',1,'LineStyle',':');
    scatter(X,y,200,'MarkerEdgeColor',[0 0 0],...
                  'MarkerFaceColor',pcol,...
                  'LineWidth',1.5);
    
end;

scatter(X,zeros(1,n),50,'MarkerEdgeColor',[0 0 0],...
                        'MarkerFaceColor',pcol,...
                        'LineWidth',1.5);



xlabel('a');
ylabel('y');
[~,bad_x] = min(X);
[~,good_x] = max(X);

if withmax
    bcol = [0.8,0.5,0.5];
    scatter(X(good_x),y(good_x),200,'MarkerEdgeColor',bcol,...
                  'MarkerFaceColor',pcol,...
                  'LineWidth',3);
    
    scatter(X(good_x),0,75,'MarkerEdgeColor',bcol,...
                              'MarkerFaceColor',pcol,...
                               'LineWidth',3);
    t1 = text(X(good_x)-0.015,-0.08,'x');
    t = text(X(good_x)+0.01,-0.11,'max');
    t.FontSize = 12;
    t.Color = bcol;
    t1.Color = bcol;

end;
    
if strcmp(model,'norm')

    plot(x_list,[0, x_list(2)*y(good_x)/X(good_x)],'Color','blue','LineWidth',3);
    
    scatter(X(good_x),y(good_x),200,'MarkerEdgeColor','blue',...
                  'MarkerFaceColor',pcol,...
                  'LineWidth',3);
    
    plot(x_list,[0, x_list(2)*y(bad_x)/X(bad_x)],'Color','red','LineWidth',3);
    
    scatter(X(bad_x),y(bad_x),200,'MarkerEdgeColor','red',...
                  'MarkerFaceColor',pcol,...
                  'LineWidth',3);

    L_best = sum((X'*w-y).^2)
    L_good = sum((X'*(y(good_x)/X(good_x))-y).^2)
    L_bad = sum((X'*(y(bad_x)/X(bad_x))-y).^2)
    L_exp = 2*L_best
elseif strcmp(model,'subset')
    s = 3;
    S = 1:s;
    XS = X(S);
    yS = y(S);
    wS = inv(XS*XS')*XS*yS;
    plot(x_list,wS'*x_list,'Color','blue','LineWidth',3);
    
    line([XS;XS],[zeros(1,s);yS'],'Color','black','LineWidth',1,'LineStyle',':');
    scatter(XS,yS,200,'MarkerEdgeColor','blue',...
                  'MarkerFaceColor',pcol,...
                  'LineWidth',3);
    
    scatter(XS,zeros(1,s),75,'MarkerEdgeColor','blue',...
                              'MarkerFaceColor',pcol,...
                               'LineWidth',3);
%     t1 = text(XS(1)-0.015,-0.08,'x');
%     t = text(XS(1)+0.01,-0.11,'i');
%     t.FontSize = 12;
%     t.Color = 'blue';
%     t1.Color = 'blue';
end
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
ylim([0,max(y)+.1]);
xlim([0,max(X)+.1]);
hold off
if ~strcmp(filename,'')
    saveas(myfig,filename,'epsc')
end
