function S = VolumeSampleFast(X, s, lambda)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
    [a,b] = size(X);
    n = max(a,b);
    d = min(a,b);
    if a > b, X = X'; end
    
    Z = inv(X*X'+lambda * eye(d));
    p = 1 - sum(X.*(Z*X))';
    S = true(n,1);
    all_ind = 1:n;
    sample_set = randsample(1:n, ceil(n*log(n)),true,p); 
    sample_ind = 1;
    sample_num = length(sample_set);
    for t=n:-1:s+1
        %fprintf('Sampling %d\n',t);
        while 1
%             R(n-t+1) = R(n-t+1) + 1;
            found_one = false;
            while sample_ind <= sample_num
                i = sample_set(sample_ind);
                sample_ind = sample_ind + 1;
                if S(i)
                    found_one = true;
                    break;
                end
            end
            if ~found_one
                if sum(p(S)) > 0
                    i = randsample(all_ind(S),1,true,p(S));
                else
                    i = randsample(all_ind(S),1,true);
                end
            end
            if t < n
                q = max(0,1 - X(:,i)'*Z*X(:,i));
                %fprintf('Trial %d, i=%d, q=%f, p=%f\n',R(t),i,q,p(i));
                if p(i)>q
                    A = binornd(1,q/p(i));
                else
                    break;
                end
                if A, break; end
            else
                q=p(i);
                break;
            end
        end
%        H(n-t+1) = i;
        v = Z*X(:,i) / sqrt(q);
        S(i) = false;
        Z = Z + v*v';
    end
    S = all_ind(S);
end

