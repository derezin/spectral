function [] = dplusk(sample_fun,d_max,reps)
% sample_fun(X,s) returns:
% S - index sequence of size s
% ZS - inverse of the matrix which approximates X'*X

    d_list = ceil(logspace(2,log10(d_max),20));
    d_num = length(d_list);

    %a_list = [1/2,2/3,3/4,1];
    a_list = [8,4,2,1];
    a_num = length(a_list);

    sm = 8;
    
    set(0,'DefaultAxesFontSize', 15);
    set(0,'DefaultTextFontSize', 15);
    figure('Color','white','Position', [500, 500, 800, 500])

    for k=1:a_num
        a = a_list(k);
        E_small = zeros(d_num,1);
        E_missed = zeros(d_num,1);
        E_min = zeros(d_num,1);
        for j=1:d_num    
            d = d_list(j);
            [j,d_num,d]
            n = d+ ceil(d/a); %ceil(d^a);
            extra = d;
            for i=1:reps
                if mod(i,5)==1
                    X = randn(n,d);
                    [U,~,~] = svd(X,'econ');            
                end
                [S,ZS] = sample_fun(U,d+extra);
                ee = eig(ZS);
%                 E_small(j) = E_small(j) + sum(ee > sm)/reps;
%                 E_missed(j) = E_missed(j) + (n-length(unique(S)))/reps;
                E_min(j) = E_min(j) + max(ee)/reps;
            end
        end
        subplot(2,2,k);
        ph_list = [];
        legend_list = {};
        hold on
%         ph_list(end+1) = plot(d_list,E_small,'b','LineWidth',2);
%         legend_list{end+1} = sprintf('eigs < 1/%d',sm);
%         ph_list(end+1) = plot(d_list,E_missed,'b--','LineWidth',2);
%         legend_list{end+1} = sprintf('points missed');
        ph_list(end+1) = plot(d_list,E_min,'b:','LineWidth',2);
        legend_list{end+1} = sprintf('1/min(eigs)');
        hold off
        legend(ph_list,legend_list,'Location','best');
        xlabel('dimension d');
        ylabel('number of ...');
        %title(sprintf('n = d + d^{%.2f}',a));
        title(sprintf('n = d + d/%d',a));
    end

end
