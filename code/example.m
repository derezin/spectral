% Example usage

dataset = 2;

if dataset == 1
    d = 20;
    n = 10000;
    X = randn(n,d);
elseif dataset == 2
    d=50;
    k=2;
    eps = 0.01;
    n = k*d + 5000;
    X = [repmat(eye(d),k,1);zeros(n-k*d,d)] + eps*randn(n,d);
end


disp('VolumeSampleFast');
S = VolumeSampleFast(X,d,0)

disp('VolumeSample');
S = VolumeSample(X,d,0)

disp('dplusk');
dplusk(@VolumeSamplePlusPlus,250,10)