%!TEX root = robust-colt21.tex
\section{Additional Proofs from Section~\ref{sec:analysis_ell1}}\label{sec:proof_thm_deviation}
Since any row with $p_i \ge 1$ will always be selected in $S$, those rows will not affect the random variable $\wt{L}(\beta)$ considered in this section. We restrict our attention to rows with $p_i < 1$ in this proof. As a warm up, we will first prove the following lemma, which is a simplified version of Lemma~\ref{lem:high_prob_guarantee}. 
\begin{lemma}\label{lem:concentration_deviation}
Given $A \in \mathbb{R}^{n \times d}$ and $\epsilon$, let $w_1,\ldots,w_n$ be the Lewis weight of each row $A_i$. Let $p_1,\ldots,p_n$ be a sequence of numbers upper bounding $w$, i.e.,
\[
p_i \ge w_i/u \text{ for } u =\Theta\left( \frac{\eps^2}{\log (m+d/\eps)} \right) \text{ where } m = \sum_i p_i.
\]
Suppose the coefficients $(s_1,\ldots,s_n)$ in $\wt{L}(\beta):=\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|$ is generated as follows: For each $i \in [n]$, with probability $p_i$, we set $s_i=1/p_i$ (when $p_i \ge 1$, $s_i$ is always 1). Given any subset $B_{\beta} \subset \mathbb{R}^d$ of $\beta$,
\[
\E \left[ \sup_{\beta \in B_{\beta}} \left\{ \wt{L}(\beta)-\wt{L}(\beta^*)-\big( L(\beta)-L(\beta^*) \big) \right\} \right] \le \eps \cdot \sup_{\beta \in B_{\beta}} \|A (\beta-\beta^*)\|_1.
\]
%\E \left[ \sup_{\beta \in B_{\beta}} \left\{ \sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \right\} \right] \le \eps \cdot \sup_{\beta \in B_{\beta}} \|A \beta\|_1.
%Namely if we use $B_{\beta}$ to denote the set of $\beta$ --- $\{\beta|\sum_{i=1}^n \delta_i(\beta) \le B \sum_{i=1}^n |y_i|\}$, \[\E_S\left[ \sup_{\beta \in B_{\beta}}\bigg| \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) - \sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg| \le \eps \cdot \sum_{i=1}^n |y_i|\right]\]
\end{lemma}
We remark that while Lemma~\ref{lem:concentration_deviation} implies that rescaling of $u$ to $u=u/\delta^2$ gives
\[
\text{ w.p. $1-\delta$}, \wt{L}(\beta)-\wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot \sup_{\beta \in B_{\beta}}\|A (\beta-\beta^*)\|_1 \text{ for all } \beta \in B_{\beta},
\] 
Lemma~\ref{lem:high_prob_guarantee} has a better dependency on $\delta$. 

We need a few ingredients about random Gaussian processes to finish the proofs of Lemma~\ref{lem:concentration_deviation} and Lemma~\ref{lem:high_prob_guarantee}. Except the chaining arguments in \cite{CP15_Lewis}, the proof of Lemma~\ref{lem:concentration_deviation} will use the following Gaussian comparison theorem (e.g. Theorem 7.2.11 and Exercise 8.6.4 in \cite{Vershynin}). In the rest of this section, we use $g$ to denote an i.i.d.~Gaussian vector $N(0,1)^d$ when the dimension is clear.
\begin{theorem}[Slepian-Fernique]\label{thm:Slepian_Fernique}
Let $v_0,\ldots,v_n$ and $u_0,\ldots,u_n$ be two sets of vectors in $\mathbb{R}^d$ where $v_0=u_0=\vec{0}$. Suppose that 
\[
\|v_i-v_j\|_2 \ge \|u_i-u_j\|_2 \text{ for all } i,j=0,\ldots,n.
\]
Then $\E_g\left[\max_i \big| \langle v_i, g \rangle \big| \right] \ge C_0 \cdot \E_g \left[\max_i \big| \langle u_i,g \rangle \big| \right]$ for some constant $C_0$.
\end{theorem}
The proof of Lemma~\ref{lem:high_prob_guarantee} follows the same outline except that we will use the following higher moments version for a better dependency on $\delta$.
\begin{corollary}[Corollary 3.17 of \cite{LTbook}]\label{cor:comparison_higher}
Let $v_0,\ldots,v_n$ and $u_0,\ldots,u_n$ be two sets of vectors satisfying the conditions in the above Theorem. Then for any $\ell>0$, 
\[4^{\ell} \cdot \E_g\left[\max_i \big| \langle v_i, g \rangle \big|^{\ell} \right] \ge \E_g \left[\max_i \big| \langle u_i,g \rangle \big|^{\ell} \right].\]
\end{corollary}

We will use the following concentration bound for $\ell_1$ ball when the Lewis weights are uniformly small for all rows. It is an extension of Lemma 8.2 in \cite{CP15_Lewis}; but for completeness, we provide a proof in Section~\ref{sec:ell_1_concentration}.
\begin{lemma}\label{lem:Gaussian_proc_bounded_Lewis}
Let $A$ be a matrix with Lewis weight upper bounded by $u$. For any set $S \subseteq \mathbb{R}^{d}$,
\[
\E_g\left[\max_{\beta \in S} \bigg| \langle g, A \beta \rangle \bigg| \right] \lesssim \sqrt{u \cdot \log n} \cdot \max_{\beta \in S} \|A \beta\|_1.
\]
\end{lemma}
%The last ingredient is the following higher moments bound for Gaussian process (e.g., Exercise 8.6.6 in~\cite{Vershynin}).
%\begin{lemma}\label{lem:Gaussian_high_moment}
%Let $T$ be a subset of $\mathbb{R}^n$. For any $p>1$, we always have 
%\[\E_g[ \sup_{x \in T} |\langle g,x\rangle|^p]^{1/p} \le C \sqrt{p} \cdot \E_g[ \sup_{x \in T} |\langle g,x\rangle|].\]
%\end{lemma}

We finish the proof of Lemma~\ref{lem:concentration_deviation} here and defer the proof of Lemma~\ref{lem:high_prob_guarantee} to Section~\ref{sec:proof_cor_high_prob}.
%we plan to bound the deviation \[
%\wt{L}(\beta)-\wt{L}(\beta^*)=\bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|\] for all $\beta \in B_{\beta}$ simultaneously. In particular, 

\begin{proofof}{Lemma~\ref{lem:concentration_deviation}}
For convenience, we define 
\[
\delta_i(\beta):=|a_i^{\top} \beta - y_i| \text{ such that } \wt{L}(\beta)-\wt{L}(\beta^*)=\sum_i s_i (\delta_i(\beta)-|y_i|)
\] given the assumption $\beta^*=0$ and rewrite $L(\beta)-L(\beta^*)=\sum_i (\delta_i(\beta)-|y_i|)$ similarly. 

So, we have
\[
\E_{S} \left[ \sup_{\beta \in B_{\beta}} \left\{ \wt{L}(\beta)-\wt{L}(\beta^*)-\big( L(\beta)-L(\beta^*) \big) \right\} \right] = \E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \bigg| \right].
\]
Since each $\big( \delta_i(\beta) - |y_i| \big)$ is the expectation of $s_i \cdot \big( \delta_{i}(\beta) - |y_{i}| \big)$, from the standard symmetrization and Gaussianization in \cite{LTbook} (which is shown in  Section~\ref{sec:sym_gau} for completeness), this is upper bounded by
\[
\sqrt{2 \pi} \E_S \left[ \E_{g} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right] \right].
\]

Then we fix $S$ and plan to apply the Gaussian comparison Theorem~\ref{thm:Slepian_Fernique} to the following process.
\[
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j} g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right] = \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \bigg\langle g, \big(s_j \cdot ( \delta_{j}(\beta) - |y_{j}|)\big)_{j \in [n]} \bigg\rangle \bigg| \right].
\]
Let us verify the condition of the Gaussian comparison Theorem~\ref{thm:Slepian_Fernique} to upper bound this by \[C_2 \cdot \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \bigg\langle g, SA\beta \bigg\rangle \bigg| \right].\] Note that for any $\beta$ and $\beta'$, the $j$th term of $\beta$ and $\beta'$ in the above Gaussian process is upper bounded by
\[
\left| s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - s_j \cdot \big( \delta_{j}(\beta') - |y_{j}|\big) \right| \le s_j \cdot \left| \delta_j(\beta)-\delta_j(\beta') \right| \le s_j \cdot \left| a_j^{\top} \beta - a_j^{\top} \beta' \right|.
\]
At the same time, for the $\vec{0}$ vector, we always have
\[
\left| s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - 0 \right| \le s_j \cdot \left| |a_j^{\top} \beta - y_j| - |y_j| \right| \le s_j \cdot |a_j^{\top} \beta|.
\]
Hence, the Gaussian comparison Theorem~\ref{thm:Slepian_Fernique} implies that the Gaussian process is upper bounded by
\[
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j} g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right] \le \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, S A \beta \rangle \bigg| \right].
\]
Next we plan to use Lemma~\ref{lem:Gaussian_proc_bounded_Lewis} to bound this Gaussian process on $SA\beta$.

However, Lemma~\ref{lem:Gaussian_proc_bounded_Lewis} requires that $S A$ has bounded Lewis weight. To show this, the starting point is that if we replace $a_i$ in \eqref{eq:def_Lewis_weight} by $s_i \cdot a_i = \frac{1}{p_i} a_i$, its Lewis weight becomes $w_i/p_i \le u$ by our setup. So we only need to handle the inverse in the middle of \eqref{eq:def_Lewis_weight}. The rest of the proof is very similar to the proof of Lemma~7.4 in \cite{CP15_Lewis}: We add a matrix $A'$ into the Gaussian process to have this property of bounded Lewis weight. Recall that $u \le \frac{\eps^2}{C \log (m + d/\eps)}$ for a large constant $C$. By sampling the rows in $A$ with the Lewis weight and scaling every sample by a factor of $u$, we have the existence of $A'$ with the following 3 properties (see Lemma B.1 in \cite{CP15_Lewis} for the whole argument using the Lewis weight),
\begin{enumerate}
\item $A'$ has $\tilde{O}(d/u)$ rows and each row has Lewis weight at most $u$.
\item The Lewis weight $W'$ of $A'$ satisfies $A'^{\top} \ov{W'}^{1-2/p} A' \succeq A^{\top} \ov{W}^{1-2/p} A$.
\item $\|A' x\|_1 = O(\|A x\|_1)$ for all $x$.
\end{enumerate}
Let $A''$ be the union of $SA$ and $A'$ (so the $j$th row of $A''$ is $s_j \cdot a_j$ for $j \le [n]$ and $A'[j,*]$ for $j \ge n+1$). Because the first $n$ entries of $A'' \cdot \beta$ are the same as $SA\beta$ for any $\beta$ and the rest entries will only increase the energy, we have
\[
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, S A \beta \rangle \bigg| \right] \le \E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, A'' \cdot \beta \rangle \bigg| \right].
\]

To apply Lemma~\ref{lem:Gaussian_proc_bounded_Lewis}, let us verify the Lewis weight of $A''$ is bounded. First of all, $A''^{\top} \overline{W}''^{1-2/p} A'' \succeq A^{\top} \overline{W}^{1-2/p} A$ from Lemma 5.6 in \cite{CP15_Lewis}. Now for each row $s_j a_j$ in $A''$, its Lewis weight is upper bounded by
\[
\left( \frac{1}{p_j} a_j^{\top} (A^{\top} \overline{W}^{1-2/p} A)^{-1} \cdot \frac{1}{p_j} a_j \right)^{1/2} \le w_j/p_j \le u
\]
by the assumption $p_j \ge w_j/u$ in the lemma. At the same time, the Lewis weight of $A'$ is already bounded by $u$ from the definition, which indicates the rows in $A''$ added from $A'$ are also bounded by $u$. So Lemma~\ref{lem:Gaussian_proc_bounded_Lewis} implies 
\begin{align*}
\E_g \left[ \sup_{\beta \in B_{\beta}} \bigg| \langle g, A'' \beta \rangle \bigg| \right] & \le \sqrt{u \log (m+d/u)} \cdot \sup_{\beta \in B_{\beta}} \|A'' \beta\|_1 \\
& \le \sqrt{u \cdot \log (m+d/u)} \cdot \left( \sup_{\beta \in B_{\beta}} \|S A \beta\|_1 + O(\sup_{\beta \in B_{\beta}} \|A \beta\|_1) \right)
\end{align*}
by the definition of $A''$ and the last property of $A'$. Finally, we bring the expectation of $S$ back to bound $\E_S[\sup_{\beta \in B_{\beta}} \|S A \beta\|_1] \le (1+\eps)\sup_{\beta \in B_{\beta}} \|A \beta\|_1$ by Lemma 7.4 in \cite{CP15_Lewis} about the $\ell_1$ subspace embedding.

From all discussion above, the deviation $\E_g \left[ \sup_{\beta \in B_{\beta}} | \langle g, A'' \beta \rangle | \right]$ is upper bounded by $O(\eps) \cdot \sup_{\beta \in B_{\beta}} \|A \beta\|_1$ given our choice of $u$.
\end{proofof} 

\subsection{Proof of Lemma~\ref{lem:high_prob_guarantee}}\label{sec:proof_cor_high_prob}
The proof follows the same outline of the proof of Lemma~\ref{lem:concentration_deviation} except using Corollary~\ref{cor:comparison_higher} to replace Theorem~\ref{thm:Slepian_Fernique} with a higher moment. We choose the moment $\ell=O(\log n/\delta)$ and plan to bound 
$
\E_{S}\left[ \sup_{\beta \in B_{\beta}} \big|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \big|^{\ell} \right]
$ for a better dependence on the failure probability. We apply symmetrization and Gaussianization again with different constants:
\begin{align*}
& \E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \bigg|^{\ell} \right] \\
= & \E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \E_{S'} \sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|^{\ell} \right]\\
& (\notag{\text{Use the convexity of $|\cdot|^{\ell}$ to move $\E_{S'}$ out}})\\
\le & \E_{S,S'}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|^{\ell} \right]\\
& (\notag{\text{Use the symmetry of $S$ and $S'$}})\\
\le & \E_{S,S',\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot (s_j-s'_j) \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right]\\ 
& (\notag{\text{Split $S$ and $S'$}})\\
\le & \E_{S,S',\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{j=1}^n \epsilon_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg|^{\ell} \right]\\
& (\notag{\text{Pay an extra $2^{\ell}$ factor to bound the cross terms}})\\
\le & 2^{\ell} \cdot \E_{S,\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right] + 2^{\ell} \cdot \E_{S',\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right]\\
\le & 2^{\ell+1} \cdot \E_{S,\eps \in \{\pm 1\}^n}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \epsilon_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right]\\
& (\notag{\text{Gaussianize it}})\\
\le & 2^{\ell+1} \cdot \sqrt{\pi/2}^{\ell} \cdot \E_{S,g}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg|^{\ell} \right].
\end{align*}
Then we replace the vectors $\big(s_j \cdot (\delta_j(\beta)-|y_j|)\big)_j$ by $SA\beta$ using the Gaussian comparison Corollary~\ref{cor:comparison_higher} (we omit the verification here because it is the same as the verification in the proof of Lemma~\ref{lem:concentration_deviation}):
\[
C_1^{\ell} \cdot \E_S \E_g \left[ \sup_{\beta}  \bigg| \langle g, SA\beta \rangle \bigg|^{\ell}\right].
\]
The proofs of Lemma~8.4 and Theorem 2.3 in Section 8 of \cite{CP15_Lewis} show that
\[
\E_S \E_g \left[ \sup_{\beta}  \bigg| \langle g, SA\beta \rangle \bigg|^{\ell}\right] \le C_2^{\ell} \cdot \eps^{\ell} \cdot \delta \cdot 
\sup_{\beta \in B_{\beta}} \|A \beta\|_1^{\ell}.
\]
From all discussion above, we have
\[ \E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \bigg|^{\ell} \right] \le (C_1 C_2)^{\ell} \cdot \eps^{\ell} \delta \cdot \sup_{\beta \in B_{\beta}} \|A \beta\|_1^{\ell}.\]

This implies with probability $1-\delta$, the R.H.S. is at most $(C_1 C_2)^{\ell} \cdot \eps^{\ell} \cdot \sup_{\beta \in B_{\beta}} \|A \beta\|_1^{\ell}$. So $\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) = \sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) \pm (C_1 C_2) \cdot \eps \sup_{\beta \in B_{\beta}} \|A \beta\|_1$ for all $\beta \in B_{\beta}$. Finally, we finish the proof by rescaling $\eps$ by a factor of $C_1 \cdot C_2$.


\subsection{Proof of $\ell_1$ Concentration}\label{sec:ell_1_concentration}
We finish the proof of Lemma~\ref{lem:Gaussian_proc_bounded_Lewis} in this section.

\begin{proofof}{Lemma~\ref{lem:Gaussian_proc_bounded_Lewis}}
%Observe that for each coordinate $i$, 
%\[\left| (\delta_i(\beta)-|y_i|) - (\delta_i(\beta')-|y_i|) \right| \le |a_i^{\top} \beta - a_i^{\top} \beta'|.
%\] So we apply Theorem~\ref{thm:Slepian_Fernique} to the Guassian process of $\big(\delta_i(\beta) - |y_i|\big)_{i \in [n]}$ and $A \beta = \big( a_i^{\top} \beta \big)_{i \in [n]}$:
%\[\E_g\left[\max_{\beta \in B_{\beta}}  \bigg| \sum_i g_i \big( \delta_i(\beta)-|y_i| \big) \bigg| \right] \le C_0 \cdot \E_g \left[ \max_{\beta \in B_{\beta}} \bigg| \langle g, A \beta \rangle \bigg| \right].
%\]

%\textcolor{blue}{Xue: The rest follows from Section 8 of \cite{CP15_Lewis}.} 

%Then we consider the new Gaussian process. 

We consider the natural inner product induced by the Lewis weights $(w_1,\ldots,w_n)$ and define the weighted projection onto the column space of $A$ with $W=\diag(w_1,\ldots,w_n)$ as $\Pi := A \cdot (A^\top W^{-1} A)^{-1} \cdot A^\top W^{-1}$. It is straightforward to verify $\Pi A= A$. Thus we write $\langle g, A \beta \rangle$ as $\langle g, \Pi A \beta \rangle$. In the rest of this proof, let $\Pi_1,\ldots,\Pi_n$ denote its $n$ columns.

Then we upper bound the inner product in the Gaussian process by $\|\cdot\|_1 \cdot \|\cdot\|_{\infty}$:
\[
\E_g \left[ \max_{\beta \in S} \big| \langle \Pi^{\top} g, A \beta \rangle \big| \right] \le \E_g \left[ \max_{\beta \in S} \{ \|\Pi^{\top} g\|_{\infty} \cdot \|A \beta\|_1 \} \right].
\]
%Since $\beta \in B_{\beta}$ always has $\sum_i |a_i^{\top} \beta - y_i| \le B \sum_i |y_i|$, this gives $\|A \beta\|_1=\sum_i |a_i^{\top} \beta|$ is upper bounded $(B+1) \sum_i |y_i|$ by the triangle inequality.

So we further simplify the Gaussian process as
\[
\E_g \left[ \|\Pi^{\top} g\|_{\infty} \right] \cdot \max_{\beta \in S} \|A \beta\|_1 \le \sqrt{2 \log n} \cdot \max_{i \in [n]} \|\Pi_i\|_2 \cdot \max_{\beta \in S} \|A \beta\|_1,
%\E_g \left[ \max_{\beta \in B_{\beta}} \|\Pi g\|_{\infty} \right] \cdot (B+1) \sum_i |y_i| \le \sqrt{2 \log n} \cdot \max_{i \in [n]} \|\Pi_i\|_2 \cdot (B+1) \sum_i |y_i|,
\]
where we observe the entry $i$ of $\Pi^{\top} g$ is a Gaussian variable with variance $\|\Pi_i\|^2_2$ and apply a union bound over $n$ Gaussian variables. In the rest of this proof, we bound $\|\Pi_i\|^2_2$. %Since $\Pi$ is a projection matrix, this is also equal to $\sum_j |\Pi_{j,i}|^2$:
\begin{align*}
& \sum_j (A \cdot (A^\top W^{-1} A)^{-1} \cdot A^\top W^{-1})_{j,i}^2 \\
= & \sum_j (w_i^{-1} \cdot a_i^{\top} \cdot (A^\top W^{-1} A)^{-1} \cdot a_j)^2\\
\le & u \sum_j (w_i^{-1} \cdot a_i^{\top} \cdot (A^\top W^{-1} A)^{-1} \cdot a_j w_j^{-1/2})^2 \tag{$w_j \le u$ from the assumption}\\
\le & u w_i^{-2} \cdot \sum_j a_i^{\top} \cdot (A^\top W^{-1} A)^{-1} \cdot a_j w_j^{-1} a_j^{\top} \cdot (A^\top W^{-1} A)^{-1} \cdot a_i\\
\le & u w_i^{-2} \cdot a_i^{\top} (A^\top W^{-1} A)^{-1} \cdot \left( \sum_j a_j w_j^{-1} a_j^{\top} \right) \cdot (A^\top W^{-1} A)^{-1} \cdot a_i \\
\le & u w_i^{-2} a_i^{\top} (A^\top W^{-1} A)^{-1} \cdot a_i \tag{by the definition of Lewis weights}\\
\le & u w_i^{-2} \cdot w_i^2 = u.
\end{align*}
\end{proofof}

\subsection{Symmetrization and Gaussianization}\label{sec:sym_gau}
We start with a standard symmetrization by replacing $\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big)]$ by its expectation $\E_{S'}\bigg[\sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg]$:
\[
\E_{S}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \E_{S'}\bigg[\sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big)\bigg] \bigg| \right].
\]
Using the covexity of the absolute function, we move out the expectation over $S'$ and upper bound this by
\begin{align*}
& \E_{S,S'}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) - \sum_{j=1}^n s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right] \\
= & \E_{S,S'}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right].
\end{align*}
Because $S$ and $S'$ are symmetric and each coordinate is independent, this expectation is equivalent to
\begin{align*}
& \E_{S,S',\sigma}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) - \sigma_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right] \\
\le & \E_{S,S',\sigma}\left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| + \bigg|\sum_{j=1}^n  \sigma_j \cdot s'_j \cdot \big( \delta_{j}(\beta) - |y_{j}| \big) \bigg| \right]\\
\le & 2 \E_{S,\sigma} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\end{align*}
Then we apply Gaussianization: Since $\E[|g_j|]=\sqrt{2/\pi}$, the expectation is upper bounded by
\[
\sqrt{2 \pi} \E_{S,\sigma} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n \E[|g_j|] \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\]
Using the convexity of the absolute function again, we move out the expectation over $g_j$ and upper bound this by
\[
\sqrt{2 \pi} \E_{S,\sigma,g} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n |g_j| \sigma_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\]
Now $|g_j| \sigma_j$ is a standard Gaussian random variable, so we simplify it to
\[
\sqrt{2 \pi} \E_{S,g} \left[ \sup_{\beta \in B_{\beta}} \bigg|\sum_{j=1}^n g_j \cdot s_j \cdot \big( \delta_{j}(\beta) - |y_{j}|\big) \bigg| \right].
\]