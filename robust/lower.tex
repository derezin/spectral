%!TEX root = robust-colt21.tex
\section{Lower Bound for $\ell_1$ Regression}\label{sec:lower_bound}
In this section, we prove the information-theoretic lower bound in Theorem~\ref{t:ell1} for the query complexity of $\ell_1$ regression.
\begin{theorem}\label{thm:information_lower_bound}
  Given any $\epsilon$, $\delta<0.01$, and $d$, consider the $n \times d$ matrix $A$ defined as follows: \[A^{\top}=[\underbrace{e_1^{\top},\ldots,e_1^{\top}}_{n/d},e_2,\ldots,e_{d-1},\underbrace{e_d^{\top},\ldots,e_d^{\top}}_{n/d}]\]
  for the canonical basis $e_1,\ldots,e_d \in \mathbb{R}^d$ with a sufficiently large $n$. It takes $m=\Omega(\frac{d + \log 1/\delta}{\eps^2})$ queries to $y$ to produce $\tilde{\beta}$ satisfying $\|A \tilde{\beta}- y\|_1 \le (1+\epsilon) \cdot \|A \beta^* - y\|_1$ with probability $1-\delta$.
\end{theorem}
%We defer the proof of Lemma~\ref{lem:information_lower_dim_1} to Section~\ref{sec:lower_bound_dim_1} and finish the proof of Theorem~\ref{thm:information_lower_bound}.
Our proof will make a reduction to the classical biased coin testing problem \citep{Kleinberg}, by relying on Yao's minmax principle so that we can consider a deterministic querying algorithm and a randomized label vector $y$.

We rely on the fact that the $\ell_1$ regression problem $\|A \beta - y\|_1$ for $A$ specified in Theorem~\ref{thm:information_lower_bound} can be separated into $d$ independent subproblems of dimension $n' \times 1$ for $n':=n/d$. First of all, let us consider the 1-dimensional subproblem, which is a reformulation of the biased coin test. In this subproblem, we will use the following two distributions with different biases to generate $y' \in \{\pm 1\}^{n'}$:
\begin{enumerate}
\item $D_1:$ Each label $y'_i=1$ with probability $1/2+\epsilon$ and $-1$ with probability $1/2-\epsilon$.
\item $D_{-1}:$ Each label $y'_i=1$ with probability $1/2-\epsilon$ and $-1$ with probability $1/2+\epsilon$.
\end{enumerate}
The starting point of our reduction is that a $(1+\eps)$-approximate solution of $\beta^*$ could distinguish between the two biased coins of $D_1$ and $D_{-1}$.
\begin{claim}\label{clm:distinguish_two_dist}
  Let $d=1$, $n' \ge \frac{100 \log 1/\delta}{\epsilon^2}$, and $A'=[1,\ldots,1]^{\top}$ whose dimension is $n' \times 1$. Let $L(\beta)$ for $\beta \in \mathbb{R}$ denote $\|A' \beta - y'\|_1$ for $y' \in \mathbb{R}^{n'}$ and $\beta^*$ be the minimizer of $L(\beta)$.

  If $y'$ is generated from $D_1$, then with probability $1-\frac{\delta}{100}$, we will have $\beta^*=1$ and any $(1+\eps)$-approximation $\tilde{\beta}$ will be positive. Similarly, if $y' \sim D_{-1}$, then with probability $1-\frac{\delta}{100}$  we will have $\beta^*=-1$ and any $(1+\eps)$-approximation $\tilde{\beta}$ will be negative.
\end{claim}
\begin{proof}
Let us consider $y'$ generated from $D_1$ of dimension $n' \ge \frac{100 \log 1/\delta}{\epsilon^2}$. Let $n_1$ and $n_{-1}$ denote the number of $1$s and $-1$s in $y'$. From the standard concentration bound, with probability $1-\frac{\delta}{100}$, we have $n_1 \ge (1/2+\epsilon/2)n'$ for a random string $y'$ from $D_1$. So for this 1-dimensional problem, $\beta^*$ minimizing $\|A' \beta-y'\|_1$ will equal $1$ with loss $L(\beta^*):=\|A' \beta^* - y'\|_1 = 2 \cdot n_{-1} \le (1-\epsilon)n$. Let $\tilde{\beta}$ be any $(1+\eps)$-approximation s.t. $\|A' \tilde{\beta}- y'\|_1 \le (1+\epsilon) \cdot \|A' \beta^* - y\|_1$. 

We show that $\tilde{\beta}$ must be positive given $n_1 \ge (1/2+\epsilon/2)n$. If $\tilde{\beta} \in (-1,0]$, then the loss of $\tilde{\beta}$ is given by
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (1+\tilde{\beta}) = n - \tilde{\beta} (n_1 - n_{-1}) \ge n\geq \frac{L(\beta^*)}{1-\epsilon}.
\]
Otherwise if $\tilde{\beta} \le -1$, the $\ell_1$ loss of $\tilde{\beta}$ becomes
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (-\tilde{\beta} - 1)=-\tilde{\beta} \cdot n + n_1 - n_{-1} \ge n\geq \frac{L(\beta^*)}{1-\epsilon}.
\]
In both cases, $L(\tilde{\beta})$ does not give a $(1+\epsilon)$-approximation of $L(\beta^*)$. Similarly when $y$ is generated from $D_{-1}$ and $n_{-1} \ge (1/2+\epsilon/2)n$, then any $\tilde{\beta}$ that is a $(1+\eps)$-approximation of $\beta^*$, must be negative.
\end{proof}

Given the connection between the 1-dimensional subproblem and the biased coin test in Claim~\ref{clm:distinguish_two_dist}, the $\Omega(\frac{\log 1/\delta}{\eps^2})$ lower bound in the main theorem comes from the classical lower bound for distinguishing between biased coins.
\begin{lemma}\label{lem:information_lower_dim_1}
Let us consider the following game between Alice and Bob:
\begin{enumerate}
\item Let Alice generate $\alpha \sim \{\pm 1\}$ randomly.
\item Alice generates $y \in \{\pm 1\}^{n'}$ from $D_\alpha$ defined above and allows Bob to query $m'$ entries in $y$.
\item After all queries, Bob wins the game only if he predicts $\alpha$ correctly.
\end{enumerate}
If Bob wants to win this game with probability $1-\delta$, he needs to make $m'=\Omega(\frac{\log 1/\delta}{\eps^2})$ queries.
\end{lemma}
The proof of Lemma~\ref{lem:information_lower_dim_1} follows from KL divergence or the variation distance (a.k.a. statistical distance). We refer to Theorem 1.5 in \cite{Kleinberg} for a proof with minor modifications such as replacing the fair coin by one of bias $1/2 - \eps$.

Next, we discuss how to obtain the $\Omega(d/\eps^2)$ part. Due to the space constraint, we give a high-level overview and defer the formal proof of Theorem~\ref{thm:information_lower_bound} to Appendix~\ref{app:proof_lower}. First of all, for each string in $b \in \{\pm 1\}^d$, we consider the $n$-dimensional distribution of $y$ generated as $D_b=D_{b_1} \circ D_{b_2} \circ \cdots \circ D_{b_d}$. Namely, each chunk of $n/d$ bits of $y$ is generated from $D_1$ or $D_{-1}$ mentioned above. 

The second observation is that when we generate $y \sim D_b$ for a random $b \in \{\pm 1\}^n$, then a $(1+\eps)$-approximation algorithm could decode almost all entries of $b$ except a tiny fraction (w.h.p. based on Claim~\ref{clm:distinguish_two_dist}). Then we show the existence of $b \in \{\pm 1\}^d$ and its flip $b^{(i)}$ on the $i$th coordinate such that: (1) the algorithm could decode entry $i$ in $b$ and $b^{(i)}$ (w.h.p. separately) when $y \sim D_b$ or $D_{b^{(i)}}$; (2) the algorithm makes $O(m/d)$ queries to decode entry $b_i$. Such a pair of $b$ and $b^{(i)}$ provides a good strategy for Bob to win the game in Lemma~\ref{lem:information_lower_dim_1} with a constant high probability using $O(m/d)$ queries, which gives a lower bound of $m$. 

\iffalse
We will use a packing set of $b$ in $\{\pm 1\}^d$ in this proof.
\begin{claim}
There exists $\mathcal{F} \subset \{1,2\}^d$  such that
\begin{enumerate}
\item $|\mathcal{F}| \ge 2^{\Omega(d)}$.
\item For each $b \in \mathcal{F}$, $\overline{b} \in \mathcal{F}$ where $\overline{b}$ denotes the flip operation on every coordinate of $b$ from 1 to 2.
\item For two distinct $b$ and $b'$ in $\mathcal{F}$, the Hamming distance between $b$ and $b'$ is at least $0.1 d$.
\end{enumerate}
\end{claim}
\begin{proof}
We can construct $\mathcal{F}$ by the following procedure:
\begin{enumerate}
\item We start with $S=\{\pm 1\}^d$.
\item While $S \neq \emptyset$: 
\begin{enumerate}
\item Add any $b \in S$ and its flip $\ov{b}$ to $S$.
\item Remove all $b' \in S$ with Hamming distance $<0.3d$ to $b$ or $\ov{b}$ in $S$.
\end{enumerate} 
\end{enumerate}
Because we will remove at most $2 \cdot 2^{0.5d}$ strings in $S$ every round, $|\mathcal{F}| = 2^{\Omega(n)}$.
\end{proof}


From all discussion above, we know that with probability $0.79$ over $D$, the algorithm $P$ outputs $\tilde{\beta}$, which could deduce the string $b$ generating $y$. By Fano's inequality \cite{Fano}, this implies the conditional entropy $H(b|\tilde{\beta}) \le H(0.21)+0.21 \cdot \log (|\mathcal{F}|-1)$. So the mutual information of $\tilde{\beta}$ and $b$ is
\[
I(b;\tilde{\beta})=H(b) - H(b|\tilde{\beta})\ge 0.5 \log |\mathcal{F}|=Omega(d).
\]
At the same time, by the data processing inequality, the algorithm $P$ makes $m$ queries and sees $y_{i_1},\ldots,y_{i_m}$, which indicates
\[
I(\tilde{\beta};b)\le I\left((y_{i_1},\ldots,y_{i_m});b\right)=\sum_{}
\]



We finish the proof of Lemma~\ref{lem:information_lower_dim_1} in this section. For contradiction, suppose there is an algorithm $P$ that achieves $(1+\eps)$-approximation with $m=0.01 \cdot \frac{1}{\epsilon^2}$ queries. Now we consider the following two distributions of $y$:
\begin{enumerate}
\item $D_1:$ Each label $y_i=1$ with probability $1/2+\epsilon$ and $-1$ with probability $1/2-\epsilon$.
\item $D_2:$ Each label $y_i=1$ with probability $1/2-\epsilon$ and $-1$ with probability $1/2+\epsilon$.
\end{enumerate}
Let us consider a label string $y$ generated from $D_1$ of dimension $n>10^3/\epsilon^2$. Let $n_1$ and $n_{-1}$ denote the number of $1$s and $-1$s in $y$. From the standard concentration bound, with probability $0.99$, we have $n_1 \ge (1/2+\epsilon/2)n$ for a random string $y$ from $D_1$. So $\beta^*$ minimizing $\|A \beta-y\|_1$ will equal $1$ with $\ell_1$ loss $L(\beta^*)=2 \cdot n_{-1} \le (1-\epsilon)n$. Now let $\tilde{\beta}$ be the output of $P$ satisfying $\|A \tilde{\beta}- y\|_1 \le (1+\epsilon) \cdot \|A \beta^* - y\|_1$. 

We claim $\tilde{\beta}$ must be positive when $n_1 \ge (1/2+\epsilon/2)n$. If $\tilde{\beta} \in [0,-1)$, the $\ell_1$ loss of $\tilde{\beta}$ is
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (1+\tilde{\beta}) = n - \tilde{\beta} (n_1 - n_{-1}) \ge n.
\]
Otherwise if $\tilde{\beta} \le -1$, the $\ell_1$ loss of $\tilde{\beta}$ becomes
\[
n_1 \cdot (1-\tilde{\beta}) + n_{-1} \cdot (-\tilde{\beta} - 1)=-\tilde{\beta} \cdot n + n_1 - n_{-1} \ge n.
\]
In both cases, $L(\tilde{\beta})$ does not give an $(1+\epsilon)$-approximation of $L(\beta^*)$. Similarly when $y$ is generated from $D_2$ and $n_{-1} \ge (1/2+\epsilon/2)n$, $\tilde{\beta}$, which is an $(1+\eps)$-approximation of $\beta^*$, must be negative.

Now let us consider the following game between Alice and Bob:
\begin{enumerate}
\item Let Alice generate $b \sim \{1,2\}$ randomly.
\item Then Alice generates $y$ from $D_b$ and allows Bob to query $m$ entries in $y$.
\item After all queries, Bob wins the game only if he predicts $b$ correctly.
\end{enumerate}

\begin{claim}\label{clm:win_prob}
Let $P$ be an $(1+\eps)$-approximation algorithm with at most $m$ queries and success probability 0.9. Then Bob has a strategy to win the above game with probability at least $0.85$ using $m$ queries.
\end{claim}
\begin{proof}
Bob's strategy is to run the linear regression algorithm $P$ with $A=[1,\ldots,1]^{\top}$ and claim $b=1$ when $\tilde{\beta}>0$ (otherwise claim $b=2$). From the above discussion, when $b=1$, with probability 0.99, $n_1 \ge (1/2+\epsilon)n$ such that any $(1+\eps)$ approximation shall have $\tilde{\beta}>0$. Similarly, when $b=2$, with probability 0.99, $n_{-1} \ge (1/2+\epsilon)n$ such that any $(1+\eps)$ approximation shall have $\tilde{\beta}<0$.

This indicates that Bob wins the game when (1) $P$ gives an $(1+\eps)$ approximation and (2) $n_1 \ge (1/2+\epsilon)n$ for $b=1$ or $n_{-1} \ge (1/2+\epsilon)n$ for $b=2$. So Bob wins the game with probability at least $0.85$ by a union bound.
\end{proof}
\fi

\iffalse

\textcolor{blue}{In my understanding, the right goal would be to show $m=\Omega(1/\eps^2)$ for $\|A \tilde{\beta}-y\|_1 \le (1+\eps) \cdot \|A \beta^* - y\|_1$. But I donot how to do it at this moment.}

First we argue that $\tilde{\beta}$ satisfying $\|A \tilde{\beta}-A \beta^*\|_1 \le \eps \cdot \|A \beta^* - y\|_1$ must have $\sign(\tilde{\beta})=\sign(\beta_0)$ for the $\beta_0$ generating $y$. When $n>>1/\epsilon^2$, we have $|\beta_0-\beta^*| \le O(\frac{1/\eps}{\sqrt{n}})$ because $\beta^*=median(y_1,\ldots,y_n)$. At the same time, we know $\|A \beta_0 - y\|_1 = \sqrt{\pi/2} \cdot 1/\eps \cdot n + o(n)$. This further implies $\|A \beta^* - y\|_1 \ge \|A \beta_0 - y\|_1 - \|A \beta^* - A \beta_0\|_1 = \sqrt{\pi/2} \cdot 1/\eps \cdot n - o(n)$. If $\|A \tilde{\beta}-A \beta^*\|_1 \le \eps/2 \cdot \|A \beta^* - y\|_1$, we have $|\tilde{\beta}-\beta^*| \le \sqrt{\pi/2}/2 - o(1)$. 
%If $\beta^*=1$ and $\tilde{\beta}<1/2$, we have $\|A \tilde{\beta} - y\|_1 \ge \|A \beta^* - y\|_1 + \frac{1}{2} \cdot (|\{g_i \ge 0\}|-|\{g_i < - 1/4\}|) \ge \sqrt{2/\pi} \cdot 1/\eps \cdot n + \eps n/10$.

The above discussion implies that we can figure out $\beta_0$ given $\tilde{\beta}$. In the language of information theory, this means we could obtain constant information (like 1-bit) about $\beta_0 \sim \{\pm 1\}$. On the other hand, each query of $y_i$ only has information $O(\eps^2)$ by the Shannon-Hartley theorem. These two together imply that $\Omega(1/\eps^2)$ query is necessary.

\begin{theorem}[The Shannon-Hartley Theorem \cite{Hartley,Shannon49}]\label{thm:Shannon_Hartley}
Let $S$ be a real-valued random variable with $\E[S^2]=\tau^2$ and $T \sim N(0,\sigma^2)$. The mutual information $I(S;S+T)\le \frac{1}{2} \log (1+\frac{\tau^2}{\sigma^2})$.
\end{theorem}

\fi