%!TEX root = robust-colt21.tex
\section{Lewis Weights Bound Importance Weights}\label{sec:Lewis_weight_importance}
We finish the proof of Theorem~\ref{thm:Lewis_weight_importance} in this section. Specifically, given any matrix $A$, we show that Lewis weights $(w_1,\ldots,w_n)$ defined in Equation~\eqref{eq:def_Lewis_weight} bound the importance weights for each row:
\[
\underset{x \in \mathbb{R}^{d}}{\max} \frac{|a_i^{\top} x|^p}{\|A x\|_p^p} \in \left[ d^{-(1-p/2)} \cdot w_i, w_i \right].
\]

\begin{remark}
There are matrices that obtain both upper and lower bounds in the above inequality (up to constants). For example, $A=I$ gives the upper bound.

For the lower bound, consider $A \in \{\pm 1\}^{2^d \times d}$ constituted by $n=2^d$ distinct vectors in $\{\pm 1/\sqrt{d}\}^d$. Then for each $a_i$, we set $x=a_i$ such that $a_i^{\top} x=1$ but $w_i=d/n$ and $\|Ax\|_p^p \approx n \cdot \underset{Z \sim N(0,1/d)}{\E}[|Z|^p] = \Theta(n \cdot d^{-p/2})$.
%For example, when $p=1$, 
%\[A = \begin{bmatrix}
%1/\sqrt{2} & 0 \\0 & 1/\sqrt{2} \\1/2 & 1/2 \\1/2 & -1/2\end{bmatrix}\]
%has importance weights $(\frac{1}{1+\sqrt{2}},\ldots,\frac{1}{1+\sqrt{2}})$ and Lewis weights $(1/2,1/2,1/2,1/2)$.
\end{remark}

To prove Theorem~\ref{thm:Lewis_weight_importance}, we will show the following lemma for the special case where the Lewis weights are almost uniform and make a reduction from the general case to the almost uniform case. Recall that $a \approx_{\alpha} b$ means $a \in [b/\alpha,b \cdot \alpha]$ and $W=\diag(w_1,\ldots,w_n)$ denotes the diagonal Lewis-weight matrix when the matrix $A$ and parameter $p$ are fixed.
\begin{lemma}\label{lem:uniform_lewis_bounds_importance}
Suppose the $\ell_p$ Lewis weights of $A$ are almost uniform: for a parameter $\alpha \ge 1$, $w_i \approx_{\alpha} d/n$. Then for every row $i$, its importance weight satisfies:
\[
\underset{x \in \mathbb{R}^{d}}{\max} \frac{|a_i^{\top} x|^p}{\|A x\|^p_p} \in \left[ \alpha^{-O(1)} \cdot d^{p/2}/n, \alpha^{O(1)} \cdot d/n \right].
\]
\end{lemma}
In particular, if $\alpha=1$ (the Lewis weights are uniform), this indicates the importance weights are uniformly upper bounded. We defer the proof to Appendix~\ref{sec:almost_uniform_Lewis}. We remark that Claim~\ref{clm:non_uniform_Lewis_w} in Appendix~\ref{sec:almost_uniform_Lewis} shows that when the statistical leverage scores are almost uniform --- $a_i^{\top} (A^{\top} A)^{-1} a_i \approx_{\alpha} d/n$ for some $\alpha$, the $\ell_p$ Lewis weights have $w_i \approx_{\alpha^{O_p(1)}} d/n$ for all $i \in [n]$ and any $p \in [1,2]$. This bounds the sample complexity of uniform sampling in terms of $\alpha$ by plugging $\gamma=\alpha^{O(1)}$ in Theorem~\ref{thm:concentration_contraction} and Theorem~\ref{thm:property_ellp}.

While this could give a bound on the importance weights in terms of the Lewis weights for the non-uniform case, we will use the following properties to obtain the tight bound in Theorem~\ref{thm:Lewis_weight_importance} via a reduction.
\begin{claim}\label{clm:split_Lewis_weight}
Given $A \in \mathbb{R}^{n \times d}$ whose Lewis weights are $(w_1,\ldots,w_n)$, let $A' \in \mathbb{R}^{(n+k-1) \times d}$ be the matrix of splitting one row, say the last row $a_n$, into $k$ copies: $a'_i=a_i$ for $i<n$ and $a'_i=a_n/k^{1/p}$ for $i \ge n$. Then the Lewis weights of $A'$ are $(w_1,\ldots,w_{n-1},w_n/k,\ldots,w_n/k)$.
\end{claim}

And the same property holds for the importance weight.
\begin{claim}\label{clm:split_importance}
Given $A \in \mathbb{R}^{n \times d}$ whose importance weights are $(u_1,\ldots,u_n)$, let $A' \in \mathbb{R}^{(n+k-1) \times d}$ be the matrix of splitting the last row $a_n$ into $k$ copies: $a'_i=a_i$ for $i<n$ and $a'_i=a_n/k^{1/p}$ for $i \ge n$. Then the importance weights of $A'$ are $(u_1,\ldots,u_{n-1},u_n/k,\ldots,u_n/k)$.
\end{claim}

We defer the proofs of these two claims to Section~\ref{sec:proofs_claims_splitting} and finish the proof of Theorem~\ref{thm:Lewis_weight_importance}.

\begin{proofof}{Theorem~\ref{thm:Lewis_weight_importance}}
Suppose the Lewis weights of $A$ are $(w_1,\ldots,w_n)$ and the importance weights are $(u_1,\ldots,u_n)$. Let $\eps$ be a tiny constant and $N_1=\lceil w_1/\eps \rceil,\ldots,N_n=\lceil w_n/\eps \rceil$. We define $A' \in \mathbb{R}^{N \times d}$ with $N=\sum_i N_i$ as
\[
\begin{bmatrix} 
a_1/N_1^{1/p} \\
\vdots \\
a_1/N_1^{1/p} \\
a_2/N_2^{1/p} \\
\vdots \\
a_n/N_n^{1/p}
\end{bmatrix}
\]
where there are $N_1$ rows of $a_1/N_1^{1/p}$, $N_2$ rows of $a_2/N_2^{1/p}$, and so on. From Claim~\ref{clm:split_Lewis_weight}, we have the Lewis weights of $A'$ are
\[
\left( \underbrace{w_1/N_1,\ldots,w_1/N_1}_{N_1},\underbrace{w_2/N_2,\ldots,w_2/N_2}_{N_2},\ldots,\underbrace{w_n/N_n,\ldots,w_n/N_n}_{N_n} \right).
\]
Since $w_1,\ldots,w_n$ are fixed, we know $w_i/N_i=w_i/\lceil w_i/\eps \rceil \in [\eps/(1+\eps/w_1),\eps]$. So let $\alpha$ be the parameter satisfying $w_i/N_i \approx_{\alpha} d/N$. If $w_1,\ldots,w_n$ are multiples of $\eps$, $d/N=\epsilon$ and $\alpha=1$. Since $\lim_{\eps \rightarrow 0} d/N=\eps$ (by the property $\sum_i w_i=d$ and the definition of $N$), we know $\lim_{\eps \rightarrow 0} \alpha=1$.

From Claim~\ref{clm:split_importance}, we have the importance weights of $A'$ are
\[
\left( \underbrace{u_1/N_1,\ldots,u_1/N_1}_{N_1},\underbrace{u_2/N_2,\ldots,u_2/N_2}_{N_2},\ldots,\underbrace{u_n/N_n,\ldots,u_n/N_n}_{N_n} \right).
\]

By Lemma~\ref{lem:uniform_lewis_bounds_importance}, we have $w_i/N_1 \in [\alpha^{-O(1)} \cdot d^{-(1-p/2)} \cdot u_1/N_1  , \alpha^{O(1)} \cdot u_1/N_1]$ for each $i$. By taking $\eps \rightarrow 0$ and $\alpha \rightarrow 1$, this shows both upper and lower bounds.
\end{proofof}

\subsection{Proofs of Claim~\ref{clm:split_Lewis_weight} and~\ref{clm:split_importance}}\label{sec:proofs_claims_splitting}
To prove Claim~\ref{clm:split_Lewis_weight}, we use the property that Lewis weights constitute the unique diagonal matrix satisfying \eqref{eq:def_Lewis_weight} \citep{CP15_Lewis}. So we only need to verify $(w_1,\ldots,w_{n-1},w_n/k,\ldots,w_n/k)$ satisfying \eqref{eq:def_Lewis_weight} for $A'$. First of all, we show the inverse in the equation is the same:
\begin{align*}
A'^{\top} (W')^{1-2/p} A' &=\sum_{i=1}^{n+k-1} (w'_i)^{1-2/p} \cdot (a'_i) \cdot (a'_i)^{\top} \\
& =\sum_{i=1}^{n-1} (w_i)^{1-2/p} a_i \cdot a_i^{\top} + \sum_{i=1}^k (w_n/k)^{1-2/p} \cdot (a_n/k^{1/p})\cdot (a_n/k^{1/p})^{\top} \\
& = \sum_{i=1}^{n-1} (w_i)^{1-2/p} a_i \cdot a_i^{\top} + k^{2/p} \cdot w_n^{1-2/p} \cdot (a_n/k^{1/p})\cdot (a_n/k^{1/p})^{\top} = \sum_{i=1}^{n} (w_i)^{1-2/p} a_i \cdot a_i^{\top}.
\end{align*}
Then it is straightforward to verify $(a_n/k^{1/p})^{\top} (A'^{\top} W'^{-1} A')^{1-2/p} \cdot (a_n/k^{1/p})=(w_n/k)^{2/p}$.

Next We prove Claim~\ref{clm:split_importance}. We note that for any $x$, we always have $\|A x\|^p_p=\|A' x\|^p_p$ and $|(a'_i)^{\top} x|^p=|a_i^{\top} x|^p$ for any $i<n$. These two indicate $u_i=u'_i$ for $i<n$.

Now we prove $u_n=u'_n/k$. For any $x$, we still have $\|A x\|_p^p=\|A' x\|_p^p$ but $|(a'_n)^{\top} x|^p=|a_n^{\top} x|^p/k$. These two indicate $u'_n=u_n/k$.

\subsection{Almost Uniform Lewis Weights}\label{sec:almost_uniform_Lewis}
%More important, the leverage score is equal to the importance weight for $p=2$: $a_i^{\top} (A^{\top} A)^{-1} a_i=\sup_{\beta} \frac{|a_i^{\top} \beta|^2}{\|A \beta\|_2^2}$ from \cite{CP19,spielman2011graph}.

Without loss of generality, we assume $A^{\top} A=I$. We recall the definition of leverage scores that will be used in this proof. Given $p=2$ and $A$ with rows $a_1,\ldots,a_n$, the leverage score of $a_i$ is $a_i^{\top} (A^{\top} A)^{-1} a_i$. Since we assume  $A^{\top} A = I$, this simplifies the score to $\|a_i\|_2^2$. 

Let us start with the case of the uniform Lewis weights for ease of exposition. Because $w_1=w_2=\cdots=w_n=d/n$, we simplify the equation of the Lewis weights
\[
a_i^{\top} \cdot (A^{\top} W^{-1} A)^{1-2/p} a_i = w_i^{2/p}
\]
to $a_i^{\top} \cdot w_i^{2/p-1} \cdot I \cdot a_i=w_i^{2/p}$. This indicates that the leverage score $\|a_i\|^2_2=w_i=d/n$ is also uniform. 

Then we show the upper bound $\frac{|a_i^{\top} x|^p}{\|A x\|_p^p} \le d/n$. By Cauchy-Schwartz,
\[
|a_i^{\top} x| \le \|a_i\|_2 \cdot \|x\|_2 = \sqrt{d/n} \cdot \|x\|_2.
\]
Next we lower bound $\|A x\|_p^p$ by
\begin{equation}\label{eq:p_power_norm}
\|A x\|_p^p \cdot \|A x\|^{2-p}_{\infty} \ge \|A x\|_2^2.
\end{equation}
Because $A^{\top} A = I$, the right hand side is $\|x\|_2^2$. So 
\[
\|A x\|_p^p \ge \frac{\|x\|_2^2}{(\|x\|_2 \cdot \sqrt{d/n})^{2-p}}=\frac{\|x\|^p_2}{(d/n)^{1-p/2}}.
\]
We combine the upper bound and lower bound to obtain
\[
\frac{|a_i^{\top} x|^p}{\|A x\|_p^p} \le \frac{(\sqrt{d/n} \cdot \|x\|_2)^p}{\|x\|^p_2/(d/n)^{1-p/2}}=d/n.
\]
The lower bound $\frac{|a_i^{\top} x|^p}{\|A x\|_p^p} \ge \frac{d^{p/2}}{n}$ follows from choosing $x=a_i$ and replacing \eqref{eq:p_power_norm} by the Holder's inequality $\|Ax\|_p^p \le (n)^{\frac{2-p}{2}} \cdot \|Ax\|_2^{p}$. 

%For convenience, given two variables $A$ and $B$, for a fixed parameter $\alpha \ge 1$, we say $A \approx_\alpha B$ if $A \in [\frac{1}{\alpha}, \alpha] \cdot B$. 
In the non-uniform case, we will use the stability of Lewis weights (Definition 5.1 and 5.2 in \cite{CP15_Lewis}) to finish the proof. Consider any $\ov{W}=diag[\ov{w}_1,\ldots,\ov{w}_n]$ satisfies
\[
\forall i \in [n], a_i^{\top} \left( A^{\top} \ov{W}^{1-2/p} A\right)^{-1} a_i \approx_\alpha \ov{w}_i^{2/p}.
\]
Lemma 5.3 in \cite{CP15_Lewis} shows that for each $i\in[n]$, $\ov{w}_i \approx_{\alpha^{c_p}} w_i$ for the Lewis weights $(w_1,\ldots,w_n)$ where the constant $c_p=\frac{p/2}{1-|p/2-1|}$.

Back to our problem, if the $\ell_p$ Lewis weights of $A$ are almost uniform, we show the uniformity for its $\ell_q$ Lewis weights.

\begin{claim}\label{clm:non_uniform_Lewis_w}
Given $p$ and $q$ less than $4$, let $A \in \mathbb{R}^{n \times d}$ be a matrix whose $\ell_p$ Lewis weight $(w_1,\ldots,w_n)$ satisfies $w_i \approx_{\alpha} d/n$ for each $i \in [n]$. Then the $\ell_q$ Lewis weight of $A$ satisfies $w'_i \approx_{\alpha^C} d/n$ for constant $C=(4/p-1) \cdot c_q$. In particular, when $p=2$ and $q=1$, $C=1$.
\end{claim}
\begin{proof}
Since $w_i \approx_{\alpha} d/n$, $\alpha^{-1} d/n \cdot I \preceq W \preceq \alpha d/n \cdot I$. We plug this sandwich-bound into $A^{\top} W^{1-2/p} A$, which becomes between $(\alpha d/n)^{1-2/p} \cdot A^{\top} A$ and $(\alpha^{-1} d/n)^{1-2/p} \cdot A^{\top} A$. Similarly, we bound its inverse $(A^{\top} W^{1-2/p} A)^{-1}$ by
\[
(\alpha^{-1} d/n)^{2/p - 1} \cdot (A^{\top} A)^{-1} \preceq (A^{\top} W^{1-2/p} A)^{-1} \preceq (\alpha d/n)^{2/p-1} \cdot (A^{\top} A)^{-1}.
\] From the definition of $\ell_p$ Lewis weights $a_i^{\top} \left( A^{\top} W^{1-2/p} A\right)^{-1} a_i=w_i^{2/p}$, we have
\[
(\alpha^{-1} d/n)^{2/p - 1} \cdot a_i^{\top} (A^{\top} A)^{-1} a_i \le w_i^{2/p} \le (\alpha d/n)^{2/p  - 1} \cdot a_i^{\top} (A^{\top} A)^{-1} a_i.
\]
We combine the lower bound above with the property $w_i \approx_{\alpha} d/n$ to upper bound the leverage score $a_i^{\top} (A^{\top} A)^{-1} a_i$ by
\[
(\alpha \cdot d/n)^{2/p} \cdot (\alpha)^{2/p - 1} \cdot (d/n)^{1 - 2/p}=\alpha^{4/p - 1} \cdot d/n.
\]
Similarly, we lower bound the leverage score $a_i^{\top} (A^{\top} A)^{-1} a_i$ by $\alpha^{1-4/p} \cdot d/n$. These two imply the leverage scores are almost uniform: $a_i^{\top} (A^{\top} A)^{-1} a_i \approx_{\alpha^{4/p-1}} d/n$. So for the $\ell_q$ Lewis weights, we approximate it by $\ov{W}=d/n \cdot I$ and get
\[
a_i^{\top} \left( A^{\top} \ov{W}^{1-2/q} A \right)^{-1} a_i = (d/n)^{2/q-1} \cdot a_i^{\top} (A^{\top} A)^{-1} a_i \approx_{\alpha^{4/p-1}} (d/n)^{2/q}.
\]
From the stability of the Lewis weight, we know the actual $\ell_q$ Lewis weights $(w'_1,\ldots,w'_n)$ satisfy $w'_i \approx_{\alpha^{(4/p-1)\cdot c_q}} d/n$
\end{proof}

Now we restate Lemma~\ref{lem:uniform_lewis_bounds_importance} here.
\begin{lemma}\label{lem:non_uniform_import_p}
Given $p$, let the matrix $A$ satisfy (1) $A^{\top} A=I$ and (2) its Lewis weights $w_i \approx_\alpha d/n$ for each row $i$. We have the $\ell_p$ importance weights of $A$ satisfy
\[
\max_x \frac{|a_i^{\top} x|^p}{\|Ax\|_p^p} \in \left[ \alpha^{-C p/2} \cdot d^{p/2}/n , \alpha^{C} \cdot d/n \right]
\]
for $C=4/p-1$.
\end{lemma}
\begin{proof}
For the upper bound, we have $|a_i^{\top} x| \le \|a_i\|_2 \cdot \|x\|_2$ for all $i$. In particular, this implies $\|Ax\|_{\infty} \le \sqrt{\alpha^{C} \cdot d/n} \cdot \|x\|_2$ for $C=4/p-1$ after plugging $q=2$ into Claim~\ref{clm:non_uniform_Lewis_w} to bound $\|a_i\|_2^2$ by the leverage score. Then the rest of the proof is the same as the uniform case shown in the beginning of this section: We lower bound 
\[
\|Ax\|_p^p \ge \|Ax\|_2^2/\|Ax\|^{2-p}_{\infty} = \|x\|_2^2/ (\sqrt{\alpha^{C} \cdot d/n} \cdot \|x\|_2)^{2-p}.
\]
Now we upper bound $\max_x \frac{|a_i^{\top} x|^p}{\|Ax\|_p^p} $ by
\[
\frac{(\sqrt{\alpha^{C} \cdot d/n} \cdot \|x\|_2)^p}{\|x\|_2^2/ (\sqrt{\alpha^{C} \cdot d/n} \cdot \|x\|_2)^{2-p}} \le \alpha^{C} \cdot d/n.
\]

For the lower bound, we choose $x=a_i$ such that $|a_i^{\top} x|=\|a_i\|_2 \cdot \|x\|_2$. Then by Holder's inequality, $\|Ax\|_p^p \le (n)^{\frac{2-p}{2}} \cdot \|Ax\|_2^{p}$. These two show
\[
\frac{|a_i^{\top} x|^p}{\|Ax\|_p^p} \ge \frac{(\sqrt{\alpha^{-C} \cdot d/n} \cdot \|x\|_2)^p}{(n)^{\frac{2-p}{2}} \cdot \|x\|_2^{p}} \ge \alpha^{-C \cdot p/2} \cdot d^{p/2}/n.
\]
\end{proof}
