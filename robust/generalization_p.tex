%!TEX root = robust-colt21.tex
\section{General Analysis for $\ell_p$ Regression}\label{sec:analysis_ellp}
In this section, we consider the $\ell_p$ loss function $L(\beta):=\sum_{i=1}^n |a_i^{\top} \beta - y_i|^p$ for a given $p \in (1,2]$. The main result is to show the upper bound in Theorem~\ref{t:ellp}, i.e., that $\wt{L}(\beta)$, when generated properly according to the Lewis weights with sample size $\wt{O}(\frac{d^2}{\eps^2 \delta})$, satisfies {\cuc}. 

\begin{theorem}\label{thm:property_ellp}
Given $p \in (1,2]$ and a matrix $A \in \mathbb{R}^{n \times d}$, let $(w'_1,\ldots,w'_n)$ be a $\gamma$-approximation of the Lewis weights $(w_1,\ldots,w_n)$ of $A$, i.e., $w_i \approx_{\gamma} w'_i$ for all $i \in [n]$. For $m=O \big( \frac{\gamma \cdot d^2 \log d/\eps\delta}{\eps^2} + \frac{ \gamma \cdot d^{2/p}}{\eps^2 \delta} \big)$ with a sufficiently large constant, we sample $s_i \sim \frac{d}{m \cdot w'_i} \cdot \poiss(\frac{m \cdot w'_i}{d})$ for each $i \in [n]$.

Then with probability $1-\delta$, $|\supp(s)|=O(m)$ and $\wt{L}(\beta):=\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|^p$ satisfies \cuc. Namely,
\[ \wt{L}(\beta) - \wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot L(\beta) \quad\text{ for any $\beta \in \mathbb{R}^d$}. \]
\end{theorem}

First of all, we bound the support size of $s$. Note that $\Pr[s_i>0]= 1- e^{-\frac{m \cdot w_i'}{d}} \le \frac{m \cdot w_i'}{d}$. So $\E[|\supp(s)|] \le \sum_i \frac{m \cdot w'_i}{d} \le \gamma \cdot m$. Since the variance of $|\supp(s)|$ is bounded by $\gamma \cdot m$, with probability $1-\delta/2$, the sample size is $O(\gamma m+ \sqrt{\gamma m \cdot \log 1/\delta} + \log 1/\delta)=O(\gamma m)$ by the Bernstein inequalities.
Also, Lemma~\ref{lem:compute_Lewis} provides an efficient algorithm to generate $s$ with $\gamma=2$.

In the rest of this section, we outline the proof of the \cuc~property in Theorem~\ref{thm:property_ellp}, while the formal proof is deferred to Appendix~\ref{sec:property_uniform_Lewis}. The proof has two steps. 

The 1st step is a reduction to the case of uniform Lewis weights.
% , which is in the same spirit of the proof of Theorem~\ref{thm:Lewis_weight_importance}
Specifically, we create an equivalent problem with matrix $A' \in \mathbb{R}^{N \times d}$ and $y' \in \mathbb{R}^{N}$ such that $L(\beta) = \|A' \beta - y'\|^p_p$ and the Lewis weights of $A'$ are almost uniform. Moreover, we show an equivalent way to generate $(s_1,\ldots,s_n)$ so that  $\wt{L}(\beta)=\|S'A'\beta - S' y'\|_p^p$. Thus, showing that the new empirical loss $\|S' A' \beta -S'  y'\|_p^p$ satisfies {\cuc} with respect to $\|A' \beta - y'\|_p^p$ will imply the same property for $SA$. 

In the 2nd step, we interpolate techniques for $\ell_1$ and $\ell_2$ losses to prove the property for the special case of almost uniform Lewis weights (namely $A'$ in the above paragraph). Let us discuss the key ideas in this interpolation. For ease of exposition, we assume $\beta^*=0$ and $A^{\top} A = I$. Furthermore let $\alpha$ denote the approximation parameter of the uniform sampling compared to the Lewis weights of $A$ after the 1st step reduction, i.e., $w_i \approx_{\alpha} d/n$. 

We will again consider the \cuc~as a concentration of $\wt{L}(\beta)-\wt{L}(\beta^*)$, which can be written as $\sum_i s_i \cdot \big( |a_i^\top \beta - y_i|^p - |y_i|^p \big)$ from the assumption $\beta^*=0$. Our proof heavily relies on the following two properties of $A$ when its Lewis weights are almost uniform.
\begin{fact}\label{fact:almost_uniform}
   Let $A$ be an $n\times d$ matrix such that $A^\top A=I$. If $w_i \approx_{\alpha} d/n$ for every $i\in [n]$, then:
   \begin{enumerate}
   \item
     The leverage scores are almost uniform (recall that when $A^{\top} A=I$, the $i$th leverage score becomes $\|a_i\|_2^2$):
\begin{equation}\label{eq:leverage_score_ell2}
\|a_i\|_2^2 \approx_{\alpha^{C_p}} d/n \quad \text{ for } \quad C_p=4/p-1.
\end{equation}
\item The importance weights of $\|A \beta\|^p_p$ are almost uniform:
\begin{equation}\label{eq:importance_ellp}
\forall \beta, \quad \forall i \in [n], \quad \frac{\|a_i^{\top} \beta\|_p^p}{\|A \beta\|_p^p} \le w_i \le \alpha \cdot d/n.
\end{equation}
\end{enumerate}
\end{fact}
The first property is implied by Claim~\ref{clm:non_uniform_Lewis_w} in Appendix~\ref{sec:Lewis_weight_importance} and the 2nd one is from Theorem~\ref{thm:Lewis_weight_importance}.

The major difference between the analyses of $p>1$ and $p=1$ is that we cannot use the contraction principle to remove the effect of outliers in $\sum_{i} s_i \cdot |a_i^{\top} \beta - y_i|^p$. For example, when $p=2$, we always have the cross term $2\sum_i s_i \cdot (a_i^{\top} \beta) y_i$ besides $\sum_i s_i \big((a_i^{\top} \beta)^2 + y_i^2 \big)$. For general $p\in(1,2)$, the cross term is replaced by the 1st order Taylor expansion of $\sum_{i} s_i \cdot |a_i^{\top} \beta - y_i|^p$. While the assumption that $\beta^*=0$ implies that its expectation is $\sum_i (a_i^{\top} \beta) y_i=0$, we can only use Chebyshev's inequality to conclude that the cross term is small for \emph{a fixed} $\beta$. Since the coefficient $s_i$ is independent of $y_i$, we cannot rely on a stronger Chernoff/Bernstein type concentration. This turns out to be insufficient to use the union bound \emph{for all} $\beta$. 

So our key technical contribution in the 2nd step is to bound the cross term for all $\beta$ simultaneously. To do this, we rewrite it as an inner product between $\beta$ and $( \langle A[*,j], S^p \cdot y \rangle )_{j \in [d]}$ (say $p=2$), where $A[*,j]$ denotes the $j$th column of $A$. To save the union bound for $\beta$, we bound the $\ell_2$ norms of these two vectors separately to apply the Cauchy-Schwartz inequality. While prior work \citep{CP19} for $\ell_2$ loss provides a way to bound the 2nd vector $( \langle A[*,j], S^p \cdot y \rangle )_{j \in [d]}$ given bounded leverage scores in \eqref{eq:leverage_score_ell2}, the new ingredient is to bound $\|\beta\|_2$ in terms of $\|A \beta\|_p$ for $p<2$ when the Lewis weights are uniform. This is summarized in the following claim, stated for general $p \in (1,2)$, in which we bound the 1st order Taylor expansion of $\sum_{i} s_i \cdot |a_i^{\top} \beta - y_i|^p$.
\begin{claim}\label{clm:inner_product}
  If $w_i \approx_{\alpha} d/n$ for every $i\in [n]$, then with probability at least $1-\delta$, we have the following bound for all $\beta\in\R^d$:
  \vspace{-3mm}
\begin{equation}\label{eq:bound_inner}
\sum_{i=1}^n s_i \cdot p \cdot |y_i|^{p-1} \cdot \sign(y_i) \cdot a_i^{\top} \beta = O\bigg(\sqrt{\frac{\alpha^{\frac{2+p}{p}} \cdot \gamma \cdot d^{2/p}}{\delta \cdot m}} \cdot \|A \beta\|_p \cdot \|y\|_p^{p-1}\bigg).
\end{equation}
\end{claim}
The rest of the proof (given in Appendix~\ref{sec:property_uniform_Lewis}) is centered on obtaining a strong concentration bound for the 2nd order Taylor expansion using \eqref{eq:importance_ellp}. We remark that in the special case of $p=2$, this analysis simplifies considerably, and it can be easily adapted to show an $O(\frac{d\log d}{\delta\epsilon^2})$ bound on the query complexity of {\cuc} for least squares regression.


% For $p=2$, we remark that our approach shows $m=O(\frac{d \log d}{\delta \eps^2})$ because the concentration for the 2nd order Taylor expansion can be replaced by matrix concentration bound.
% the 2nd order Taylor expansion is $\|A \beta\|_2^2$ matrix concentration bound.

%Given $m$ and $A$, For each $i\in[n]$, we randomly generate $s_i=\frac{n}{m}$ with probability $\frac{m}{n}$ and $0$ with probability $1-\frac{m}{n}$. Similar to Section~\ref{sec:uniform_concergence}, we define $L(\beta):=\sum_{i=1}^n |a_i^{\top} \beta - y_i|^p$ and $\beta^*:=\arg\min_{\beta} L(\beta)$. After generating $s_1,\ldots,s_n$, we define $\wt{L}(\beta):=\sum_i s_i \cdot |a_i^{\top} \beta - y_i|^p$.

