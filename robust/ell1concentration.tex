%!TEX root = robust-colt21.tex
\section{Near-optimal Analysis for $\ell_1$ Regression}\label{sec:analysis_ell1}
We consider the $\ell_1$ loss function $L(\beta)=\|A \beta - y\|_1$ in this section. The main result is to prove the upper bound in Theorem~\ref{t:ell1}, namely that we can use Lewis weight sampling with support size $\wt{O}(d/\eps^2)$ to construct $\wt{L}(\beta)$ that satisfies the {\cuc} property.

\begin{theorem}\label{thm:concentration_contraction}
Given an $n\times d$ matrix $A$ and $\epsilon,\delta\in(0,1)$, let $(w'_1,\ldots,w'_n)$ be a $\gamma$-approximation of the Lewis weights $(w_1,\ldots,w_n)$ of $A$, i.e., $w'_i \approx_{\gamma} w_i$ for all $i \in [n]$. Let $u = \Theta\big( \frac{\eps^2}{\log \gamma d/\delta \eps} \big)$ and $p_i=\min\{\gamma w'_i/u, 1\}$. For each $i\in [n]$, with probability $p_i$, set $s_i=1/p_i$ (otherwise, let $s_i=0$). 

Let $L(\beta)=\|A \beta - y\|_1$ with an unknown label vector $y \in \mathbb{R}^n$. With probability $1-\delta$, the support size of $s$ is $O(\frac{\gamma^2 d \cdot \log \gamma d/\eps\delta}{\eps^2})$ and $\wt{L}(\beta):=\sum_{i=1}^n s_i \cdot |a_i^\top \beta - y_i|$ satisfies \cuc:
\[
\wt{L}(\beta)-\wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \epsilon \cdot L(\beta) \quad\text{ for all } \beta.
\]
\end{theorem}

First of all, we bound the sample size (i.e., the support size of $s$). Since $\sum_i w_i=d$ and $w'_i \approx_{\gamma} w_i$, let $m:=\sum_i p_i$ denote the expected sample size, which is at most $\gamma/u \cdot \sum_i w'_i \le \gamma^2 d/u$. Since the variance of $|\supp(s)|$ is at most $m$, with probability $1-\delta/2$, the support size of $s$ is $O(m+ \sqrt{m \cdot \log 1/\delta} + \log 1/\delta)=O(\frac{\gamma^2 d \cdot \log \gamma d/\eps\delta}{\eps^2})$ by the Bernstein inequalities. Moreover, Lemma~\ref{lem:compute_Lewis} provides an efficient algorithm to generate $s$ with $\gamma=2$.

%Our goal is to bound the sample complexity $m$ to give concentration bounds of $\delta_i(\beta)-|y_i|$ for all $\beta$. Specifically, for a fixed matrix $A$ and a vector $y$, we want $m$ indices $i_1,\ldots,i_m$ with weight $w_1,\ldots,w_m$ to satisfy
%\begin{equation}\label{prop:concentration}
%\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big) = \sum_{j=1}^m w_j \big( \delta_{i_j}(\beta) - |y_{i_j}| \big) \pm \eps \cdot \sum_{i=1}^n \bigg( |y_i| + |\delta_i (\beta)| \bigg) \text{ for all } \beta.
%\end{equation}


%be the sequence of probabilities
%\[
%q_i \ge \ov{w}_i/u \text{ for } u =\Theta\left( \frac{\eps^2}{\log (m/\eps+d/\eps)} \right) \text{ where } m = \sum_i q_i.
%\]
\iffalse
\textcolor{red}{Michal:
  One way to write the result in a general form is: Consider a non-negative function $f:\mathbb{R}^n\rightarrow\mathbb{R}_{\geq 0}$ such that for all $\alpha\in\mathbb{R}^n$:
  \begin{align*}
    f(\alpha)\leq \|\alpha\|_1\quad\text{and}\quad \big|f(\alpha)-\|\alpha\|_1\big|\leq K.
  \end{align*}
  Then, our result from Theorem \ref{thm:concentration_contraction} could be written as follows:
  \begin{align*}
    f(SA\beta) = (1\pm\epsilon)\cdot f(A\beta) \pm \epsilon\cdot K\quad\text{for}\quad f(\alpha) = \big|\|\alpha-y\|_1-\|y\|_1\big|,\quad K=2\|y\|_1.
  \end{align*}
Further,  note that for $f(\alpha)=\|\alpha\|_1$ we get $K=0$ and standard $l_1$-concentration.
}

\textcolor{blue}{Xue: I am afraid that this won't work for all $y$ until $m=\Omega(n)$. My point is that in that scenario, we could choose $y$ after fixing $S$, which is the main trouble. For example, suppose the Lewis weight is uniform and we generate $S$ with support size $m=o(n)$. Now we only consider a fix $\beta$ such that $\sum_{j: s_j=0} |a_j^{\top} \beta| \ge \frac{1}{2} \|A \beta\|_1$. Then I could choose $y$ such that $y_j=a_j^{\top}\beta/2$ for $j$ with $s_j\neq 0$ and $y_j=0$ otherwise. So $\sum_{j=1}^m w_j \big( \delta_{i_j}(\beta) - |y_{i_j}| \big)=0$ but $\sum_{i=1}^n \big( \delta_i(\beta) - |y_i| \big)\ge \frac{1}{2}\|A \beta\|_1$.} 
\fi

In the rest of this section, we prove the \cuc~property in Theorem~\ref{thm:concentration_contraction}. Before showing the formal proof in Section~\ref{sec:concentration_contraction}, we discuss the main technical tool --- a concentration bound for all $\beta$ with bounded $\|A (\beta-\beta^*)\|_1$. 

\begin{lemma}\label{lem:high_prob_guarantee}
Given $A \in \mathbb{R}^{n \times d}$, $\epsilon$ and a failure probability $\delta$, let $w_1,\ldots,w_n$ be the Lewis weight of each row $A_i$. Let $p_1,\ldots,p_n$ be a sequence of numbers upper bounding $w$, i.e.,
\[
p_i \ge \min\{ w_i/u, 1\} \text{ for } u =\Theta\left( \frac{\eps^2}{\log (m/\delta+d/\eps\delta)} \right) \text{ where } m = \sum_i p_i.
\]
Suppose that the coefficients $(s_1,\ldots,s_n)$ in $\wt{L}(\beta):=\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|$ are generated as follows: For each $i \in [n]$, with probability $p_i$, we set $s_i=1/p_i$. Given any subset $B \subset \mathbb{R}^d$ of $\beta$, with probability at least $1-\delta$,
\[
\wt{L}(\beta)-\wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \eps \cdot \sup_{\beta \in B}\|A (\beta-\beta^*)\|_1 \quad \text{ for all } \beta \in B.
\]
\end{lemma}

We remark that Lemma~\ref{lem:high_prob_guarantee} holds for any choice of $B$ while the error depends on its radius via the term $\eps \cdot \sup_{\beta \in B}\|A (\beta-\beta^*)\|_1$. For example, we could apply this lemma to $B:=\big\{ \beta \mid \|A (\beta-\beta^*)\|_1 \le 3 L(\beta^*) \big\}$ to conclude the error is at most $\epsilon \cdot \|A (\beta-\beta^*)\|_1 \le 3 \epsilon\cdot L(\beta^*)$, which is at most $3 \epsilon \cdot L(\beta)$ by the definition of $\beta^*$. However, this only gives guarantee for bounded $\beta$. To bound the error in terms of $\eps L(\beta)$ for \emph{all} $\beta \in \mathbb{R}^d$ with high probability, we partition $\beta \in \mathbb{R}^d$ into several subsets and apply Lemma~\ref{lem:high_prob_guarantee} to those subsets separately. We defer the detailed proof of Theorem~\ref{thm:concentration_contraction} to Section~\ref{sec:concentration_contraction}. 

The proof of Lemma~\ref{lem:high_prob_guarantee} is based on the contraction principle from~\cite{LTbook} and chaining arguments introduced in \cite{Talagrand90, CP15_Lewis}. The main observation in its proof is that after rewriting $\wt{L}(\beta)-\wt{L}(\beta^*)=\sum_{i=1}^n s_i \cdot (|a_i^{\top} \beta - y_i|-|a_i^{\top} \beta^* - y_i|)$, all vectors in this summation, $\big( |a_i^{\top} \beta - y_i|-|a_i^{\top} \beta^* - y_i| \big)_{i \in [n]}$, contract from the vector $\big( |a_i^{\top} (\beta-\beta^*) |\big)_{i \in [n]}$ in the $\ell_1$ summation. So, using the contraction principle \citep{LTbook}, $\wt{L}(\beta)-\wt{L}(\beta^*)$ will concentrate at least as well as $\|SA(\beta-\beta^*)\|_1$, which can be analyzed using the $\ell_1$ subspace embedding property (see Fact \ref{fact:subspace_emd}). We defer the formal proof to Appendix~\ref{sec:proof_thm_deviation}. 

\subsection{Proof of Theorem~\ref{thm:concentration_contraction}}\label{sec:concentration_contraction}
Recall that $m=\sum_i p_i \le \gamma^2 d / u$ and $u =\Theta\left( \frac{\eps^2}{d/\eps\delta} \right)$. Since $\log m/\eps\delta=O(\log d\gamma /\eps\delta)$, we choose a small constant for $u$ such that for some large $C$, $
u \le \frac{\eps^2}{C \log (m/\eps\delta +d/\eps\delta)} \text{ and } p_i \ge \min\{1,w_i/u\}$.

For convenience, and without loss of generality, we assume $\beta^*=0$ such that~$L(\beta^*)=\|y\|_1$ (this is because we can always shift $y$ to $y- A \beta^*$ and $\beta^*$ to $0$). Thus we can write \[
\wt{L}(\beta)-\wt{L}(\beta^*)=\|S A \beta - Sy\|_1 - \|S y\|_1 \quad\text{ and }\quad L(\beta)-L(\beta^*)=\|A \beta - y\|_1 - \|y\|_1
\] so that we can use the triangle inequality of the $\ell_1$ norm. Next, notice that $
\E_S[\|S y\|_1]=\|y\|_1.$
By Markov's inequality, $\|S y\|_1 \le \frac{1}{\delta} \cdot \|y\|_1$ holds with probability $1-\delta$. 
We now apply Lemma~\ref{lem:high_prob_guarantee} multiple times with different choices of $B_{\beta}$. We choose $t=O(1/\eps^2\delta)$ and $B_i=\big\{ \beta \mid \|A \beta\|_1 \le (3+\eps \cdot t) \|y\|_1 \big\}$ for $i=0,1,\ldots,t$. Then we apply Lemma~\ref{lem:high_prob_guarantee} with failure probability $\frac{\delta}{t+1}=O(\delta^2 \epsilon^2)$ to guarantee that with probability $1-\delta$, we have for \emph{all} $i=0,1,\ldots,t$: %(recall the assumption $\beta^*=0$)
\[
  \|S A \beta - Sy\|_1 - \|S y\|_1 = \|A \beta\|_1 - \|y\|_1 \pm \eps \cdot \sup_{\beta \in B_i} \|A \beta\|_1 \quad\text{ for all } \beta \in B_i.
\]
Furthermore, with probability $1-\delta$ over $S$, we have the $\ell_1$ subspace embedding property:
\[
\forall \beta, \|S A \beta\|_1 = (1\pm \eps) \cdot \|A \beta\|_1.
\]
We can now argue that the concentration holds for all $\beta$, by considering the following three cases:
\begin{enumerate}
\item $\beta$ has $\|A \beta\|_1 < 3 \|y\|_1$: From the concentration of $B_0$, the error is $\leq\eps \cdot 3 \|y\|_1 \le 3\eps \cdot L(\beta)$.

\item $\beta$ has $\|A \beta\|_1 \in \big[(3+\eps \cdot i) \cdot \|y\|_1, (3+ \eps \cdot (i+1))\cdot \|y\|_1 \big)$ for $i<t$: Let $\beta'$ be the rescaling of $\beta$ with $\|A \beta'\|_1=(3+\eps \cdot i) \|y\|_1$ such that $\|A (\beta -\beta')\|_1 \le \eps \|y\|_1$. Then we rewrite $\|A \beta - y\|_1 - \|y\|_1$ as
\[
\|A (\beta - \beta' + \beta') - y\|_1 - \|y\|_1 = \|A \beta' - y\|_1 - \|y\|_1 \pm \|A (\beta-\beta')\|_1 = \|A \beta' - y\|_1 - \|y\|_1 \pm \eps \|y\|_1.
\]
Similarly, we rewrite $\wt{L}(\beta)-\wt{L}(\beta^*)$ as $\|S A \beta - S y\|_1 - \|S y\|_1$ and bound it by
\begin{align*}
 \|S A \beta' - S y\|_1 - \|S y\|_1 \pm \|S A (\beta-\beta')\|_1 & = \|S A \beta' - S y\|_1 - \|S y\|_1 \pm (1+\eps ) \cdot \|A(\beta-\beta')\|_1 \\ 
 & =\|S A \beta' - S y\|_1 - \|S y\|_1 \pm (1+\eps ) \cdot \eps\|y\|_1,
\end{align*}
where we use the $\ell_1$ subspace embedding in the middle step. Next we use the guarantee of $\beta' \in B_i$ to bound the error between $\|A \beta' - y\|_1 - \|y\|_1$ and $\|S A \beta' - S y\|_1$ by $\eps \cdot \|A \beta'\|_1$. So the total error is $O(\eps) \cdot (\|A \beta'\|_1 +\|y\|_1) = O(\eps) \cdot L(\beta)$ since $L(\beta) \ge \|A \beta'\|_1/2$ by the triangle inequality.

\item $\beta$ has $\|A \beta\|_1 \ge \frac{25}{\eps \delta} \cdot \|y\|_1$: We always have $\|A \beta - y\|_1 - \|y\|_1 = \|A \beta\|_1 \pm 2 \|y\|_1$ by the triangle inequality. On the other hand, the $\ell_1$ subspace embedding and the bound on $\|Sy\|_1$ imply: 
\[
\|SA\beta - S y\|_1 - \|S y\|_1 = \| S A \beta\|_1 \pm 2 \|S y\|_1= (1 \pm \eps)\|A \beta\|_1 \pm \frac{2}{\delta} \|y\|_1.
\] Since $\|A \beta\|_1 \ge \frac{25}{\eps \delta} \cdot \|y\|_1$, this becomes $(1 \pm \epsilon) \|A\beta\|_1 \pm \frac{2}{\delta} \cdot \frac{\eps \delta}{25} \|A \beta\|_1 = (1 \pm \epsilon \pm \frac{2\epsilon}{25}) \|A \beta\|_1$.
Again, the error is $2\eps \|A \beta\|_1 = O(\eps) \cdot L(\beta)/2$ by the triangle inequality.
\end{enumerate}

\iffalse
\paragraph{\textcolor{blue}{Xue: This is the plan in my mind. Please take it as a grain of salt because we may need to adjust parameters and inequalities later.}} We will start by reproving the results in Section 8 of \cite{CP15_Lewis}. Let $u$ be an upper bound on the Lewis weight of $A$. For example, let us set $u=d/n$ such that the Lewis weight is uniform to gain some intuition. Then Lemma 8.4 in \cite{CP15_Lewis} indicates that when we randomly pick each coordinate $i$ with probability $1/2$ (say the moment $\ell=O(\log n/\delta)$, $\sigma_i=1$ means we keep it otherwise $\sigma_i=-1$ means we discard it), with high probability, 
\[
\max_{x: \|Ax\|_1=1} \sum_{i=1}^n \frac{\sigma_i+1}{2}  \cdot |a_i^{\top} x| \le \frac{1}{2} + O(\sqrt{\frac{d \log n}{n}}).
\]
If we choose $\eps=O(\sqrt{\frac{d \log n}{n}})$, this means $m \approx n/2=\Theta(d \log n/\eps^2)$ is enough to give the concentration for all $x$. This saves an extra $d$ factor compare to the union bound. Formally Lemma 7.4 in \cite{CP15_Lewis} shows how to turn Lemma 8.4 (which samples about half rows) into a concentration bound for any number $m$.

Back to our problem, instead of considering the ball $\bigg\{x \bigg| \|Ax\|_1=1\bigg\}$, we will consider the ball $B_{\beta}:=\bigg\{ \beta \bigg | \sum_{i=1}^n \delta_i(\beta) \le B \sum_{i=1}^n |y_i| \bigg\}$. Furthermore, after normalizing, the error term in \eqref{prop:concentration} is exactly $\eps/B$ which corresponds to error $O(\sqrt{\frac{d \log n}{n}})$ in $\ell_1$ concentration. 

However, one big change is that we will use $g \sim N(0,1)^n$ instead of $\sigma \in \{\pm 1\}^n$ in this calculation. The main reason is that we need some comparison theorem (for our choice of $\delta_i(\beta)$ rather than $|a_i^\top x|$), which I only know the version for Gaussian variables and may be false for subGaussian variables. In the rest of this section, we plan to reprove Lemma 8.4 and 7.4 in \cite{CP15_Lewis}.
\fi