\section{Additional Proofs from Section~\ref{sec:lower_bound}}\label{app:proof_lower}
We finish the proof of Theorem~\ref{thm:information_lower_bound} in this section.

\begin{proofof}{Theorem~\ref{thm:information_lower_bound}}
For contradiction, we assume there is an algorithm $P$ that outputs an $(1+\eps/200)$-approximation with probability $1-\delta$ using $m$ queries on $y$. We first demonstrate $m = \Omega(\frac{\log 1/\delta}{\eps^2})$. We pick $\alpha \in \{\pm 1\}$ and generate $y$ as follows:
\begin{enumerate}
\item $(y_1,\ldots,y_{n/d})$ are generated from $D_{\alpha}$ defined in Section~\ref{sec:lower_bound}.
\item The remaining entries are 0, i.e. $y_i=0$ for $i>n/d$.
\end{enumerate}
So $\beta^*=(\alpha,0,\ldots,0)$ with probability $1-\delta/d$. For any $(1+\eps/200)$-approximation $\wt{\beta}$, its first entry has $\sign(\wt{\beta}_1) = \alpha$ from Claim~\ref{clm:distinguish_two_dist}. By the lower bound in Lemma~\ref{lem:information_lower_dim_1}, the algorithm must make $\Omega(\frac{\log 1/\delta}{\eps^2})$ queries to $y_1,\ldots,y_{n/d}$.

Then we show $m=\Omega(d/\eps^2)$. For convenience, the rest of the proof considers a fixed $\delta=0.01$. By Yao's minmax principle, we consider a deterministic algorithm $P$ in this part. Now let us define the distribution $D_b$ over $\{\pm 1\}^n$ for $b \sim \{\pm 1\}^d$ as follows:
\begin{enumerate}
\item We sample $b \sim \{ \pm 1\}^d$.
\item We generate $y \sim D_b$ where $D_b = D_{b_1} \circ D_{b_2} \circ \cdots \circ D_{b_d}$.
\end{enumerate}
For any $b$, when $n>100d \log d/\eps^2$ and $n'=n/d$, with probability $0.99$, for each $i \in [d]$, $D_{b_i}$ will generate $n'$ bits where the number of bits equaling $b_i$ is in the range of $[(1/2+\eps/2)n',(1/2+3\eps/2)n']$. We assume this condition in the rest of this proof. From Claim~\ref{clm:distinguish_two_dist}, $\beta^*$ minimizing $\|A \beta - y\|_1$ will have $b_i=\sign(\beta^*_i)$ for every $i \in [d]$. 

Next, given $A$ and $y$, let $\tilde{\beta}$ be the output of $P$. We define $b'$ as $b'_i=\sign(\tilde{\beta}_i)$ for each coordinate $i$.
We show that $b'$ will agree with $b$ (the string used to generate $y$) on 0.99 fraction of bits when $P$ outputs an $(1+\eps/200)$-approximation. As discussed before, $\|A \tilde{\beta}-y\|_1$ is the summation of $d$ subproblems for $d$ coordinates separately, i.e., $L_1(\tilde{\beta}_1),\ldots,L_d(\tilde{\beta}_d)$. In particular, $\|A \tilde{\beta}-y\|_1 \le (1+\epsilon/200)\|A \beta^*-y\|_1$ implies
\[
\sum_{i=1}^d L_i(\tilde{\beta}_i) \le (1+\eps/200) \sum_{i=1}^d L_i(\beta^*_i).
\]
At the same time, we know $L_i(\tilde{\beta}_i) \ge L_i(\beta^*_i)$ and $L_i(\beta^*_i) \in [(1-\eps)n',(1-3\eps)n']$ for any $i$. This implies that for at least $0.99$ fraction of $i \in [d]$, $L_i(\tilde{\beta}_i) \le (1+\eps) L_i(\beta^*_i)$: Otherwise the approximation ratio is not $(1+\eps/200)$ given
\begin{equation}
0.99 \cdot (1-\eps)+0.01 \cdot (1-3\eps) \cdot (1+\eps) > (1+\eps/200) \cdot \big( 0.99 \cdot (1-\eps)+0.01(1-3\eps) \big).
\end{equation}
 From Claim~\ref{clm:distinguish_two_dist}, for such an $i$, $\tilde{\beta}_i$ will have the same sign with $\beta^*_i$. So $b'$ agree with $b$ on at least $0.99$ fraction of coordinates. 

For each $b$, let $m_i(b)$ denote the expected queries of $P$ on $y_{(i-1)(n/d)+1},\ldots,y_{i(n/d)}$ (over the randomness of $y \sim D_b$). Since $P$ makes at most $m$ queries, we have $\E_y[\sum_i m_i(b)] \le m$.

Now for each $b \in \{\pm 1\}^d$ and coordinate $i \in [d]$, we say that the coordinate $i$ is good in $b$ if (1) $\E_{y}[m_i(b)] \le 60m/d$; and (2) $b'_i=b_i$ with probability 0.8 over $y \sim D_b$ when $b'=\sign(\tilde{\beta})$ defined from the output $\tilde{\beta}$ of $P$. 

Let $b^{(i)}$ denote the flip of $b$ on coordinate $i$. We plan to show the existence of $b$ and a coordinate $i \in [d]$ such that $i$ is good in both $b$ and $b^{(i)}$: Let us consider the graph $G$ on the Boolean cube that corresponds to $b \in \{ \pm 1\}^d$ and has edges of $(b, b^{(i)})$ for every $b$ and $i$. To show the existence, for each edge $(b,b^{(i)})$ in the Boolean cube, we remove it if $i$ is not good in $b$ or $b^{(i)}$. We prove that after removing all bad events by a union bound, $G$ still has edges inside. 

For the 1st condition (1) $\E[m_i(b)] \le 60m/d$, we will remove at most $d/60$ edges from each vertex $b$. So the fraction of edges removed by this condition is at most $1/30$. For the 2nd condition, from the guarantee of $P$, we know
\[
\Pr_{b,y}[\tilde{\beta} \text{ is an $(1+\eps/200)$ approximation}] \ge 0.99.
\]
So for $0.8$ fraction of points $b$, we have $\Pr_{y \sim D_b}[\tilde{\beta} \text{ is an $(1+\eps/200)$ approximation}] \ge 0.95$ (Otherwise we get a contradiction since $0.8+0.2 \cdot 0.95=0.99$). Thus, when $y \sim D_b$ for such a string $b$, w.p. 0.95, $b'$ will agree with $b$ on at least $0.99$ fraction of coordinates from the above discussion. In another word, for such a string $b$, \[
\E_{y \sim D_b}[\sum_i 1(b_i=b'_i)] \ge 0.95 \cdot 0.99 n.
\] 
This indicates that there are at least $0.65$ fraction of coordinates in $[d]$ that satisfy $\Pr_y[b_i=b'_i] \ge 0.8$ (otherwise we get a contradiction since $0.65+0.35 \cdot 0.8=0.93$). Hence, such a string $b$ will have $0.65$ fraction of \emph{good} coordinates.

Back to the counting of bad edges removed by the 2nd condition, we will remove at most $2 \cdot (0.2+0.8 \cdot 0.35)=0.96$ fraction of edges. Because $1/30+0.96<1$, we know the existence of $b$ and $b^{(i)}$ such that $i$ is a good coordinate in $b$ and $b^{(i)}$.

Now we use $b, b^{(i)}$ and $P$ to construct an algorithm for Bob to win the game in Lemma~\ref{lem:information_lower_dim_1}. From the definition, $P$ makes $60m/d$ queries in expectation on entries $y_{(i-1)(n/d)+1},\ldots,y_{i(n/d)}$ and outputs $b'_i=b_i$ with probability 0.8. Since halting $P$ at $20 \cdot 60m/d$ queries on entries $y_{(i-1)(n/d)+1},\ldots,y_{i(n/d)}$ will reduce the success probability to $0.8-0.05=0.75$. We assume $P$ makes at most $1200m/d$ queries with success probability 0.75. Now we describe Bob's strategy to win the game: 
\begin{enumerate}
\item Randomly sample $b_j \sim \{\pm 1\}$ for $j \neq i$.
\item Simulate the algorithm $P$: each time when $P$ asks the label $y_{\ell}$ for $\ell \in [(j-1)n/d+1,jn/d]$.
\begin{enumerate}
\item If $j=i$, Bob queries the corresponding label from Alice.
\item Otherwise, Bob generates the label using $D_{b_j}$ by himself.
\end{enumerate}
\item Finally, we use $P$ to produce $b' \in \{\pm 1\}^n$ and outputs $b'_i$.
\end{enumerate}
Bob wins this game with probability at least
\[
\Pr_{a}[a=-1] \cdot \Pr_{y}[b'_i=-1] + \Pr_{a}[a=1] \cdot \Pr_{y}[b'_i=1] \ge 0.75
\]
from the properties of $i$ in $b$ and $b^{(i)}$. So we know $1200m/d=\Omega(1/\eps^2)$, which lower bounds $m=\Omega(d/\eps^2)$.
\end{proofof}

\iffalse
\subsection{Lower bound for one dimension}\label{sec:lower_bound_dim_1}
To finish the proof of Lemma~\ref{lem:information_lower_dim_1}, we show that Bob has no strategy to win the game with probability more than $0.75$ when $m \le \frac{0.001}{\eps^2}$. By Yao's minmax principle, we assume Bob's strategy is deterministic. Since Bob only receives a string $r$ of length $m$ from Alice, Bob's strategy depends on $r \in \{\pm 1\}^m$. So the best strategy of Bob to win the game is to compare $\Pr_{D_1}[r]$ and $\Pr_{D_{-1}}[r]$: If $\Pr_{D_b}[r]$ is larger, Bob claims $b$. 

For convenience, let $T \subseteq \{\pm 1\}^m$ denote those strings $r$ with $\Pr_{D_1}[r] \ge \Pr_{D_{-1}}[r]$. So Bob wins the game with probability 
\[
\frac{1}{2} \cdot \bigg( \Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] \bigg).
\]
Then the advantage comparing to randomly guess $b \sim \{1,2\}$ is 
\begin{align*}
& \frac{1}{2} \cdot \bigg( \Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] \bigg) - \frac{1}{2} \\
= & \frac{1}{2} \bigg( \Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] - 1 \bigg)\\
= & \frac{1}{2} \bigg( \Pr_{r \sim D_1}[r \in T] - \frac{\Pr_{r \sim D_1}[r \in T] + \Pr_{r \sim D_1}[r \not\in T]}{2} + \Pr_{r \sim D_{-1}}[r \notin T] - \frac{\Pr_{r \sim D_{-1}}[r \in T] + \Pr_{r \sim D_{-1}}[r \not\in T]}{2}\bigg)\\
= & \frac{1}{4} \bigg( \Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T] + \Pr_{r \sim D_{-1}}[r \notin T] - \Pr_{r \sim D_1}[r \notin T]\bigg)\\
= & \frac{1}{2} \bigg( \Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T]\bigg).
\end{align*}
Furthermore, by the definition of $D_1$ and $D_{-1}$, $T=\{r| \sum_i r_i >0 \}$. In the rest of this proof, we will show $\Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T] \le 0.5$. For convenience, we assume $m$ is even:
\begin{align*}
& \Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T]\\
=& \sum_{\text{even } j \in [2,\ldots,m]} (1/2+\epsilon)^{\frac{m+j}{2}} \cdot (1/2+\epsilon)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}} - (1/2-\epsilon)^{\frac{m+j}{2}} \cdot (1/2+\epsilon)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}}\\
=& \sum_{\text{even } j \in [2,\ldots,m]} (1/4-\epsilon^2)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}}  \cdot \bigg( (1/2+\epsilon)^{j} - (1/2-\epsilon)^{j}  \bigg)\\
=& \sum_{\text{even } j \le 3 \sqrt{m}} (1/4-\epsilon^2)^{\frac{m-j}{2}} \cdot {m \choose \frac{m+j}{2}}  \cdot \bigg( (1/2+\epsilon)^{j} - (1/2-\epsilon)^{j}  \bigg) + \Pr_{r \sim D_1}[\sum_i r_i > 3 \sqrt{m}].
\end{align*}
By the Chebyshev inequality, the 2nd probability is upper bounded by $\frac{m \cdot (1-\epsilon^2)}{9m}\le 1/9$. We use the Stirling formula $\sqrt{2 \pi} \cdot n^{n+1/2} \cdot e^{-n}\le n!\le e \cdot n^{n+1/2} \cdot e^{-n}$ to simplify the first probability as
\begin{align*}
& \sum_{\text{even } j \le 3 \sqrt{m}} 2^{-(m-j)} \cdot \frac{e \cdot m^{m+1/2} }{2 \pi \cdot (\frac{m+j}{2})^{\frac{m+j}{2}} \cdot (\frac{m-j}{2})^{\frac{m-j}{2}}} \cdot \bigg( (1/2+\epsilon)^{j} - (1/2-\epsilon)^{j}  \bigg) \\
\le & \sum_{\text{even } j \le 3 \sqrt{m}} \frac{e}{2 \pi} \cdot \frac{\sqrt{m}}{\sqrt{(m+j)/2} \cdot \sqrt{(m-j)/2}} \cdot \bigg( (1+2\epsilon)^{j} - (1-2\epsilon)^{j} \bigg)\\
\le & \frac{e}{\pi \sqrt{m}} \sum_{\text{even } j \le 3 \sqrt{m}} \bigg( (1+2\epsilon)^{j} - (1-2\epsilon)^{j} \bigg).
\end{align*}
Now we use the fact $m=\frac{0.001}{\eps^2}$ and $j\le 3\sqrt{m}<0.1/\eps$ to upper bound it by
\[
\frac{e}{\pi \sqrt{m}} \sum_{\text{even } j \le 3\sqrt{m}} \bigg( (1+2\epsilon)^{j} - (1-2\epsilon)^{j} \bigg) \le \frac{e}{\pi \sqrt{m}} \sum_{\text{even } j \le 3\sqrt{m}} \bigg( 1+3\epsilon \cdot j - (1-2\epsilon \cdot j) \bigg).
\]
This is at most $\frac{e}{\pi \sqrt{m}} \cdot \sum_j 5 \eps \cdot j=\frac{e}{\pi \sqrt{m}} \cdot 5 \eps \cdot (3\sqrt{m}/2) \cdot (3\sqrt{m}+2)/2 \le \frac{13e}{\pi} \cdot \eps \cdot \sqrt{m} \le 0.38$ when $\eps<0.01$. From all discussion above, we have
\[
\Pr_{r \sim D_1}[r \in T] - \Pr_{r \sim D_{-1}}[r \in T] \le 0.38+1/9 = 0.5.
\]
\fi