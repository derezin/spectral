\documentclass[11pt]{article}
\usepackage{times}
%\usepackage{natbib}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
%\usepackage{algorithmic}
\usepackage{algorithm}
%\usepackage{subfig}
\usepackage{color}
\usepackage[english]{babel}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{enumitem}

%\usepackage[square,sort,comma,numbers]{natbib}
\usepackage{natbib}

%\usepackage[pdf]{pstricks}
\usepackage{wrapfig,epsfig}
\usepackage{psfrag}
\usepackage{epstopdf}
\usepackage{url}
\usepackage{graphicx}
\usepackage{color,xcolor}
\usepackage{epstopdf}
\usepackage{algpseudocode}
\usepackage[hidelinks,pdfencoding=auto,psdextra]{hyperref}
%\usepackage[T1]{fontenc}
%\usepackage{verbatim}
%\let\C\relax
%\usepackage{tikz}
\usepackage{hyperref}
\hypersetup{colorlinks=false}
\hypersetup{%draft, % TODO: remove "draft" option before final version after making sure no hyperlinks are broken due to pagebreaks.
	colorlinks,
	linkcolor={red!40!gray},
	citecolor={blue!40!gray},
	urlcolor={blue!70!gray}
}

%\usetikzlibrary{arrows}
%\usepackage[lmargin=1in,rmargin=1in,tmargin=0.8in,bmargin=0.8in]{geometry}
\usepackage[margin=1in]{geometry}
\linespread{1}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{property}[theorem]{Property}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{notation}[theorem]{Notation}
%\newtheorem{proof}[theorem]{Proof}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{assumption}[theorem]{Assumption}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{example}[theorem]{Example}
\newtheorem{open}[theorem]{Open Problem}

\newcommand{\setdiff}{\Delta}
\newcommand{\hf}{\wh{f}}
\newcommand{\hg}{\wh{g}}
\newcommand{\hh}{\wh{h}}
\newcommand{\wh}{\widehat}
\newcommand{\wt}{\widetilde}
\newcommand{\ov}{\overline}

\def\bx{{\bf x}}

\def\P{\mbox{\rm P}}
\newcommand{\eps}{\varepsilon}
\newcommand{\Ot}{\widetilde{O}}

\newcommand{\norm}[1]{\|#1\|}
\newcommand{\abs}[1]{|#1|}
\newcommand{\NP}{\mbox{\rm NP}}
\newcommand{\AC}{\mbox{\rm AC}}
\newcommand{\opt}{\mathsf{opt}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\rev}{\mathsf{rev}}
\newcommand{\Tr}{\mathsf{Tr}}
\newcommand{\val}{\mathsf{val}}
\newcommand{\supp}{\mathsf{supp}}
\newcommand{\diag}{\mathsf{diag}}
\newcommand{\sign}{\mathsf{sign}}
\newcommand{\poly}{\mathsf{poly}}
\newcommand{\poiss}{\mathsf{Poisson}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\cov}{\mathsf{cov}}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\E}{\mathbb{E}}
\newcommand{\empt}{\mathsf{empt}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\FF}{\mathcal{F}}
\renewcommand{\i}{\mathbf{i}}
\newcommand{\midd}{\mathsf{mid}}
\newcommand{\Mc}{\mathcal{M}}


\DeclareMathOperator*{\argmax}{arg\,max}

\DeclareMathOperator{\Gaussian}{Gaussian}
\DeclareMathOperator{\rect}{rect}
\DeclareMathOperator{\sinc}{sinc}
\DeclareMathOperator{\Comb}{Comb}
\DeclareMathOperator{\signal}{signal}

%\newcommand{\sq}{\hbox{\rlap{$\sqcap$}$\sqcup$}}
%\newcommand{\qed}{\hspace*{\fill}\sq}
\newenvironment{proofof}[1]{\bigskip \noindent {\bfseries \upshape Proof of #1.}\quad }
{\qed\par\vskip 4mm\par}

\def\cuc{{robust uniform convergence}}
\def\Cuc{{Robust uniform convergence}}
\def\CUC{{RUC}}

\begin{document}

\title{Query Complexity of
  Least Absolute Deviation Regression\\
  via Robust Uniform Convergence}

% Use \Name{Author Name} to specify the name.
% If the surname contains spaces, enclose the surname
% in braces, e.g. \Name{John {Smith Jones}} similarly
% if the name has a "von" part, e.g \Name{Jane {de Winter}}.
% If the first letter in the forenames is a diacritic
% enclose the diacritic in braces, e.g. \Name{{\'E}louise Smith}

% Two authors with the same address
% \coltauthor{\Name{Author Name1} \Email{abc@sample.com}\and
%  \Name{Author Name2} \Email{xyz@sample.com}\\
%  \addr Address}

% Three or more authors with the same address:
% \coltauthor{\Name{Author Name1} \Email{an1@sample.com}\\
%  \Name{Author Name2} \Email{an2@sample.com}\\
%  \Name{Author Name3} \Email{an3@sample.com}\\
%  \addr Address}

% Authors with different addresses:
\author{\textbf{Xue Chen} \\
George Mason University\\
\texttt{xuechen@gmu.edu}
\and \textbf{Micha{\l} Derezi\'nski}\\
University of California, Berkeley\\
 \texttt{mderezin@berkeley.edu}
}


\maketitle

\date{}

% Authors with different addresses:

\begin{abstract}%
  Consider a regression problem where the learner is given
  a large collection of $d$-dimensional data points, but can only
  query a small subset of the real-valued labels. How many queries are
  needed to obtain a $1+\epsilon$ relative error approximation of the
  optimum? While this problem has been extensively studied for least
  squares regression, little is known for other losses. An important
  example is least absolute deviation regression ($\ell_1$ regression)
  which enjoys superior robustness to outliers compared to least
  squares. We develop a new framework for analyzing importance
  sampling methods in regression problems, which enables us to show
  that the query complexity of 
  least absolute deviation regression is $\Theta(d/\epsilon^2)$ up to
  logarithmic factors. We further extend our techniques to show the
  first bounds on the query complexity for any $\ell_p$ loss with
  $p\in(1,2)$. As a key novelty in our analysis, we introduce the
  notion of \emph{\cuc}, which is a new approximation guarantee for the
  empirical loss. While it is inspired by uniform
  convergence in statistical learning, our approach additionally incorporates a correction
  term to avoid unnecessary variance due to outliers. This
  can be viewed as a new connection between statistical learning theory and
  variance reduction techniques in stochastic optimization,
which should be of
  independent interest.
\end{abstract}

\section{Introduction}

Consider a linear regression problem defined by an $n\times d$ data matrix
$A$ and a vector $y\in\R^n$ of labels (or responses). Our goal is to
approximately find a vector $\beta\in\R^d$ that minimizes the total
loss over the entire dataset, given by
$L(\beta)=\sum_{i=1}^nl(a_i^\top\beta-y_i)$, where $a_i^\top$ is the $i$th
row of $A$. Suppose that we are only given the data matrix $A$ and we
can choose to query some number of individual labels $y_i$ (with the
remaining labels hidden). When the
cost of obtaining those labels dominates other computational costs
(for example, because it requires performing a complex and
resource-consuming measurement), %then 
it is natural to ask how many
queries are required to obtain a good approximation of the optimal
solution over the entire dataset. This so-called query complexity
arises in a number of statistical learning tasks such as active learning
and experimental design.
\begin{definition}[Query complexity]\label{def:query-complexity}
  For a given loss function $l:\R\rightarrow \R_{\geq 0}$, the query
  complexity $\Mc:=\Mc_l(d,\epsilon,\delta)$ is the smallest number such that
  there is a randomized algorithm that, given any $n\times d$ matrix
  $A$ and any hidden vector $y\in\R^n$, queries $\Mc$ entries of $y$
  and returns $\wt{\beta}\in\R^d$, so that:
  \begin{align*}
L(\wt{\beta})\leq (1+\epsilon)\cdot \min_\beta
    L(\beta)\quad\text{with probability $1-\delta$},\quad\text{where}\quad L(\beta) =
    \sum_{i=1}^nl(a_i^\top\beta-y_i).
  \end{align*}
\end{definition}
Note that since the algorithm has access to the entire dataset on
which it will be evaluated, it can use that information to select the
queries better than, say uniformly at random, which differentiates
this problem from the traditional sample complexity in statistical
learning theory. Also, note that we require a relative error
approximation, as opposed to an additive one. This is because we
are not restricting the range of the labels, so a relative error
approximation provides a more useful scale-invariant guarantee (this is a common
practice when approximating regression problems).   

Significant work has been dedicated to various notions of query complexity in
classification \citep[for an overview, see][]{hanneke2014theory}.
On the other hand, in the context of regression, prior 
literature has primarily focused on the special case of least squares regression,
$l(a) = a^2$, where the optimum solution has a closed form expression,
which considerably simplifies the analysis. In this setting, the query
complexity is known to be $\Mc=O(d/\epsilon)$ \citep{CP19}. 
An important drawback of the square loss in linear regression is its
sensitivity to outliers, and to that end, a number of other loss
functions are commonly used in practice to ensure robustness to
outliers. Those losses no longer yield a closed form solution, so the
techniques from least squares do not  apply. The primary example is
\emph{least absolute deviation} regression, 
also known as $\ell_1$ regression, which uses the loss function
$l(a)=|a|$. A natural way of interpolating between the robustness of
the $\ell_1$ loss and the smoothness of least squares, is to use an
$\ell_p$ loss, i.e., $l(a)=|a|^p$ for $p\in(1,2)$. Other popular
choices include the Huber loss and the Tukey loss; for more discussion on robust regression
see \cite{CW15}. The basic question that motivates this paper is:
What is the query complexity of robust regression? This question has
remained largely open even for the special case of least absolute
deviation regression.


\subsection{Main Results}

Our first main result gives nearly-matching (up to logarithmic factors) upper and lower bounds for
the query complexity of least absolute deviation regression.
While the randomized algorithm that achieves the upper bound is in
fact based on an existing importance sampling method, our key contribution is to provide a
new analysis of this method that overcomes a significant limitation of
the prior work, resulting in the first non-trivial guarantee for query complexity.
\begin{theorem}\label{t:ell1}
  The query complexity of least absolute deviation regression, $l(a)=|a|$, satisfies:
  \begin{align*}
  \Omega( (d+\log(1/\delta))/\epsilon^2)  \leq \Mc_{l}(d,\epsilon,\delta) \leq
    O( d\log(d/\epsilon\delta)/\epsilon^2).
  \end{align*}
\end{theorem}
To obtain the upper bound, we use the so-called Lewis
weight sampling originally due to \cite{Lewis1978} (see Section
\ref{sec:preli}), which can be implemented in 
time that is nearly-linear in the support size of the matrix $A$
\citep{CP15_Lewis}. Lewis weight sampling is known to provide 
a strong guarantee called the $\ell_1$ subspace embedding
property~\citep{Talagrand90,CP15_Lewis}, which 
is an important tool in randomized linear algebra for approximately solving least absolute 
deviation regression. However, this strategy only works if the
algorithm has unrestricted access to the vector 
$y$ when computing the weights (e.g., when
the motivation is computational efficiency rather than query
complexity), or if we require only a constant factor approximation
(i.e., $\epsilon >1$).%
\footnote{A constant factor
  approximation with $\eps=7$ and $\delta=0.4$ is folklore (e.g.,
  Theorem 6 in \cite{dasgupta2009sampling}). To obtain a more accurate approximation,
one can use the $\ell_1$ subspace embedding property
with respect to the matrix $[A;y]$ (which has $d+1$ columns) instead of $A$, however this requires
unrestricted access to the vector $y$.}
So, prior work on Lewis weight sampling provides
no guarantees for query complexity with $\epsilon\leq 1$.

In this paper, we develop a new analysis of Lewis weight sampling,
which relies on what we call \emph{\cuc}: an approximation guarantee for the
empirical loss that is similar to uniform
convergence in statistical learning theory, except it incorporates a correction 
to reduce the variance due to outliers. This leads to an intriguing
connection between uniform convergence in statistical learning and
variance reduction techniques in stochastic optimization (see Section \ref{s:technique}). Our new
approach not only allows us to compute the weights without accessing
the label vector, thereby obtaining guarantees for query complexity,
but it also leads to simplifications of existing methods for approximately solving 
the regression problem, both in terms of the algorithms and the
analysis (see Section \ref{s:related-work}). Our analysis framework is
not specific to the $\ell_1$ loss, and is likely of interest beyond
robust regression. To demonstrate this, our second main result
uses {\cuc} to provide an upper bound on the query complexity of regression with
any $\ell_p$ loss where $p\in(1,2)$, which is again the first
non-trivial guarantee of this kind.
\begin{theorem}\label{t:ellp}
For any $p\in(1,2)$,  the query complexity of $\ell_p$ regression,
$l(a)=|a|^p$, satisfies:
\begin{align*}
  \Mc_{l}(d,\epsilon,\delta) \leq O(d^2\log(d/\epsilon\delta)/\epsilon^2\delta).
\end{align*}
\end{theorem}
While this result also relies on Lewis weight sampling, the analysis
is considerably more challenging, since it requires interpolating between
the techniques for $\ell_1$ and $\ell_2$ losses. We expect that the upper
bound can be improved to match the one from Theorem \ref{t:ell1} in
terms of the dependence on $d$, and we leave this as a new direction for
future work.

\subsection{Key Technique: Robust Uniform Convergence}
\label{s:technique}

Next, we give a brief overview of the proof techniques used to obtain
the main results, focusing on the
notion of {\cuc} which is central to our analysis.

A key building block in the algorithms for approximately solving
regression problems is randomized sampling, which is used to construct
an estimate of the loss
$L(\beta)=\sum_{i=1}^nl(a_i^\top\beta-y_i)$. This involves
constructing a sparse sequence of non-negative random variables
$s_1,...,s_n$, such that $\wt{L}(\beta)=\sum_{i=1}^ns_i l(a_i^\top\beta-y_i)$ 
is an unbiased estimate of $L(\beta)$, i.e.,
$\E[\wt{L}(\beta)]=L(\beta)$. The algorithm then solves the regression
problem defined by the loss estimate, $\wt{\beta}=\argmin_\beta
\wt{L}(\beta)$, and returns $\wt{\beta}$ as the approximate solution
to the original problem. To minimize the number of queries, we
have to make sure that the number of non-zero $s_i$'s is small
(if $s_i>0$, then we must query $y_i$), while
at the same time preserving the quality of the approximate solution.

A standard approach for establishing guarantees in statistical
learning is via the framework of uniform convergence: Showing that
$\sup_\beta|L(\beta)-\wt{L}(\beta)|$ vanishes at some rate with the
sample size \cite[for an overview,
see][]{vapnik1999overview,vapnik2013nature}. This approach can be
adapted to the setting of relative 
error approximation, requiring that for sufficiently large sample size:
\begin{align*}
  \text{(uniform convergence of relative error)}\quad
  \sup_\beta\frac{|L(\beta)-\wt{L}(\beta)|}{L(\beta)}\leq\epsilon
  \quad\text{with probability }1-\delta.
\end{align*}
This has proven successful for importance sampling in
regression problems where the access to label vector $y$ is
unrestricted and the primary aim is computational efficiency. However, in the query model, it may happen that one
entry $y_i$ (unknown to the algorithm) is an outlier which significantly contributes to the loss
$L(\beta)$, and without sampling that entry we will not obtain a good
estimate $\wt{L}(\beta)$. In the worst-case, a randomized algorithm
that is oblivious to $y$ may need to sample almost all of the entries
before discovering the outlier. Does this mean that it is impossible to
obtain a relative error approximation of the optimum without catching
the outlier? In fact it does not, and this is particularly intuitive in
the case of least absolute deviation regression, where our goal is
specifically to ignore the outliers. For this reason, we use a
modified version of the uniform convergence property, where the
contribution of the outliers is subtracted  from the loss, using a
correction term denoted by $\Delta$ in the following definition.
\begin{definition}[\Cuc]\label{def:uniform_approx}
  A  randomized algorithm satisfies the
  {\cuc} property with query complexity
  $m(d,\epsilon,\delta)$ if, given any $n\times d$ matrix
  $A$ and hidden vector $y\in\R^n$, it queries $m(d,\epsilon,\delta)$ entries of $y$
  and then with probability $1-\delta$ returns $\wt{L}(\cdot)$ s.t.:  
  % An estimate $\wt{L}(\cdot)$ is a centered uniform
  % $\epsilon$-approximation of the loss $L(\cdot)$ if, denoting
  % $\beta^*=\argmin_\beta L(\beta)$, we have:
  \begin{align}
    \sup_\beta\frac{|L(\beta)-\wt{L}(\beta) - \Delta|}{L(\beta)}
%    |(L(\beta)-L(\beta^*)) - (\wt{L}(\beta)-\wt{L}(\beta^*))|
    \leq \epsilon,\quad\text{where}\quad \Delta = L(\beta^*) -
    \wt{L}(\beta^*),\quad \beta^*=\argmin_\beta L(\beta).\label{eq:cuc}
  \end{align}
  % If a randomized algorithm, given any $n\times d$ matrix
  % $A$ and any hidden vector $y\in\R^n$, queries $m(d,\epsilon,\delta)$ entries of $y$
  % and then with probability $1-\delta$ returns a centered uniform
  % $\epsilon$-approximation of $L(\cdot)$,
  % then it satisfies the
  % centered uniform convergence property with query complexity $m(d,\epsilon,\delta)$.
\end{definition}
Note that as long as $\wt{L}(\cdot)$ is
an unbiased estimate of $L(\cdot)$, then $\Delta$ is a mean zero
correction of the  error quantity in uniform convergence. This bares
much similarity to variance reduction 
techniques which have gained considerable attention in stochastic
optimization \cite[for an overview, see][]{gower2020variance}. The key
difference is that we are using the correction purely for the analysis, and not for the
algorithm. Thus, whereas in optimization algorithms
such as Stochastic Variance Reduced Gradient \cite[SVRG,][]{johnson2013accelerating}, one must
explicitly compute the correction based on an estimate of $\beta^*$,
in our setting one never has to compute $\Delta$, so we can simply use
$\beta^*$ itself to define the correction. Nevertheless, the analogy is apt
in that the corrected error quantity will in fact have reduced
variance, particularly in the presence of outliers, which is what
enables our query complexity analysis.

The guarantee from \eqref{eq:cuc} immediately implies that the regression
estimate $\wt{\beta}=\argmin_\beta\wt{L}(\beta)$ is a
relative error approximation of $\beta^*$, as long as $\epsilon<1$:
\begin{align*}
  L(\wt{\beta})-L(\beta^*)\leq \wt{L}(\wt{\beta})-\wt{L}(\beta^*) +
  \epsilon\cdot L(\wt{\beta})\leq \epsilon\cdot L(\wt{\beta}),
\end{align*}
where we used that $\wt{L}(\wt{\beta})\leq
\wt{L}(\beta^*)$, and after some manipulations, we get
$L(\wt{\beta})\leq(1+\frac\epsilon{1-\epsilon})\cdot L(\beta^*)$. Thus
to bound the query complexity in Theorems \ref{t:ell1} and
\ref{t:ellp}, it suffices to bound the complexity of ensuring
{\cuc} for a given loss function. % which is in
% fact what we do in Sections \ref{sec:analysis_ell1} and
% \ref{sec:analysis_ellp}, respectively.

Finally, to obtain our results we must still use
carefully chosen
importance sampling to construct the loss estimate, where the
importance weights depend on the data matrix $A$. Here, we use the Lewis
weights, which is an extension of statistical leverage scores (see
Section \ref{sec:preli} for details) that is known to be effective in
approximating $\ell_p$ losses. Existing guarantees for Lewis
weight sampling (namely, the $\ell_p$ subspace embedding property)
prove insufficient for establishing {\cuc}, so 
we develop new techniques, which are presented for $p=1$ in
Section~\ref{sec:analysis_ell1} and for $p\in(1,2)$ in
Section~\ref{sec:analysis_ellp}. 
Moreover, in Section~\ref{sec:preli}, we discuss Lewis weights and
their connection to natural importance weights defined as
$\underset{\beta}{\max} \frac{ |a_i^{\top} \beta|^p}{\|A \beta\|_p^p}$
for each row $a_i$.  
Also, note that our results only
require a constant factor approximation of Lewis weights, where the
constant enters into the query complexity. As a consequence, we can
obtain new relative error guarantees for uniform sampling, where the sample size
depends on a notion of matrix coherence based on the degree of
non-uniformity of Lewis~weights.

\subsection{Related Work}
\label{s:related-work}

There is significant prior work related to query complexity and
relative error approximations for linear regression, which we summarize below.

Much of the work on importance sampling for linear regression has been
done in the context of Randomized Numerical Linear Algebra 
\citep[RandNLA; see, e.g.,][]{DM16_CACM,dpps-in-randnla}, where the primary goals are computational efficiency or
reducing the size of the problem. This line of work was initiated by
\cite{drineas2006sampling}, using importance sampling via statistical
leverage scores to obtain relative error approximations of $\ell_2$
regression. Leverage score sampling is known to require $\Theta(d\log d + d/\epsilon)$
queries \citep[see, e.g.,][]{correcting-bias-journal}, where we let
the failure probability $\delta$ be a small constant for
simplicity. Other techniques, such as random projections
\citep[e.g.,][]{sarlos-sketching,regression-input-sparsity-time},
have been 
used for efficiently solving regression problems, however those
methods generally require unrestricted access to the label vector
$y$. More recently, \cite{unbiased-estimates} considered the query
complexity of $\ell_2$ regression, showing that $d$ queries are
sufficient to obtain a relative error approximation with
$\epsilon=d$, by using a non-i.i.d.\ importance sampling technique
called Volume Sampling. Note that at least $d$ queries are necessary
for regression with any non-trivial loss
\citep{unbiased-estimates-journal}.  Efficient algorithms for 
Volume Sampling were given by
\cite{leveraged-volume-sampling,correcting-bias}. A different
non-i.i.d.~sampling approach was used by \cite{CP19} to reduce
the query complexity of least squares to $O(d/\epsilon)$, with a matching
lower bound.

Sampling algorithms for regression with a general $\ell_p$ loss were
studied by \cite{dasgupta2009sampling}. The importance weights they used are
closely related to Lewis weights, and most of our analysis can
be adapted to work with those weights (we use Lewis weights because
they yield a better polynomial dependence on $d$). They use a two-stage sampling scheme, where the
importance weights in the first stage are computed without accessing
the label vector. Their analysis of the first stage leads to $O(d^{2.5})$ query
complexity for obtaining a relative error approximation with
$\epsilon=O(1)$. However, to further improve the approximation, their
second stage constructs refined importance weights using the entire
label vector. Remarkably, using {\cuc} one can show that, at least for
$p\in[1,2]$, their second stage weights are not necessary (it suffices
to use the weights from the first stage with an adjusted sample size),
simplifying both the algorithm and the analysis. Other randomized
methods have been proposed for robust regression 
\citep[e.g.,
see][]{clarkson2005subgradient,iterative-row-sampling,mm-sparse,DLS18a}. In
particular, \cite{CW15} provide a more general 
framework that includes the Huber loss. However, all of these
approaches require unrestricted access to the label vector, and so
they do not imply any bounds for query complexity. Finally,
\cite{CP15_Lewis} proposed to use Lewis weights as an improvement to the
importance weights of \cite{dasgupta2009sampling}, however their results
again focus on computational efficiency.  We discuss this in
more detail in Section \ref{sec:preli}.

Query complexity has been studied extensively in the context of active
learning \citep[see,
e.g.,][]{cohn1994improving,balcan2009agnostic,hanneke2014theory}. These
works focus mostly on classification 
problems, where one can take advantage of adaptivity: selecting next
query based on the previously obtained labels. Our framework
(Definition \ref{def:query-complexity}) does allow for such adaptivity,
however, in the context of regression it appears to be of limited use 
beyond the initial selection of sampling weights. Approaches without
adaptivity are sometimes referred to as pool-based 
active learning \citep{pool-based-active-learning-regression} or
experimental design \citep{chaloner1984optimal,minimax-experimental-design}.

Finally, in classification problems where each $y_i \in \{0,1\}$,
prior work considered a variance reducing correction term to
obtain faster rates of uniform convergence \citep[for an overview,
see][]{boucheron2005theory}. Despite 
some similarities, our notion of robust uniform convergence serves a
different purpose in that it is used to
remove the effect of outliers in a regression problem where $y_i$
could be~unbounded. 

\input{preli}

\input{ell1concentration}

\input{generalization_p}

\input{lower}

\section{Conclusions and Future Directions}
We provided nearly-matching upper and lower bounds for the query
complexity of least absolute deviation regression. In this setting, the learner is
allowed to use a sampling distribution that depends on the unlabeled
data. In the process, we
proposed \emph{\cuc}, a framework for showing sample complexity
guarantees in regression tasks, which includes a correction term that minimizes the effect
of outliers. Further, we extended our results to robust regression with any
$\ell_p$ loss for $p\in(1,2)$. Note that the guarantees in this paper
also apply to data-oblivious sampling, in which case we pay an additional
data-dependent factor in the sample complexity.

Many new directions for future work arise from our results. First of
all, we expect that the query complexity of $\ell_p$ regression
should match the guarantee for $\ell_1$ (our current bounds exhibit
quadratic dependence on the input dimension $d$ for $p>1$, and linear dependence
for $p=1$). Moreover, the query complexity question remains open for a
number of other robust losses, such as the Huber loss and the
Tukey loss. More broadly, we ask whether the {\cuc} framework can be
used to obtain sample complexity bounds that are robust to the presence of
outliers for other settings in
statistical 
learning, e.g., for classification and non-linear regression problems,
and with additive as well as multiplicative error bounds.

\subsection*{Acknowledgments}
We would like to acknowledge DARPA, IARPA, NSF, and ONR via its BRC on
RandNLA for providing partial support of this work.  Our conclusions
do not necessarily reflect the position or the policy of our sponsors,
and no official endorsement should be inferred. 


% Acknowledgments---Will not appear in anonymized version
%\acks{We thank a bunch of people and funding agency.}

\bibliographystyle{apalike} 
\bibliography{all.bib,../pap.bib}

\appendix

\input{importance_weight_Lewis}

\input{app_proof_ell1}

\input{app_proof_ellp}

\input{app_proof_lower}

\end{document}
