%!TEX root = robust-colt21.tex
\section{Preliminaries}\label{sec:preli}
Throughout this work, let $A$ denote the data matrix of dimension $n \times d$ and $a_1,\ldots,a_n$ be its $n$ rows such that $A^{\top}= [a_1^{\top},\ldots,a_n^{\top}]$. Then let $\beta \in \mathbb{R}^d$ denote the coefficient vector and $y \in \mathbb{R}^n$ be the hidden vector of labels. When $p$ is clear, $\beta^*$ denotes the minimizer of $\ell_p$ regression $L(\beta)=\|A \beta - y\|_p^p$, where $\|\cdot\|_p$ denotes the $\ell_p$ vector norm.

We always use $\wt{L}$ to denote the empirical loss $\wt{L}(\beta)=\sum_{i=1}^n s_i \cdot |a_i^{\top} \beta - y_i|^p$ with weights $(s_1,\ldots,s_n)$. When we write down the weights as a diagonal matrix $S=\diag(s_1^{1/p},\ldots,s_n^{1/p})$, the loss becomes $\wt{L}(\beta)=\|S A \beta - S y\|_p^p$. Since it is more convenient to study the concentration of the random variable $\wt{L}(\beta)-\wt{L}(\beta^*)$ around its mean $L(\beta)-L(\beta^*)$, we will rewrite \eqref{eq:cuc} as
\[
\wt{L}(\beta)-\wt{L}(\beta^*) = L(\beta)-L(\beta^*) \pm \epsilon\cdot L(\beta)\quad\forall \beta \in \mathbb{R}^d,
\]
where $a=b \pm \epsilon$ means that $a$ and $b$ are $\eps$-close, i.e., $a \in [b-\epsilon,b+\epsilon]$.
Furthermore, for two variables (or numbers) $a$ and $b$, we use $a \approx_{\alpha} b$ to denote that $a$ is an $\alpha$-approximation of $b$ for $\alpha \ge 1$, i.e., that $b/\alpha \le a \le \alpha \cdot b$.  Also, let $\poiss(\lambda)$ denote the Poisson random variable with mean $\lambda$ and $\sign(x)$ be the sign function which is 0 for $x=0$ and $x/|x|$ for $x\neq 0$.

\paragraph{Importance weights and Lewis weights.} Given $A$ and the norm $\ell_p$, we define the importance weight of a row $a_i$ to be \[
\sup_{\beta} \frac{|a_i^{\top} \beta|^p}{\|A \beta\|_p^p}.
\]
Note that this definition is rotation free, i.e., for any rotation matrix $R \in \mathbb{R}^{d \times d}$, vector $R^\top a_i$ has the same importance weight in $AR$ since we could replace $\beta$ in the above definition by $R^{-1}\beta$. 

While most of our results hold for importance weights, it will be more convenient to work with Lewis weights \citep{Lewis1978}, which have efficient approximation algorithms  and provide additional useful properties. The Lewis weights $(w_1,\ldots,w_n)$ of $A \in \mathbb{R}^{n \times d}$ are defined as the unique solution \citep{Lewis1978,CP15_Lewis} of the following set of equations:
\begin{equation}\label{eq:def_Lewis_weight}
\forall i \in [n], a_i^{\top} \cdot (A^{\top} \cdot \diag(w_1,\ldots,w_n)^{1-2/p} \cdot A)^{-1} a_i = w_i^{2/p}.
\end{equation}
Note that Lewis weights always satisfy $\sum_i w_i=d$. We will use extensively the following relation between the importance weights and Lewis weights, which is potentially of independent interest.
\begin{theorem}\label{thm:Lewis_weight_importance}
Given any $p \in [1,2]$ and matrix $A$, let $(w_1,\ldots,w_n)$ be the Lewis weights of $A$ defined in \eqref{eq:def_Lewis_weight}. Then the importance weight of every row $i$ in $A$ is bounded by
\[
\underset{\beta \in \mathbb{R}^{d}}{\max} \frac{|a_i^{\top} \beta|^p}{\|A \beta\|_p^p} \in \left[ d^{-(1-p/2)} \cdot w_i, w_i \right].
\]
\end{theorem}
We remark that when $p=2$, the Lewis weights are equal to the statistical leverage scores of $A$ (the $i$th leverage score is defined as $a_i^\top(A^\top A)^\dagger a_i$, where $^\dagger$ denotes the Moore-Penrose pseudoinverse), which are known to be equal to the $\ell_2$  importance weights \citep{spielman2011graph,CP19}. The proof of Theorem~\ref{thm:Lewis_weight_importance} is via a study of the relationships between importance weights, statistical leverages scores, and Lewis weights. In particular, our analysis (Lemma~\ref{lem:uniform_lewis_bounds_importance} and Claim~\ref{clm:non_uniform_Lewis_w}) also gives the sample complexity of uniform sampling for $\ell_p$ regression by comparing Lewis weights with uniform sampling in terms of the non-uniformity of the statistical leverage scores. We defer the detailed discussions and proofs to Appendix~\ref{sec:Lewis_weight_importance}.

\paragraph{Computing Lewis weights.} Given two sequences of weights $(w'_1,\ldots,w'_n)$ and $(w_1,\ldots,w_n)$, we say $w'$ is an $\gamma$-approximation of $w$ if $w'_i \approx_{\gamma} w_i$ for every $i \in [n]$. Now we invoke the contraction algorithm by \cite{CP15_Lewis} to approximate $w_i$. While we state it for a $(1+\epsilon)$-approximation, our results only need a constant approximation say $\gamma=2$.
\begin{lemma}\label{lem:compute_Lewis}[Theorem 1.1 in \cite{CP15_Lewis}]
Given any matrix $A \in \mathbb{R}^{n \times d}$ and $p \in [1,2]$, there is an algorithm that runs in time $\log (\log \frac{n}{\epsilon}) \cdot O(\mathbf{nnz}(A)\log n+d^{\omega+o(1)} \big) $ to output a $(1+\epsilon)$-approximation of the Lewis weights of $A$, where $\mathbf{nnz}(A)$ denotes the number of nonzero entries in $A$ and $\omega$ is the matrix-multiplication exponent.
\end{lemma}

\paragraph{Subspace embedding.} Since the diagonal matrix $S=\diag(s_1^{1/p},\ldots,s_n^{1/p})$ will be generated from the Lewis weights with support size $O(\frac{d \log \frac{d}{\eps\delta}}{\eps^2})$ for $p=1$ and $O(\frac{d^2 \log d/\eps\delta}{\eps^2 \delta})$ for $p\in (1,2]$, by the main results in \cite{CP15_Lewis}, we can assume that $SA$ satisfies the $\ell_p$ subspace embedding property, given below.
\begin{fact}\label{fact:subspace_emd}
Given any $p \in [1,2]$ and $A$, the sketch matrix $S$ generated in Theorem~\ref{thm:concentration_contraction} for $p=1$ or Theorem~\ref{thm:property_ellp} for $p\in (1,2]$ has the following property: With probability $1-\delta$,
\[
\|S A \beta\|_p^p \approx_{1+\epsilon} \|A \beta\|_p^p \quad\text{ for any } \beta.
\]
\end{fact}